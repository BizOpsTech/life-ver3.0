//
//  RuleSliderView.m
//  Life Calculation
//
//  Created by Snow on 9/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "RuleSliderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation RuleSliderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {

        //slider button and value change
        [self.swSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.swSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

//- (void)setDefaultSuffix:(NSString *)defaultSuffix{
//    if (_defaultSuffix == nil) {
//        _defaultSuffix = [NSString new];
//    }
//    _defaultSuffix = defaultSuffix;
//}
//
//- (void)setDefaultUnits:(NSString *)defaultUnits{
//    if (_defaultUnits == nil) {
//        _defaultUnits = [NSString new];
//    }
//    _defaultUnits = defaultUnits;
//}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    UIView *parterformLabel = [self viewWithTag:501];
    parterformLabel.layer.borderColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.6f].CGColor;
    parterformLabel.layer.borderWidth = 1.0f;
    parterformLabel.layer.cornerRadius = 5.0f;

    // Drawing code
    [self.swSlider setThumbImage:[UIImage imageNamed:@"slider_button"] forState:UIControlStateNormal];
    [self.swSlider setThumbImage:[UIImage imageNamed:@"slider_button"] forState:UIControlStateHighlighted];
    
    [self.swSlider setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"slider_ruler"]]];
}

- (void)sliderIntervalSteper:(UISlider*)slider{
    
    NSUInteger intOfInter = _intervalValue;
    
    if (intOfInter > 0) {
        float newStep = roundf(slider.value / _intervalValue);
        [slider setValue:(newStep *_intervalValue) animated:YES];
    }
}

- (IBAction)sliderValueChanged:(UISlider*)slider{
    
    [self sliderIntervalSteper:slider];
    
    self.currentNumberLabel.text = [NSString stringWithFormat:@"%@ %i%@",_defaultUnits == nil ? @"" : _defaultUnits,[[NSNumber numberWithFloat:slider.value] intValue], _defaultSuffix == nil ? @"" :_defaultSuffix];
    
    //self.minLabel.text = self.maxLabel.text = [NSString stringWithFormat:@"%i",[[NSNumber numberWithFloat:slider.value] integerValue]];
    if ([_delegate respondsToSelector:@selector(sliderValueChanged:)]) {
        [self.delegate sliderValueChanged:slider];
    }
}


- (void)setSliderValue:(float)value{
    
    self.swSlider.value = value;
    [self sliderIntervalSteper:self.swSlider];
}

- (void)isHiddenMinAndMaxLabel:(BOOL)hidden{
    
    UIView *v502,*v503,*v504,*v505;
    v502 = [self viewWithTag:502];
    v503 = [self viewWithTag:503];
    v504 = [self viewWithTag:504];
    v505 = [self viewWithTag:505];
    if (v502 && v503 && v504 && v505) {
        v502.hidden = v503.hidden = v504.hidden = v505.hidden = hidden;
    }
}

- (void)isHiddenSlideTitleLabel:(BOOL)hidden{
    UIView *v506;
    v506 = [self viewWithTag:506];
    if (v506) {
        v506.hidden = hidden;
    }
}
@end
