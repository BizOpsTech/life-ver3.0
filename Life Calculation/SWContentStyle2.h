//
//  SWContentStyle2.h
//  Life_20131116
//
//  Created by Snow on 11/18/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioGroupView.h"
#import "ChangeValueDelegate.h"

@interface SWContentStyle2 : UIView<RadioGroupViewDelegate,ChangeValueDelegate>

@property (nonatomic, strong) NSArray *images;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *middleNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (weak, nonatomic) IBOutlet RadioGroupView *rgView;

@property (nonatomic, assign) id<ChangeValueDelegate> delegate;

@end
