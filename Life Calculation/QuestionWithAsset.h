//
//  QuestionWithAsset.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithAsset : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;

- (IBAction)actionWithButton:(UIButton *)sender;


@property (strong, nonatomic) IBOutlet UIButton *btnCostForEach;

@property (strong, nonatomic) IBOutlet UIButton *btnHaveHouse;

@property (strong, nonatomic) IBOutlet UIButton *btnHaveCar;

@property (strong, nonatomic) IBOutlet UIButton *btnAsset;

@property (strong, nonatomic) IBOutlet UIButton *btnLoans;


@end
