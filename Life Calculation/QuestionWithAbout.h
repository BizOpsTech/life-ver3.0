//
//  QuestionWithAbout.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithAbout : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnAge;
@property (strong, nonatomic) IBOutlet UIButton *btnGender;
@property (strong, nonatomic) IBOutlet UIButton *btnMarried;
@property (strong, nonatomic) IBOutlet UIButton *btnKids;
@property (strong, nonatomic) IBOutlet UIButton *btnOccupation;

- (IBAction)ActionWithButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;
@end