//
//  QuestionWithInvestment.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithInvestment.h"
#import "SWContentView.h"
#import "FSTKind.h"

@interface QuestionWithInvestment ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;
@property(nonatomic,strong)AppDelegate *app;
@property(nonatomic,strong)Question *Qinvestor;
@end

@implementation QuestionWithInvestment


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _app = [[UIApplication sharedApplication]delegate];
    _Qinvestor = [Question new];
    
     _btnArr = [NSArray arrayWithObjects:_btnInvestor,_btnSaving,_btnInvestment,_btnProvide,nil];
    
    #pragma mark 填充用户内容
    [self setValueToPage];
    
    [self addCustomViewOnScroll];
    
    [_app autoFixedForButton:_btnArr];
    [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}

- (IBAction)actionWithButton:(UIButton *)sender {
    
    int page =0;
    
    if([sender isEqual:_btnInvestor]){
        page = 0;
       // [self changeBackgroundForButton:YES ButtonIndex:0];
        
    }else if([sender isEqual:_btnSaving]){
        page = 1;
       // [self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnInvestment]){
        page = 2;
       // [self changeBackgroundForButton:YES ButtonIndex:2];
        
    }else if([sender isEqual:_btnProvide]){
        //_btnProvide
        page = 3;
       // [self changeBackgroundForButton:YES ButtonIndex:3];
    }else{
        return;
    }
    
    
    
    CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
    
}

#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    UIColor *borderColor;
    
    if(isEditStatu){
        borderColor = [UIColor colorWithRed:0/255.f green:204/255.f blue:255/255.f alpha:1.0];
        
        ////// 修改按钮样式 ///////////////////////////////////////////////////////////////////
        
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 2.0;
        btn.layer.borderColor = borderColor.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@"buttonBG"] forState:UIControlStateNormal];
        
        ////// 取消其他按钮样式 //////////////////////////////////////////////////////////////////
        [self didEditCompleteFromButton:btnIndex];
        
        
    }else{
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        btn.layer.borderWidth = 0;
    }
}

#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{

    if(_app.recodDb.investment.selectIndex >0){
        _Qinvestor.selectIndex = _app.recodDb.investment.selectIndex;
        NSString *strInvestment = _app.recodDb.investment.answer;
        [self setValueToButton:0 componentVal:strInvestment];
    }
    
    if(_app.recodDb.deposit > 0){
        [self setValueToButton:1 componentVal:[NSString stringWithFormat:@"%d万",_app.recodDb.deposit/ UnitRMB]];
    }
    
    if(_app.recodDb.countInvestment >0){
        [self setValueToButton:2 componentVal:[NSString stringWithFormat:@"%d万",_app.recodDb.countInvestment/ UnitRMB]];
    }
    
    if(_app.recodDb.AdditionalFunding >0){
        [self setValueToButton:3 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.AdditionalFunding/ UnitRMB]];
    }
}



/*
 @property(nonatomic,assign)Question *investment;
 @property(nonatomic,assign)int deposit; //存款
 @property(nonatomic,assign)int countInvestment;  //zhong投资
 @property(nonatomic,assign)NSUInteger AdditionalFunding; //额外资金
 
 _btnInvestor,_btnSaving,_btnInvestment,_btnProvide
 */

#pragma mark 记录选项值
-(void)getValueFromButton{
    _Qinvestor.answer = _btnInvestor.titleLabel.text;
    _app.recodDb.investment = _Qinvestor;
    _app.recodDb.deposit = [_btnSaving.titleLabel.text intValue]* UnitRMB;
    _app.recodDb.countInvestment = [_btnInvestment.titleLabel.text intValue]* UnitRMB;
    _app.recodDb.AdditionalFunding = [_btnProvide.titleLabel.text integerValue]* UnitRMB;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
}



#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    
}

- (void)addCustomViewOnScroll{
    
    self.tabScroll.pagingEnabled = YES;
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    
    self.tabScroll.contentSize = CGSizeMake(width*4, height);
    
    FSTKind *content1 = [[FSTKind alloc] init];
    content1.tag = KQuestion1;
    content1.delegate = self;
    if(_app.recodDb.investment.selectIndex >0){
        [content1 selectedListAtIndex:_app.recodDb.investment.selectIndex];
    }
    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    SWContentView *content2 = [[SWContentView alloc] init];
    content2.imageView.image = ImageName(@"saving");
    content2.images = @[ImageName(@"saving")];
    content2.tag = KQuestion2;
    content2.delegate = self;
    content2.rsView.intervalValue = 0;
    content2.swSlider.tag = 727;
    content2.swSlider.minimumValue = 0;
    content2.swSlider.maximumValue = 100;
    content2.minLabel.text = @"0";
    content2.maxLabel.text = @"100万";
    content2.minSubLabel.hidden=YES;
    if(_app.recodDb.deposit >0){
        content2.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.deposit / UnitRMB];
        content2.swSlider.value = (float)_app.recodDb.deposit / UnitRMB;
    }else{
        content2.currentNumberLabel.text = @"0";
        content2.swSlider.value = 0;
    }
    
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    
    SWContentView *content3 = [[SWContentView alloc] init];
    content3.imageView.image = ImageName(@"investment_box");
    content3.images = @[ImageName(@"investment_box")];
    content3.tag = KQuestion3;
    content3.delegate = self;
    content3.rsView.intervalValue = 0;
    content3.swSlider.tag = 727;
    content3.swSlider.minimumValue = 0;
    content3.swSlider.maximumValue = 100;
    content3.minLabel.text = @"0";
    content3.maxLabel.text = @"100万";
    content3.minSubLabel.hidden=YES;
    if(_app.recodDb.countInvestment >0){
        content3.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.countInvestment / UnitRMB];
        content3.swSlider.value = (float)_app.recodDb.countInvestment / UnitRMB;
    }else{
        content3.currentNumberLabel.text = @"0";
        content3.swSlider.value = 0;
    }
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
    
    
    SWContentView *content4 = [[SWContentView alloc] init];
    content4.imageView.image = ImageName(@"subsidy");
    content4.images = @[ImageName(@"subsidy")];
    content4.tag = KQuestion4;
    content4.delegate = self;
    content4.swSlider.tag = 727;
    content4.rsView.intervalValue = 0;
    content4.swSlider.minimumValue = 0;
    content4.swSlider.maximumValue = 100;
    content4.minLabel.text = @"0";
    content4.maxLabel.text = @"100万";
    content4.minSubLabel.hidden = YES;
    if(_app.recodDb.AdditionalFunding >0){
        content4.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.AdditionalFunding/UnitRMB];
        content4.swSlider.value = (float)_app.recodDb.AdditionalFunding/UnitRMB;
    }else{
        content4.currentNumberLabel.text = @"0";
        content4.swSlider.value = 0;
    }
    
    [self addViewObject:content4 withHeight:height withWidth:width withAtIndex:3];
}


- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
    
    NSString *title;
    UIButton *cusButton;
    
    switch (tagTag) {
        case KQuestion1:
            _Qinvestor.selectIndex = index;
            cusButton = _btnInvestor;
            switch (index) {
                case 1:
                    title = @"保守型";
                    break;
                case 2:
                    title = @"谨慎型";
                    break;
                case 3:
                    title = @"积极型";
                    break;
                case 4:
                    title = @"激进型";
                    break;
                default:
                    break;
            }
            break;
            
        case KQuestion2:
            cusButton = _btnSaving;
            title = [NSNumber numberWithInt:value].stringValue;
                title = [title stringByAppendingString:@"万"];
            break;
        case KQuestion3:
            cusButton = _btnInvestment;
            title = [NSNumber numberWithInt:value].stringValue;
                title = [title stringByAppendingString:@"万"];
            break;
        case KQuestion4:
            cusButton = _btnProvide;
            title = [NSNumber numberWithInt:value].stringValue;
                title = [title stringByAppendingString:@"万"];
            break;
        default:
            break;
    }

    [self myCustomButton:cusButton withTitle:title];
}
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}

@end
