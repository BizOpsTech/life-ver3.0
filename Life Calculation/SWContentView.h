//
//  SWContentView.h
//  Life_20131116
//
//  Created by Snow on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeValueDelegate.h"
#import "RuleSliderView.h"

@protocol ChangeValueDelegate;

@interface SWContentView : UIView<RuleSliderViewDelegate>

@property (nonatomic, strong) NSArray *images;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *sliderTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentNumberLabel;
@property (weak, nonatomic) IBOutlet UISlider *swSlider;
@property (weak, nonatomic) IBOutlet UILabel *maxLabel;
@property (weak, nonatomic) IBOutlet UILabel *minLabel;
@property (weak, nonatomic) IBOutlet UILabel *minSubLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxSubLabel;
@property (weak, nonatomic) IBOutlet RuleSliderView *rsView;

@property (nonatomic, assign) id<ChangeValueDelegate> delegate;
- (void)sliderValueChanged:(UISlider*)swSlider;
@end
