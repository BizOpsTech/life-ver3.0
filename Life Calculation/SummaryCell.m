//
//  SummaryCell.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/1/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "SummaryCell.h"
@interface SummaryCell()

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelCoverage;

@end

@implementation SummaryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTitle:(NSString *)title{
    self.labelTitle.text = title;
}
-(void)setCoverage:(NSNumber *)coverage{
    NSNumberFormatter *dataFormat = [[NSNumberFormatter alloc]init];
    dataFormat.numberStyle = NSNumberFormatterDecimalStyle;
    self.labelCoverage.text = [NSString stringWithFormat:@"￥%@",[dataFormat stringFromNumber:coverage]];
}
@end
