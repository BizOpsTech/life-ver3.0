//
//  BuyInsurancePurpose.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "Recommended.h"
#import "BuyInsurancePurpose.h"
#import "Summary.h"
#import "SummaryCell.h"

#import "TreeGrid.h"
#import "Recommended.h"
@interface BuyInsurancePurpose ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic)buyModel myBuyModel;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,strong)Summary *SDB;
@end
@implementation BuyInsurancePurpose
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    _SDB = [Summary new];
    
    //购买模式
    _myBuyModel = [_SDB getBuyModel:app.recodDb];
    
    //结果数据
    _dataArr = [_SDB getInsuranceResult:app.recodDb];
    ResultBase *tempDB;
    for(NSUInteger i=0; i<_dataArr.count; i++){
        tempDB = [_dataArr objectAtIndex:i];
        if(tempDB.InsuranceType == insuranceModel_Accidental){
            [_dataArr removeObjectAtIndex:i];
            break;
        }
    }
    
    _grid.delegate = self;
    _grid.dataSource = self;
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self setLabelLocation:_myBuyModel tagView:[self.view viewWithTag:200]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLabelLocation:(buyModel)model tagView:(UIView *)view{
    CGFloat offsetX,offsetY;
    offsetX = -25;
    
    switch (model) {
        case buyWithAccidentAndHospitalization:
            offsetY = 175;
            break;
        case buyWithLifeAndCriticalDisease:
            offsetY = 125;
            break;
        case buyWithLongTermSaving:
            offsetY = 75;
            break;
        case buyWithWealthManagement:
            offsetY = 25;
            break;
    }
    
    UIImageView *localImg = [[UIImageView alloc]initWithFrame:CGRectMake(209, offsetY, 68, 22)];
    [localImg setImage:[UIImage imageNamed:@"youareHere"]];
    [view addSubview:localImg];
}

#pragma mark tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (tableView.frame.size.height / _dataArr.count);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    SummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"SummaryCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ResultBase *cellData = [_dataArr objectAtIndex:indexPath.row];
    NSString *category = [_SDB getCategoryName:cellData.InsuranceType];
    cell.title = category;
    cell.coverage = [NSNumber numberWithInt:(int)cellData.unadjusted_Coverage];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"gotoTreePage" sender:[NSNumber numberWithInteger:indexPath.row]];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"gotoRecommendPage"]) {
        Recommended *vc = [segue destinationViewController];
        vc.myBuyModel = self.myBuyModel;
        vc.gridDataArr = self.dataArr;
        
    }else if([segue.identifier isEqualToString:@"gotoTreePage"]){
        UINavigationController *nac  = segue.destinationViewController;
        TreeGrid *TG = nac.viewControllers[0];
        ResultBase *data;
        TG.parentNC = self.navigationController;
        
        
        for(NSUInteger i=0; i< _dataArr.count; i++){
            data = [_dataArr objectAtIndex:i];
            
            //if(data.IsSelect){
                switch (data.InsuranceType) {
                    case insuranceModel_Traditional:
                        TG.LifeCoverage = data.unadjusted_Coverage;
                        if([(NSNumber *)sender integerValue] == i){
                            TG.Caption = @"人寿和意外险";
                            TG.iModel = insuranceModel_Traditional;
                        }
                        
                        break;
                    case insuranceModel_Accidental:
                        TG.AccidentCoverage = data.unadjusted_Coverage;
                        if([(NSNumber *)sender integerValue] == i){
                            TG.Caption = @"意外险";
                            TG.iModel = insuranceModel_Accidental;
                        }
                        
                        break;
                    case insuranceModel_Healthy:
                        TG.HealthCoverage = data.unadjusted_Coverage;
                        if([(NSNumber *)sender integerValue] == i){
                            TG.Caption = @"健康险";
                            TG.iModel = insuranceModel_Healthy;
                        }
                        
                        break;
                    case insuranceModel_Pension:
                        TG.PenSionCoverage = data.unadjusted_Coverage;
                        if([(NSNumber *)sender integerValue] == i){
                            TG.Caption = @"养老险";
                            TG.iModel = insuranceModel_Pension;
                        }
                    
                        break;
                    case insuranceModel_Education:
                        TG.EducationCoverage = data.unadjusted_Coverage;
                        if([(NSNumber *)sender integerValue] == i){
                            TG.Caption = @"教育险";
                            TG.iModel = insuranceModel_Education;
                        }
                        
                        break;
                    default:
                        break;
                }
           // }
        }
        
    }
    
}
@end
