//
//  CustomProductCell.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProductViewDelegate;
@interface CustomProductCell : UITableViewCell
@property(nonatomic,assign)BOOL statu;//按钮状态
@property(nonatomic,assign)BOOL isDefaultSelect;//是否默认选中
@property(nonatomic,assign)NSUInteger rowIndex;

@property (strong, nonatomic) IBOutlet UIButton *btnTitle;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,assign)int years;
@property(nonatomic,assign)float coverage;
@property(nonatomic,assign)float premium;
@property(nonatomic,assign)productOfInsurance productType;

@property (strong, nonatomic) IBOutlet UILabel *labelPremium;
@property (strong, nonatomic) IBOutlet UITextField *txtYears;
@property (strong, nonatomic) IBOutlet UITextField *txtCoverage;

- (IBAction)actionWithTitleButton:(UIButton *)sender;
@property(nonatomic,assign)id<ProductViewDelegate>delegate;
@end

@protocol ProductViewDelegate <NSObject>
-(void)showDidSelectRowDetailView:(NSUInteger)rowIndex;
-(void)showEditViewForTextField:(CustomProductCell *)xibView EditIndex:(int)index SelectRowIndex:(NSUInteger)rowIndex;

-(void)HandleSingleSelection:(CustomProductCell *)xibView;

@optional

@end