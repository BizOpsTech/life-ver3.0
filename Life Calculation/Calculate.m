//
//  Calculate.m
//  Life Calculate
//
//  Created by Snow on 11/14/13.
//  Copyright (c) 2013 RbBtSn0w. All rights reserved.
//

#import "Calculate.h"
#include <math.h>

@implementation Calculate



float calculateTraditionalLifeCoverage(float yearlyIncom, float yearyExpense, bool isRetiredOfMan, float currentAge, float endowmentInsurance, float life){
    
    float retiredAge = 60;
    if (!isRetiredOfMan) retiredAge = 55;
    
    float formulaA = (yearlyIncom-yearyExpense*0.5);
    float formulaB = (1+0.03);
    float previousResult = 0.0f;
    float fN = retiredAge - currentAge;
    
    for (int i=1; i<=fN; i++) {
        float currentResult = formulaA/powf(formulaB, i);
        previousResult = previousResult + currentResult;
    }
    
    return previousResult - endowmentInsurance - life;
}

float calculateAccidentCoverage(float yearlyIncom, float yearyExpense, bool isRetiredOfMan, float currentAge, float endowmentInsurance, float life, float accidentDiseaseProtection){
    
    float retiredAge = 60;
    if (!isRetiredOfMan) retiredAge = 55;
    
    float formulaA = (yearlyIncom-yearyExpense*0.5);
    float formulaB = (1+0.03);
    float previousResult = 0.0f;
    float fN = retiredAge - currentAge;
    
    for (int i=1; i<=fN; i++) {
        float currentResult = formulaA/powf(formulaB, i);
        previousResult = previousResult + currentResult;
    }
    
    return previousResult  - endowmentInsurance - life - accidentDiseaseProtection;
}

float calculateHealthCoverage(float cityType, float currentAge, float socialInsurance, float accidentDiseaseProtection){
    
    cityType = 1.0f;
    float formula = (1+0.03);
    float result = 500000 * cityType * powf(formula, (65-currentAge));
    
    return result - socialInsurance - accidentDiseaseProtection;
}


float calculateRetirementCoverage(float expectedAge,bool isRetiredOfMan,float currentAge,float yearlyExpenseNow, float endowmentInsurance){
    
    expectedAge = 81;
    float retiredAge = 60;
    if (!isRetiredOfMan) retiredAge = 55;
    
    float n1 = expectedAge - retiredAge;
    float n2 = retiredAge - currentAge;
    
    float k = 0.03f;
    float i = 0.04f;
    
    float x1 = powf(1+k, n2);
    
    float previousResult = 0.0f;
    float x2 = (1+k)/(1+i);
    
    for (int j=1; j<=n1; j++) {
        
        float currentResult = 0.8 * yearlyExpenseNow * x1 * powf(x2, j);
        
        previousResult = previousResult + currentResult;
    }
    
    return previousResult  - endowmentInsurance;
}




#pragma mark
#pragma mark        Tranditional Life

- (float)calculateTraditionalLifeCoverageByYearlyIncome:(float)fA withSelfYearlyExpense:(float)fB withIsRetiredOfMan:(BOOL)isRetiredOfMan  withCurrentAge:(float)fCA withEndowmentInsurance:(float)fEI withLife:(float)fLife{
    
    float result = calculateTraditionalLifeCoverage(fA, fB, isRetiredOfMan, fCA, fEI, fLife);
    
    return result > 0 ? result : 0;
}

#pragma mark    Accident

- (float)calculateAccidentCoverageByYearlyIncome:(float)fA withSelfYearlyExpense:(float)fB withIsRetiredOfMan:(BOOL)isRetiredOfMan  withCurrentAge:(float)fCA withEndowmentInsurance:(float)fEI withLife:(float)fLife withAD:(float)fADP{
    
    float result = calculateAccidentCoverage(fA, fB, isRetiredOfMan, fCA, fEI, fLife, fADP);
    
    return result > 0 ? result : 0;
}

#pragma mark    Health

- (float)calculateHealthCoverageByCityType:(float)fCT withCurrentAge:(float)fCA withSocialInsurance:(float)fSI withAD:(float)fADP{
    
    float result = calculateHealthCoverage(fCT, fCA, fSI, fADP);

    return result > 0 ? result : 0;
}

#pragma mark    Retirement
- (float)calculateRetirementCoverageByExpectedAge:(float)fEA withIsRetiredOfMan:(BOOL)isRetiredOfMan withCurrentAge:(float)fCA withYearlyExpenseNow:(float)fYEN withEndowmentInsurance:(float)fEI{
    
    float result = calculateRetirementCoverage(fEA, isRetiredOfMan, fCA, fYEN, fEI);
    
    return result > 0 ? result : 0;
}
#pragma mark    Children education
- (float)calculateChildrenEducationCoverageByChildsAge:(NSArray*)ages{
    
    
    float previousResult = 0.0f;
    
    float baseResult = 1+0.03;
    
    for (NSNumber *age in ages) {
        float currentChildAge = age.floatValue;
        float currentResult = powf(baseResult, (18-currentChildAge));
        previousResult = currentResult + currentResult;
    }
    
    float sumCoverage = 100000 * previousResult;
    
    return sumCoverage > 0 ? sumCoverage : 0;
}

- (float)calculateChildrenEducationCoverageByChilds:(NSUInteger)childsCount withCollageAge:(NSUInteger)collageAge withIsForeignCollage:(BOOL)isForeignCollage{
    
    if (collageAge == 0) return 0;
    
    float sumCoverage = (isForeignCollage ? 1000000:100000) * (powf((1+0.03), collageAge) * childsCount);
    
    return sumCoverage > 0 ? sumCoverage : 0;
    
}

@end
