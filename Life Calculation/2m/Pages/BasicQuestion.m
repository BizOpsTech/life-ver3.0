//
//  BasicQuestion.m
//  Life-insurance-v3
//
//  Created by BizOpsTech on 12/10/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "RingButtons.h"
#import "SliderWithImage.h"
#import "BasicQuestion.h"
#import "GuideView.h"
@interface BasicQuestion ()<RingButtonsDelegate,SliderChangeValueDelegate>
@property(nonatomic,strong)RingButtons *RB;
@property(nonatomic,strong)SliderWithImage *SWI;
@property(nonatomic,strong)AppDelegate *app;
@property(nonatomic)NSUInteger clickCount;

@property(nonatomic,strong)UIStyle *disAbleStyle;
@property(nonatomic,strong)UIStyle *enAbleStyle;
@property(nonatomic,strong)UICommon *UICommonStyle;
@end

@implementation BasicQuestion

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _app = [[UIApplication sharedApplication]delegate];
    [self configDefaultValue];
    [_btnConfirm setEnabled:NO];
}

#pragma mark 配置初始值
-(void)configDefaultValue{
    if(_app.recodDb.age == 0){
        _app.recodDb.age = 18;
    }
    
    if(_app.recodDb.gender == nil){
        Question *Qgender = [Question new];
        Qgender.selectIndex = 1;
        Qgender.answer = kFemale;
        _app.recodDb.gender = Qgender;
    }
    
    if(_app.recodDb.isMarried == nil){
        Question *QisMarried = [Question new];
        QisMarried.selectIndex = 1;
        QisMarried.answer = @"married";
        _app.recodDb.isMarried = QisMarried;
    }
    
    if(_app.recodDb.earnOfAfterTaxSelectIndex == 0){
        _app.recodDb.earnOfAfterTaxSelectIndex = 1;
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
#pragma mark 配置按钮样式
    UIColor *beginColor = [[UIColor alloc]initWithRed:86.f/255 green:151.f/255 blue:245.f/255 alpha:1.0];
    UIColor *endColor = [[UIColor alloc]initWithRed:36.f/255 green:91.f/255 blue:174.f/255 alpha:1.0];
    
    _enAbleStyle = [UIStyle new];
    _enAbleStyle.borderWidth = 1.0;
    _enAbleStyle.cornerRadius = 12.0;
    _enAbleStyle.borderColor = [UIColor blueColor];
    _enAbleStyle.gradientColorArr = [NSArray arrayWithObjects:(id)beginColor.CGColor,(id)endColor.CGColor,nil];
    
    
    UIColor *disbeginColor = [[UIColor alloc]initWithRed:185.f/255 green:185.f/255 blue:185.f/255 alpha:1.0];
    UIColor *disendColor = [[UIColor alloc]initWithRed:210.f/255 green:210.f/255 blue:210.f/255 alpha:1.0];
    _disAbleStyle = [UIStyle new];
    _disAbleStyle.borderWidth = 1.0;
    _disAbleStyle.cornerRadius = 12.0;
    _disAbleStyle.borderColor = [UIColor grayColor];
    _disAbleStyle.gradientColorArr = [NSArray arrayWithObjects:(id)disbeginColor.CGColor,(id)disendColor.CGColor,nil];
    
    _UICommonStyle = [UICommon new];
    [_UICommonStyle customButton:_btnBack buttonStyle:_enAbleStyle isReset:NO];
    [_UICommonStyle customButton:_btnConfirm buttonStyle:_disAbleStyle isReset:NO];
    
    
#pragma mark 加载环形按钮组
    _RB = [[RingButtons alloc]init];
    _RB.frame = CGRectMake(0,0,_subComponentView.frame.size.width,_subComponentView.frame.size.height);
    _RB.delegate = self;
    _RB.PrevButtonIndex = 6;
    [_RB buttonStateGroupInit];
    [_subComponentView addSubview:_RB];
    [_RB.delegate didSelectButton:RingButtonType_age pervSelectButton:RingButtonType_none];
}

-(void)viewDidAppear:(BOOL)animated{
#pragma mark 引导页面介入
    //如果是初次访问
    if(_app.isNotFirstVisitedForBasic == NO){
        GuideView *guide = [GuideView new];
        if([UIDevice isRunningOniPhone5]){
            guide.pages = @[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide1"]],
                        [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide2"]]];
        }else{
            guide.pages = @[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide4S1"]],
                            [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide4S2"]]];
        }
        [self.view addSubview:guide];
        _app.isNotFirstVisitedForBasic = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark 加载组件视图
-(void)loadComponentFromSelectButton:(RingButtonType)currentButton{
    _SWI = (SliderWithImage *)[self.view viewWithTag:2012];
    if(![_SWI isEqual:nil]){
        [_SWI removeFromSuperview];
        _SWI = nil;
    }
    
    _SWI = [[SliderWithImage alloc]init];
    _SWI.frame = CGRectMake(0, 0, 320, _mainComponentView.frame.size.height);
    _SWI.tag = 2012;
    _SWI.UIType = currentButton;
    _SWI.delegate = self;
    
    NSArray *titleArr = @[@"<2万",@"2万～10万",@"10万～50万",@"50万～500万",@"500万以上"];
    
    UIStyle *textStyle = [UIStyle new];
    textStyle.borderWidth = 1.0;
    textStyle.cornerRadius = 4.0;
    textStyle.borderColor = [UIColor lightGrayColor];
    [_UICommonStyle customTextField:_SWI.txt textStyle:textStyle];
    
    switch (currentButton) {
        case RingButtonType_age:
            _labelTitle.text= @"您的年龄是多少？";
            _SWI.itemTitele = @[@"18",@"60"];
            _SWI.itemImage = @[[UIImage imageNamed:@"age1"],[UIImage imageNamed:@"age2"],[UIImage imageNamed:@"age3"]];
            _SWI.visibleOptionValue = YES;
            _SWI.slider.minimumValue = 18.0;
            _SWI.slider.maximumValue = 60.0;
            _SWI.slider.value = _app.recodDb.age;
            _SWI.currentValue = _app.recodDb.age;
            _SWI.txt.text = [NSString stringWithFormat:@"%d",_app.recodDb.age];
            break;
        case RingButtonType_gender:
            _labelTitle.text= @"您的性别是？";
            _SWI.itemImage = @[[UIImage imageNamed:@"woman_1"],[UIImage imageNamed:@"man_1"]];
            _SWI.itemTitele = @[@"女性",@"男性"];
            _SWI.iconSize = @[@"56",@"160"];
            _SWI.visibleOptionValue = NO;
            _SWI.slider.minimumValue = 1;
            _SWI.slider.maximumValue = 2;
            _SWI.slider.value = _app.recodDb.gender.selectIndex;
            _SWI.currentValue = _app.recodDb.gender.selectIndex;
            break;
        case RingButtonType_marriage:
            _labelTitle.text= @"您的婚姻状况是怎样的？";
            _SWI.itemImage = @[[UIImage imageNamed:@"married"],[UIImage imageNamed:@"unmarried"]];
            _SWI.itemTitele = @[@"已婚",@"未婚"];
            _SWI.visibleOptionValue = NO;
            _SWI.slider.minimumValue = 1;
            _SWI.slider.maximumValue = 2;
            _SWI.slider.value = _app.recodDb.isMarried.selectIndex;
            _SWI.currentValue = _app.recodDb.isMarried.selectIndex;
            
            break;
        case RingButtonType_kids:
            _labelTitle.text= @"您有几个孩子需要抚养？";
            _SWI.icon.image = ImageName(@"kids");
            _SWI.itemTitele = @[@"0",@"5个以上"];
            _SWI.visibleOptionValue = YES;
            _SWI.slider.minimumValue = 0;
            _SWI.slider.maximumValue = 5;
            _SWI.slider.value = _app.recodDb.family.Kids;
            _SWI.txt.text = [NSString stringWithFormat:@"%d",_app.recodDb.family.Kids];            
            _SWI.sliderImageBg = @"SliderBaseBg2";
            
            break;
        case RingButtonType_famliy:
            _labelTitle.text= @"您有几个老人需要赡养？";
            _SWI.icon.image = ImageName(@"familys");
            _SWI.itemTitele = @[@"0",@"4个以上"];
            _SWI.visibleOptionValue = YES;
            _SWI.slider.minimumValue = 0;
            _SWI.slider.maximumValue = 4;
            _SWI.slider.value = _app.recodDb.family.Total;
            _SWI.txt.text = [NSString stringWithFormat:@"%d",_app.recodDb.family.Total];
            _SWI.sliderImageBg = @"SliderBaseBg";
            
            break;
        case RingButtonType_income:
            _labelTitle.text= @"您的年收入大概是多少？";
            _SWI.itemTitele = titleArr;
            _SWI.visibleOptionValue = YES;
            _SWI.slider.minimumValue = 1;
            _SWI.slider.maximumValue = 5;
            _SWI.sliderImageBg = @"SliderBaseBg";
            _SWI.slider.value = _app.recodDb.earnOfAfterTaxSelectIndex;
            _SWI.currentValue = _app.recodDb.earnOfAfterTaxSelectIndex;
             _SWI.txt.text = [titleArr objectAtIndex:_app.recodDb.earnOfAfterTaxSelectIndex-1];
            _SWI.iconSize = @[@"90",@"140"];
            
            break;
        default:
            _SWI.slider.minimumValue = 18.0;
            _SWI.slider.maximumValue = 60.0;
            _SWI.itemTitele = @[@"18",@"60"];
            _SWI.visibleOptionValue = NO;
            break;
    }
    [_mainComponentView addSubview:_SWI];

}



///////// RingButtonsDelegate
#pragma marke RingButtonsDelegate
-(void)didSelectButton:(RingButtonType)currentButton pervSelectButton:(RingButtonType)pervButton{
    //加载组件
    [self loadComponentFromSelectButton:currentButton];
    _clickCount++;
    if(_clickCount == 6){
        [_btnConfirm setEnabled:YES];
        [_UICommonStyle customButton:_btnConfirm buttonStyle:_enAbleStyle isReset:YES];
    }
    
}


#pragma mark SliderChangeValueDelegate
///////// SliderChangeValueDelegate
-(int)sliderAutoFixValue:(float)value{
    int fixValue = 0;
    
    if(value <=0.5){
        fixValue = 0;
    }else if(value > 0.5 && value <= 1.5){
        fixValue = 1;
    }else if(value > 1.5 && value <= 2.5){
        fixValue =2;
    }else if(value >2.5 && value <=3.5){
        fixValue =3;
    }else if(value >3.5 && value <=4.5){
        fixValue = 4;
    }else if(value > 4.5){
        fixValue = 5;
    }
    
    [_SWI.slider setValue:fixValue animated:YES];
    
    return fixValue;
}

#pragma mark 记录选项值
//拖拽完成
-(void)sliderValueChanged:(float)value UIType:(RingButtonType)State{
   // Question *myQuestion = [Question new];
    userFamily uf = _app.recodDb.family;
    
    switch (State) {
        case RingButtonType_age:
            [_RB setButtonStateFromIndex:0 State:2];
            _app.recodDb.age = (int)value;
            break;
            
        case RingButtonType_gender:
            [_RB setButtonStateFromIndex:1 State:2];
            [self sliderAutoFixValue:value];
            _app.recodDb.gender.selectIndex = [self sliderAutoFixValue:value];
            _app.recodDb.gender.answer = _app.recodDb.gender.selectIndex == 1? kFemale : kMale;
            break;
            
        case RingButtonType_marriage:
            [_RB setButtonStateFromIndex:2 State:2];
            _app.recodDb.isMarried.selectIndex = [self sliderAutoFixValue:value];
            _app.recodDb.isMarried.answer = _app.recodDb.isMarried.selectIndex == 1?@"married":@"unmarried";
            break;
            
        case RingButtonType_kids:
            [_RB setButtonStateFromIndex:3 State:2];
            uf.Kids = [self sliderAutoFixValue:value];
            _app.recodDb.family = uf;
            break;
            
        case RingButtonType_famliy:
            [_RB setButtonStateFromIndex:4 State:2];
            uf.Total = [self sliderAutoFixValue:value];
            _app.recodDb.family = uf;
            break;
            
        case RingButtonType_income:
            [_RB setButtonStateFromIndex:5 State:2];
            _app.recodDb.earnOfAfterTaxSelectIndex = [self sliderAutoFixValue:value];

            break;
        case RingButtonType_none:
            break;
    }
}


- (IBAction)actionWithButton:(UIButton *)sender {
    if([sender isEqual:_btnBack]){
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
}

@end
