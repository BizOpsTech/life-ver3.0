//
//  TabPagesF.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/12/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "TabPagesF.h"
@interface TabPagesF()<TabPageResultDelegate>
@property (strong,nonatomic) TraditionalLifeQuestion *surveyPage;
@end
@implementation TabPagesF


#pragma mark 推荐购买级别
-(buyModel)autoMarkForBuy:(UserDb *)source{
    int yearlyIncom = 0;
    switch (source.earnOfAfterTaxSelectIndex) {
        case 1:
            yearlyIncom = 15 * 1000;
            break;
        case 2:
            yearlyIncom = 60 * 1000;
            break;
        case 3:
            yearlyIncom = 200 * 1000;
            break;
        case 4:
            yearlyIncom = 1000 * 1000;
            break;
        case 5:
            yearlyIncom = 3000 * 1000;
            break;
        default:
            yearlyIncom = 15 * 1000;
            break;
    }

    if(source.earnOfAfterTax > 0){
        yearlyIncom = source.earnOfAfterTax;
    }
    
    buyModel myBuyModel = 0;
    if( (source.age <= 30 && yearlyIncom < 500000) || (source.age > 50 && yearlyIncom < 100000) || (source.age > 60 && (100000 <= yearlyIncom && yearlyIncom < 500000)))
    {
        myBuyModel = buyWithAccidentAndHospitalization;
    }
    
    if( (source.age <= 30 && yearlyIncom >= 500000) || ((30 < source.age && source.age <= 50) && yearlyIncom < 100000) || ((30 < source.age && source.age <= 35)  && (100000 <= yearlyIncom && yearlyIncom < 500000))|| ((50 < source.age && source.age <= 60)  && (100000 <= yearlyIncom && yearlyIncom < 500000)))
    {
        myBuyModel = buyWithLifeAndCriticalDisease;
        
    }
    
    if(((35 < source.age && source.age <= 50) && (100000 <= yearlyIncom && yearlyIncom < 5000000))||((30 < source.age && source.age <=35) && (500000 <= yearlyIncom && yearlyIncom < 5000000))){
        myBuyModel = buyWithLongTermSaving;
    }
    
    if(((35 < source.age && source.age <= 50) &&  yearlyIncom >= 5000000) || (source.age > 50 && yearlyIncom >= 500000)){
        myBuyModel = buyWithWealthManagement;
    }
    
    return myBuyModel;
}

#pragma mark 返回选择页面的视图
-(UIView *)showSelectPage:(tabPageType)page prevPage:(tabPageType)prev{
    UIView *showPage;
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    if(_surveyPage != nil && prev != tabPageQuestion && prev != tabPageResult){
        [_surveyPage SaveValues:app];
    }
    switch (page) {
        case tabPageLifeAndAccident:
            showPage = [self getLifePage];
        break;
            
        case tabPageHealth:
            showPage = [self getHealthPage];
        break;
            
        case tabPageRetirement:
            showPage = [self getRetirementPage];
        break;
            
        case tabPageCollege:
            showPage = [self getCollegePage];
        break;
            
        case tabPageResult:
            showPage = [self getResultPage:app.recodDb];
        break;
            
        default:
        break;
    }
    return showPage;
}

/*
 #define kTraditional_life @"Traditional_life"
 #define kAccident @"Accident"
 #define kHealth_insurance @"Health_insurance"
 #define kRetirement @"Retirement"
 #define kChildren_education @"Children_education"
 */
-(UIView *)getResultPage:(UserDb *)source{
    buyModel myBuyModel = [self autoMarkForBuy:source];
    
    handleCalculate * handle_Calculate = [[handleCalculate alloc]init];
    NSArray * arr = [handle_Calculate GetSortedArrObj:source];
    [self.delegate outputResultDataFormSubPage:arr BuyModel:myBuyModel];
    
    TabPageResult *pageView = [[TabPageResult alloc]init];
    pageView.gridData = arr;
    pageView.delegate = self;
    pageView.pointByModel = myBuyModel;
    return pageView;
}

-(UIView *)getLifePage{
    NSArray * dataSource = [QuestionData getLife_AccidentData];
    _surveyPage = [[TraditionalLifeQuestion alloc] init:dataSource InsuranceType:kTraditional_life];
    _surveyPage.LabTitle.text = @"人寿及意外险";
    return _surveyPage;
    
}

-(UIView *)getHealthPage{
    NSArray * dataSource = [QuestionData getHealthData];
    _surveyPage = [[TraditionalLifeQuestion alloc] init:dataSource InsuranceType:kHealth_insurance];
    _surveyPage.LabTitle.text = @"健康险";
    return _surveyPage;
}

-(UIView *)getRetirementPage{
    NSArray * dataSource = [QuestionData getRetirementData];
    _surveyPage = [[TraditionalLifeQuestion alloc] init:dataSource InsuranceType:kRetirement];
    _surveyPage.LabTitle.text = @"养老险";
    return _surveyPage;
}

-(UIView *)getCollegePage{
    NSArray * dataSource = [QuestionData getCollegeEducation];
    _surveyPage = [[TraditionalLifeQuestion alloc] init:dataSource InsuranceType:kChildren_education];
    _surveyPage.LabTitle.text = @"教育险";
    return _surveyPage;
}

////////////////////////// 结果页面的委托 ////////////////////////////////////
#pragma mark TabPageResultDelegate
-(void)didResultButtonTouchEnd{
    [self.delegate didButtonTouchEndFromSubPage:tabPageResult tagIndex:0];
}
-(void)didResultGridCellTouchEnd:(insuranceModel)celltype{
    [self.delegate didGridCellSelectFormSubPage:tabPageResult tagIndex:celltype];
}
@end
