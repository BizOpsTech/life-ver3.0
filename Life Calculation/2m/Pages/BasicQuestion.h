//
//  BasicQuestion.h
//  Life-insurance-v3
//
//  Created by BizOpsTech on 12/10/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "UICommon.h"
#import <UIKit/UIKit.h>

@interface BasicQuestion : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)actionWithButton:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *mainComponentView;
@property (strong, nonatomic) IBOutlet UIView *subComponentView;



@end