//
//  singleSelect.h
//  Created by bizopstech on 13-8-20.
//  Copyright (c) 2013年 com.youlong118. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol SigleSelectDelegate;
@interface singleSelect : UIView

//@property (nonatomic,assign) id<SigleSelectDelegate> delegate;
@property (nonatomic,strong) NSArray * MuSelData;
@property (strong,nonatomic) NSString * additionInfo;
@property (strong,nonatomic) NSString * selectedValue;
@property (assign, nonatomic) NSInteger  selectedIndex;


@property (assign,nonatomic) NSInteger singleSelectNumInLine;

-(void) addSubViewControl;

@end


//@protocol SigleSelectDelegate <NSObject>

//- (void)selected:(singleSelect*)ss withIndex:(NSInteger)atNumber;

//@end