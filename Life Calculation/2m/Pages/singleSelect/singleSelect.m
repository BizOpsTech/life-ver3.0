//
//  singleSelect.m
//  Created by bizopstech on 13-8-20.
//  Copyright (c) 2013年 com.youlong118. All rights reserved.
//

#import "singleSelect.h"

@interface singleSelect(){
     float countWidth;
     float countHieght;
     float intervalWidth;
     NSMutableArray * selectItems;
}
@end


@implementation singleSelect

@synthesize MuSelData = _MuSelData;
@synthesize additionInfo = _additionInfo;
@synthesize selectedValue = _selectedValue;
@synthesize selectedIndex = _selectedIndex;

@synthesize singleSelectNumInLine = _singleSelectNumInLine;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        selectItems = [NSMutableArray arrayWithCapacity:6];
        countWidth = 0;
        countHieght = 0;
        intervalWidth = 10; //间隔
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
*/

-(void)addSubViewControl{
     [self layout0];
}


-(void)checkboxClick:(UIButton *)btn
{
    if(btn.selected == NO){
        btn.selected = !btn.selected;
        self.selectedValue = btn.currentTitle;
        
    }
    for(int i=0; i < selectItems.count; i++)
    {
        UIButton * thisbtn = [selectItems objectAtIndex:i];
        if([thisbtn isEqual:btn] == NO)
        {
            thisbtn.selected = NO;
        }
        else
        {
             self.selectedIndex = i+1;
        }
    }
}


- (void)drawRect:(CGRect)rect
{
}


-(void) layout0{
    int selItemWidth = 0;
    //int textAlign = 0;
    if(_singleSelectNumInLine < 3)
    {
        selItemWidth = (int)(self.frame.size.width - intervalWidth * 1) / 2;
    }else{
        selItemWidth = (int)(self.frame.size.width - intervalWidth * 2) / 3;
    }
    
    for(int i=0; i < self.MuSelData.count; i++)
    {
        UIButton *checkbox = [UIButton buttonWithType:UIButtonTypeCustom];

        [checkbox setImage:[UIImage imageNamed:@"UICheckBoxUn.png"] forState:UIControlStateNormal];
        [checkbox setImage:[UIImage imageNamed:@"UICheckBoxSel.png"] forState:UIControlStateSelected];

        [checkbox.titleLabel setFont: [UIFont systemFontOfSize:12.0f]];
        [checkbox setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
        [checkbox setTitleColor:[UIColor blackColor]forState:UIControlStateSelected];
        
        checkbox.titleLabel.numberOfLines = 0;
        checkbox.titleLabel.textAlignment = NSTextAlignmentLeft;

        checkbox.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
        checkbox.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //checkbox.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        
        [checkbox setTitle:[_MuSelData objectAtIndex:i] forState:UIControlStateNormal];
        [checkbox setTitle:[_MuSelData objectAtIndex:i] forState:UIControlStateSelected];
        
        if(i > 0 && i % _singleSelectNumInLine == 0)
        {
            countHieght += 34;
            countWidth = 0;
        }
        CGRect checkboxRect = CGRectMake(countWidth,countHieght,selItemWidth,34);
        [checkbox setFrame:checkboxRect];
        //[checkbox setBackgroundColor: [UIColor redColor]];
        //[checkbox sizeToFit];
        [checkbox addTarget:self action:@selector(checkboxClick:) forControlEvents:UIControlEventTouchUpInside];
        
        if(self.selectedIndex > 0 && (int)self.selectedIndex == (i+1))
        {
            checkbox.selected = YES;
        }
        countWidth += selItemWidth + intervalWidth;
        [self addSubview:checkbox];
        [selectItems addObject:checkbox];
    }
}



-(void)InsertAddition:(BOOL)showAddition
{
    if(showAddition == YES)
    {
//        self.BtncallDriverBox = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.BtncallDriverBox setFrame:CGRectMake(0,self.countHieght+37,self.frame.size.width,30)];
//        [self.BtncallDriverBox setBackgroundColor:[UIColor whiteColor]];
//        //[self.BtncallDriverBox addTarget:self action:@selector(ShowBox) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:_BtncallDriverBox];
//        
//        UIImage * image = [UIImage imageNamed:@"driverIcon.png"];
//        self.imageIcon = [[UIImageView alloc] initWithImage:image];
//        self.imageIcon.center = CGPointMake(25, self.countHieght+53);
//        [self addSubview:_imageIcon];
//        
//        self.additionStr = [[UILabel alloc] init];
//        self.additionStr.textColor = [UIColor blueColor];
//        self.additionStr.font = [UIFont systemFontOfSize:12];
//        self.additionStr.frame = CGRectMake(50, self.countHieght+43, 200, 20);
//        self.additionStr.text = self.additionInfo;
//        [self addSubview:self.additionStr];
    }
}

@end
