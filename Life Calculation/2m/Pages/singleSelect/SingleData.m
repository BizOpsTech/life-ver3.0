//
//  SingleData.m
//  TwoPage
//
//  Created by bizopstech on 13-10-29.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "SingleData.h"

@implementation SingleData

@synthesize questionStr = _questionStr;
@synthesize selectData = _selectData;
@synthesize valueCollection = _valueCollection;
@synthesize additionInfo = _additionInfo;
@synthesize selectedItemIndex = _selectedItemIndex;

@end
