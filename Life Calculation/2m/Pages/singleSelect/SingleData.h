//
//  SingleData.h
//  TwoPage
//
//  Created by bizopstech on 13-10-29.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingleData : NSObject

@property (strong,nonatomic) NSString * questionStr;
@property (strong,nonatomic) NSArray * selectData;
@property (strong,nonatomic) NSArray * valueCollection;
@property (strong,nonatomic) NSString * additionInfo;
@property (assign,nonatomic) NSInteger selectedItemIndex;


@end
