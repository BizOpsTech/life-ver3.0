//
//  TabPageResultCell.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/12/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "TabPageResultCell.h"
@interface TabPageResultCell()
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelCoverage;
@property (strong, nonatomic) IBOutlet UIButton *btn;
@end

@implementation TabPageResultCell
@synthesize title = _title;
@synthesize coverage = _coverage;
@synthesize visibleButton = _visibleButton;
@synthesize cellType = _cellType;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTitle:(NSString *)title{
    self.labelTitle.text = title;
}

-(void)setCoverage:(NSNumber *)coverage{
    NSNumberFormatter *dataFormat = [[NSNumberFormatter alloc]init];
    dataFormat.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.labelCoverage.text = [NSString stringWithFormat:@"￥%@",[dataFormat stringFromNumber:coverage]];
}

-(void)setVisibleButton:(BOOL)visibleButton{
    self.btn.hidden = !visibleButton;
}

- (IBAction)actionWithButton:(UIButton *)sender {
    [self.delegate didResultCellButtonTouchEnd:self.cellType];
}

@end