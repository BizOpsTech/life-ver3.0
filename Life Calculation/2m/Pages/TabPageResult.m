//
//  TabPageResult.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/12/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "TabPageResult.h"
@interface TabPageResult()<TabPageResultCellDelegate>
@property (strong, nonatomic) IBOutlet UIView *levelMap;
@property (strong, nonatomic) IBOutlet UILabel *labelMark;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property(nonatomic,assign)int sum;
@end

@implementation TabPageResult
@synthesize gridData = _gridData;
@synthesize pointByModel = _pointByModel;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"TabPageResult" owner:self options:nil] lastObject];
        if (self) {
            self.grid.delegate = self;
            self.grid.dataSource = self;
            if([[[UIDevice currentDevice]systemVersion]floatValue]>=7){
                self.grid.separatorInset = UIEdgeInsetsZero;
            }
        }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
 
}
*/


-(void)setPointByModel:(buyModel)pointByModel{
    CGFloat offsetY = 0;
    NSString *textPartA = @"根据测试，您的保险购买策略应侧重于";
    NSString *textPartB;
    
    switch (pointByModel) {
        case buyWithAccidentAndHospitalization:
            offsetY = 190;
            textPartB = @"意外及住院医疗";
            break;
        case buyWithLifeAndCriticalDisease:
            offsetY = 128;
            textPartB = @"寿险及重疾险";
            break;
        case buyWithLongTermSaving:
            offsetY = 72;
            textPartB = @"长期储蓄";
            break;
        case buyWithWealthManagement:
            offsetY = 16;
            textPartB = @"财富管理";
            break;
        default:
            
            break;
    }
    
    
    _labelMark.text = [NSString stringWithFormat:@"%@%@",textPartA,textPartB];
    NSDictionary* attributes = @{
                                 NSUnderlineStyleAttributeName:[NSNumber numberWithInt: NSUnderlineStyleSingle],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]
                                 };
    NSMutableAttributedString* aString = [[NSMutableAttributedString alloc] initWithAttributedString:_labelMark.attributedText];
    [aString addAttributes:attributes range:NSMakeRange(textPartA.length,_labelMark.text.length-textPartA.length )];
   _labelMark.attributedText = aString;
    _labelMark.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:12];
    
    UIImage *pin = [UIImage imageNamed:@"localPoint"];
    UIImageView *pinView = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY, 26, 26)];
    pinView.image = pin;
    [self.levelMap addSubview:pinView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.gridData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat rowHeight = 40;
    if([UIDevice isRunningOniPhone5]) rowHeight = 44;
    return rowHeight;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    TabPageResultCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"TabPageResultCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    ResultBase *celldb = [_gridData objectAtIndex:[indexPath row]];
    cell.title = celldb.name;
    cell.coverage = [NSNumber numberWithFloat:celldb.unadjusted_Coverage];
    cell.cellType = celldb.InsuranceType;
    cell.visibleButton = celldb.IshavePreciseUnadjusted_Coverage;
    cell.delegate = self;
    
    if(celldb.IshavePreciseUnadjusted_Coverage){_sum++;}
    if(_sum == _gridData.count){
        //_nextButton.hidden = NO;
        [_nextButton setEnabled:YES];
    }else{
        //_nextButton.hidden = YES;
        [_nextButton setEnabled:NO];
    }
    
    return cell;
}

- (IBAction)actionWithButton:(id)sender {
    [self.delegate didResultButtonTouchEnd];
}
-(void)didResultCellButtonTouchEnd:(insuranceModel)celltype{
    [self.delegate didResultGridCellTouchEnd:celltype];
}
@end
