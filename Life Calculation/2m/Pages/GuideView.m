//
//  GuideView.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/26/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//
#import "UIDevice+Resolutions.h"
#import "GuideView.h"
@interface GuideView(){
    int page;
    int countPage;
}
@property (strong, nonatomic) IBOutlet UIScrollView *gsv;
@property (strong, nonatomic) IBOutlet UIPageControl *pc;
@property (strong, nonatomic) IBOutlet UIButton *exitButton;
@end

@implementation GuideView
@synthesize pages = _pages;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"GuideView" owner:self options:nil] lastObject];
     if(self){
         if(![UIDevice isRunningOniPhone5]){
             self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 320, 460);
             self.gsv.frame = self.frame;
         }
     }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)buttonStyle{
    
    UIColor *beginColor = [[UIColor alloc]initWithRed:86.f/255 green:151.f/255 blue:245.f/255 alpha:1.0];
    UIColor *endColor = [[UIColor alloc]initWithRed:36.f/255 green:91.f/255 blue:174.f/255 alpha:1.0];
    UIStyle *enAbleStyle = [UIStyle new];
    enAbleStyle.borderWidth = 1.0;
    enAbleStyle.cornerRadius = 8.0;
    enAbleStyle.borderColor = [UIColor blueColor];
    enAbleStyle.gradientColorArr = [NSArray arrayWithObjects:(id)beginColor.CGColor,(id)endColor.CGColor,nil];
    
    UICommon *UI = [UICommon new];
    [UI customButton:_exitButton buttonStyle:enAbleStyle isReset:NO];
    [_exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


-(void)setPages:(NSArray *)pages{
    [self buttonStyle];
    countPage = pages.count;
    page = 1;
    self.pc.numberOfPages = pages.count;
    self.pc.currentPage = 0;
    
    //页面装载
    CGFloat sw = 0;
    for(NSUInteger i=0; i<pages.count; i++){
        sw += self.gsv.frame.size.width;
        UIImageView *img = pages[i];
        CGRect imgRect = CGRectMake(i*self.gsv.frame.size.width,
                                    self.gsv.frame.origin.y,
                                    self.gsv.frame.size.width,
                                    self.gsv.frame.size.height);
        img.frame = imgRect;
        [self.gsv addSubview:img];
    }
    
    [self.gsv setContentSize:CGSizeMake(sw, [self.gsv bounds].size.height)];
}

-(void)clear{
    [UIView animateWithDuration:0.5 animations:^{
        [self setAlpha:0.0];
    }];
    [self removeFromSuperview];
}

//下一单元
-(void)turnNext:(int)pagenumber{
    int currentPage = pagenumber-1;
    self.pc.currentPage = currentPage;
    CGRect vRect = CGRectMake(currentPage*self.gsv.frame.size.width,
                                self.gsv.frame.origin.y,
                                self.gsv.frame.size.width,
                                self.gsv.frame.size.height);
    [self.gsv scrollRectToVisible:vRect animated:YES];
}

- (IBAction)exitGuidePage:(UIButton *)sender {
    if(page == countPage){
        [self clear];
    }else{
        page++;
        [self turnNext:page];
    }
}
@end
