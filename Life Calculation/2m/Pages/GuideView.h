//
//  GuideView.h
//  Life Calculation
//
//  Created by BizOpsTech on 12/26/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICommon.h"
@interface GuideView : UIView
@property(nonatomic,strong)NSArray *pages;
@end
