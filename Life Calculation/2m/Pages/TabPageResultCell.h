//
//  TabPageResultCell.h
//  Life Calculation
//
//  Created by BizOpsTech on 12/12/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TabPageResultCellDelegate<NSObject>
-(void)didResultCellButtonTouchEnd:(insuranceModel)celltype;
@end

@interface TabPageResultCell : UITableViewCell
@property(nonatomic,copy)NSString *title;
@property(nonatomic,assign)NSNumber *coverage;
@property(nonatomic)BOOL visibleButton;
@property(nonatomic)insuranceModel cellType;
@property(nonatomic,strong)id<TabPageResultCellDelegate>delegate;
@end
