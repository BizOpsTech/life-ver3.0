//
//  TabPageResult.h
//  Life Calculation
//
//  Created by BizOpsTech on 12/12/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//
#import "UIDevice+Resolutions.h"
#import "ResultBase.h"
#import "TabPageResultCell.h"
#import <UIKit/UIKit.h>

@protocol TabPageResultDelegate<NSObject>
-(void)didResultButtonTouchEnd;
-(void)didResultGridCellTouchEnd:(insuranceModel)celltype;
@end

@interface TabPageResult : UIView<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *gridData;
@property (strong, nonatomic) IBOutlet UITableView *grid;
@property(nonatomic)buyModel pointByModel;
@property(nonatomic,assign)id<TabPageResultDelegate>delegate;
@end
