//
//  TabPagesF.h
//  Life Calculation
//
//  Created by BizOpsTech on 12/12/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//
#import "AppDelegate.h"
#import "calculatePriorityfor2Minute.h"
#import "handleCalculate.h"
#import "singleSelect.h"
#import "TraditionalLifeQuestion.h"
#import "QuestionData.h"



#import "TabPageResult.h"
#import <Foundation/Foundation.h>

@protocol TabPagesDelegate<NSObject>
-(void)didButtonTouchEndFromSubPage:(tabPageType)page tagIndex:(NSUInteger)index;
-(void)didGridCellSelectFormSubPage:(tabPageType)page tagIndex:(NSUInteger)index;
-(void)outputResultDataFormSubPage:(NSArray *)gridData BuyModel:(buyModel)model;
@end

@interface TabPagesF : NSObject
-(UIView *)showSelectPage:(tabPageType)page prevPage:(tabPageType)prev;
@property(nonatomic,assign)id<TabPagesDelegate>delegate;

@end
