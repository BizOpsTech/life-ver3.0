//
//  TreeGrid2M.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/13/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
/*
 self.LifeData = [NSArray arrayWithObjects:
 @{@"title":@"Yearly net income"},
 @{@"title":@"Years to come before retirement"},
 @{@"title":@"Current life insurance"},
 @{@"title":@"Current endowment insurance"},
 nil];
 self.AccidentData = [NSArray arrayWithObjects:
 @{@"title":@"Yearly net income"},
 @{@"title":@"Years to come before retirement"},
 @{@"title":@"Current life insurance"},
 @{@"title":@"Current accident insurance"},
 @{@"title":@"Current endowment insurance"},
 nil];
 self.HealthData = [NSArray arrayWithObjects:
 @{@"title":@"Avg, spending for critical disease"},
 @{@"title":@"Years to come before 65 years old"},
 @{@"title":@"Social insurance"},
 @{@"title":@"Current accident insurance"},
 nil];
 
 self.PensionData = [NSArray arrayWithObjects:
 @{@"title":@"Yearly spending"},
 @{@"title":@"Years to come before retirement"},
 @{@"title":@"Average years to live after retirement"},
 @{@"title":@"Current endowment insurance"},
 nil];
 
 self.EducationData = [NSArray arrayWithObjects:
 @{@"title":@"Current college tuition fee"},
 @{@"title":@"Number of kids to support"},
 @{@"title":@"Years to come before college"},
 nil];
 */
//
#import "AppDelegate.h"
#import "treeCell2.h"
#import "TreeGrid2M.h"
#import "TreeDataForTwoM.h"
#import "ResultBase.h"

@interface TreeGrid2M ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *grid;
@property (strong, nonatomic) IBOutlet UILabel *pageTitle;
@property (nonatomic,strong) NSArray *LifeData;
@property (nonatomic,strong) NSArray * HealthData;
@property (nonatomic,strong) NSArray * PensionData;
@property (nonatomic,strong) NSArray * EducationData;
@property(nonatomic,strong) AppDelegate *app;

@property (nonatomic,strong) NSArray * CurrentData;
@property (nonatomic,strong) NSArray * CurrentValueData;
@property (nonatomic,strong) TreeDataForTwoM * tree_Data;

@property(nonatomic,strong)NSNumberFormatter *dataFormat;
@end

@implementation TreeGrid2M

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _dataFormat = [[NSNumberFormatter alloc]init];
    _dataFormat.numberStyle = NSNumberFormatterDecimalStyle;
    
    _pageTitle.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:18];
    
    
    [self pageData];
    [self selectPageDataFromModel:_iModel];
    [self setFontToItemLabel];
    
    self.grid.delegate = self;
    self.grid.dataSource = self;
}

-(void)pageData{
    self.LifeData = [NSArray arrayWithObjects:
                     @{@"title":@"每年纯收入"},
                     @{@"title":@"退休前工作年限"},
                     @{@"title":@"当前已有保障"},
                     nil];

    self.HealthData = [NSArray arrayWithObjects:
                       @{@"title":@"重疾的平均花费"},
                       @{@"title":@"距离65岁的年限"},
                       @{@"title":@"社保余额"},
                       @{@"title":@"当前已有的重疾保额"},
                       nil];
    
    self.PensionData = [NSArray arrayWithObjects:
                        @{@"title":@"每年花费"},
                        @{@"title":@"退休前工作年限"},
                        @{@"title":@"退休后的预期生活年限"},
                        @{@"title":@"当前已有养老保障"},
                        nil];
    
    self.EducationData = [NSArray arrayWithObjects:
                          @{@"title":@"当前大学学费"},
                          @{@"title":@"需要供养的小孩数量"},
                          @{@"title":@"距离上大学年限"},
                          nil];
 
    _app = [[UIApplication sharedApplication]delegate];
    _tree_Data = [[TreeDataForTwoM alloc] init:_app.recodDb];
    
}

-(void)selectPageDataFromModel:(insuranceModel)model{
    switch (model) {
        case insuranceModel_Traditional:
            _pageTitle.text = @"人寿和意外险";
            _CurrentData = _LifeData;
            _CurrentValueData = _tree_Data.LifeTreeValues;
            break;
        case insuranceModel_Healthy:
            _pageTitle.text = @"健康险";
            _CurrentData = _HealthData;
            _CurrentValueData = _tree_Data.HealthTreeValues;
            break;
        case insuranceModel_Pension:
            _pageTitle.text = @"养老险";
            _CurrentData = _PensionData;
            _CurrentValueData = _tree_Data.PensionTreeValues;
            break;
        case insuranceModel_Education:
            _pageTitle.text = @"教育险";
            _CurrentData = _EducationData;
            _CurrentValueData = _tree_Data.EducationTreeValues;
            break;
        default:
            break;
    }
    
}

-(void)setFontToItemLabel{
    UIFont *Font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:12];
    for(NSUInteger i=20; i<28;i++){
        UILabel *tempLabel = (UILabel*)[self.view viewWithTag:i];
        tempLabel.font = Font;
        if(i>23){
            ResultBase *DB;
            for(NSUInteger n = 0; n < self.source.count; n++){
                DB = [self.source objectAtIndex:n];
                if(i==24 && DB.InsuranceType == insuranceModel_Traditional){
                    
                    [self setValueToNumberLabel:tempLabel numberValue:DB.unadjusted_Coverage];
                    break;
                }else if(i==25 && DB.InsuranceType == insuranceModel_Healthy){
                    
                    [self setValueToNumberLabel:tempLabel numberValue:DB.unadjusted_Coverage];
                    break;
                }else if(i==26 && DB.InsuranceType == insuranceModel_Pension){
                    
                    [self setValueToNumberLabel:tempLabel numberValue:DB.unadjusted_Coverage];
                    break;
                }else if(i==27 && DB.InsuranceType == insuranceModel_Education){
                    
                    [self setValueToNumberLabel:tempLabel numberValue:DB.unadjusted_Coverage];
                    break;
                }
                
            }
        }
    }
}

-(void)setValueToNumberLabel:(UILabel*)label numberValue:(float)value{
    NSString *formatValue = [_dataFormat stringFromNumber:[NSNumber numberWithInt:(int)value/1000]];
    label.text = [NSString stringWithFormat:@"%@k",formatValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _CurrentData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    treeCell2 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"treeCell2" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    int index = (int)indexPath.row;
    if(index % 2 == 0)
    {
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    cell.titleLabel.text = [[_CurrentData objectAtIndex:index] objectForKey:@"title"];
    NSArray * arr = [[_CurrentValueData objectAtIndex:index] componentsSeparatedByString:@" "];
    cell.numberLabel.text = [arr objectAtIndex:0];
    if(arr.count > 1){
        cell.unitLabel.text = [arr objectAtIndex:1];
    }

    return cell;
}

-(void)pageModelChanged:(NSInteger)tagIndex{
    switch (tagIndex) {
        case 201:
            _iModel = insuranceModel_Traditional;

            break;
        case 202:
            _iModel = insuranceModel_Healthy;

            break;
        case 203:
            _iModel = insuranceModel_Pension;

            break;
        case 204:
            _iModel = insuranceModel_Education;

            break;
    }
    
    [self selectPageDataFromModel:_iModel];
    [_grid reloadData];
}


- (IBAction)selectOptionFromButton:(UIButton *)sender {
    NSInteger tagIndex = sender.tag;
    [self pageModelChanged:tagIndex];
}
- (IBAction)turnPageWithButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
