//
//  TabPageView.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/11/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//
//#import "TabPageResult.h"
#import "AppDelegate.h"
#import "Summary.h"
#import "Recommended.h"
#import "TreeGrid2M.h"
#import "TabPageView.h"
#import "GuideView.h"

@interface TabPageView ()<TabPagesDelegate>
@property(strong,nonatomic)AppDelegate *app;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (strong, nonatomic) IBOutlet UIView *wallPaperView;
@property (strong, nonatomic) IBOutlet UIButton *btnBasic;
@property (strong, nonatomic) IBOutlet UIButton *btnLife;
@property (strong, nonatomic) IBOutlet UIButton *btnHealth;
@property (strong, nonatomic) IBOutlet UIButton *btnRetirement;
@property (strong, nonatomic) IBOutlet UIButton *btnCollege;
@property (strong, nonatomic) IBOutlet UIButton *btnResult;
@property(strong,nonatomic)UIView *PrevPage;
@property(nonatomic)tabPageType localPage;

@property (strong, nonatomic) IBOutlet UIView *toolbarView;
@property(strong,nonatomic)TabPagesF *PF;
@property(strong,nonatomic)NSArray* ToolBtnArr;
////////////////////// 推荐页面需要的参数 /////////////////////////////
@property(nonatomic)buyModel myBuyModel;
@property(nonatomic,assign)NSArray *dataArr;
@end

@implementation TabPageView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _app = [[UIApplication sharedApplication]delegate];
    _ToolBtnArr = @[_btnLife,_btnHealth,_btnRetirement,_btnCollege];
    
    self.PF = [TabPagesF new];
    self.PF.delegate = self;
    
    [self showPage:tabPageResult];
    self.loading.hidesWhenStopped = YES;
    UICommon *UIStyle = [UICommon new];
    [UIStyle gradientConfig:_toolbarView ColorArr:@[(id)[UIColor whiteColor].CGColor,(id)[UIColor lightGrayColor].CGColor]];
    
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidAppear:(BOOL)animated{
#pragma mark 引导页面介入
    //如果是初次访问
    if(_app.isNotFirstVisitedForSurvey == NO){
        GuideView *guide = [GuideView new];
        if([UIDevice isRunningOniPhone5]){
            guide.pages = @[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide4"]],
                        [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide5"]],
                        [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide3"]]
                        ];
        }else{
            guide.pages = @[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide4S4"]],
                            [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide4S5"]],
                            [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide4S3"]]
                            ];
        }
        [self.view addSubview:guide];
        _app.isNotFirstVisitedForSurvey = YES;
    }
}


-(void)showPage:(tabPageType)page{
    if(page != self.localPage){
        UIView *currentPage = [_PF showSelectPage:page prevPage:self.localPage];

        
        if(_PrevPage != nil){
            [_PrevPage removeFromSuperview];
            _PrevPage = nil;
        }
        
        _PrevPage = currentPage;
        [_wallPaperView addSubview:_PrevPage];
        
        self.localPage = page;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionToSelectPage:(UIButton *)sender {
    if([sender isEqual:_btnBasic]){
        [self.navigationController popViewControllerAnimated:YES];
    }else if([sender isEqual:_btnLife]){
            [self showPage:tabPageLifeAndAccident];
    }else if([sender isEqual:_btnHealth]){
            [self showPage:tabPageHealth];
    }else if([sender isEqual:_btnRetirement]){
            [self showPage:tabPageRetirement];
       
    }else if([sender isEqual:_btnCollege]){
        [self showPage:tabPageCollege];
    }else{
       [self showPage:tabPageResult];
    }
    [self ToolButtonStateChanged:sender];
}

-(void)ToolButtonStateChanged:(UIButton*)Button{
    for(NSUInteger i=0; i<_ToolBtnArr.count; i++){
        UIButton *tempbtn = _ToolBtnArr[i];
        if([tempbtn isEqual:Button]){
            [self animationForButton:tempbtn origin:4];
        }else{
            [self animationForButton:tempbtn origin:14];
        }
    }
}

-(void)animationForButton:(UIButton *)button origin:(CGFloat)y{
    button.translatesAutoresizingMaskIntoConstraints = YES;
    [UIView animateWithDuration:0.2 animations:^{
        button.frame = CGRectMake(button.frame.origin.x, y, button.frame.size.width, button.frame.size.height);
        
    }];
}

#pragma mark TabPagesF delegate
//子页面的按钮选中后
-(void)didButtonTouchEndFromSubPage:(tabPageType)page tagIndex:(NSUInteger)index{
    switch (page) {
        case tabPageResult:
            //gotoRecommendFrom2M;
            [self performSegueWithIdentifier:@"gotoRecommendFrom2M" sender:nil];
        break;
        case tabPageLifeAndAccident:
            break;
        case tabPageHealth:
            break;
        case tabPageRetirement:
            break;
        case tabPageCollege:
            break;     
        default:
            break;
    }
}

//子页面的tableViewCell 选中后
-(void)didGridCellSelectFormSubPage:(tabPageType)page tagIndex:(NSUInteger)index{
    switch (page) {
        case tabPageResult:
                //goto2MTree;
            [self performSegueWithIdentifier:@"goto2MTree" sender:[NSNumber numberWithInteger:index]];
            break;
        case tabPageLifeAndAccident:
            break;
        case tabPageHealth:
            break;
        case tabPageRetirement:
            break;
        case tabPageCollege:
            break;
        default:
            break;
    }
}

-(void)outputResultDataFormSubPage:(NSArray *)gridData BuyModel:(buyModel)model{
    self.dataArr = gridData;
    self.myBuyModel = model;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"gotoRecommendFrom2M"]) {

        //重新计算算调整后保额
        Summary *SD = [Summary new];
        AppDelegate *app = [[UIApplication sharedApplication]delegate];
        NSMutableArray* source = [SD getInsuranceResult:app.recodDb ResultBaseArr:self.dataArr];
        ResultBase *tempdb;
        for(NSUInteger i=0; i< source.count; i++){
            tempdb = [source objectAtIndex:i];
            if(tempdb.InsuranceType == insuranceModel_Accidental){
                [source removeObjectAtIndex:i];
                break;
            }
        }
        
        Recommended *vc = [segue destinationViewController];
        vc.myBuyModel = self.myBuyModel;
        vc.gridDataArr = source;
        self.navigationController.navigationBarHidden = NO;
        
    }else if([segue.identifier isEqualToString:@"goto2MTree"]){
        UINavigationController *nac  = segue.destinationViewController;
        TreeGrid2M *TG = nac.viewControllers[0];
        TG.iModel = [(NSNumber*)sender integerValue];
        TG.source = self.dataArr;
        
    }
}
@end