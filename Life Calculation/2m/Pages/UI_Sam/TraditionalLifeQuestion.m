//
//  TraditionalLifeQuestion.m
//  testRingBtn
//
//  Created by bizopstech on 13-12-12.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "TraditionalLifeQuestion.h"
#import "SingleSelCell.h"


@interface TraditionalLifeQuestion()<UITableViewDelegate,UITableViewDataSource>
    @property (strong,nonatomic) NSArray * dataArr;
    @property (strong,nonatomic) NSString * InsuranceType;
    @property (nonatomic,strong) NSMutableArray * arrayAllThoseIndexPathes;
@end

@implementation TraditionalLifeQuestion

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (instancetype)init:(NSArray *)arr InsuranceType:(NSString *)type{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"TraditionalLife" owner:self options:nil] lastObject];
    
    if (self) {
        _dataArr = arr;
        _table_View.delegate = self;
        _table_View.dataSource = self;
        _InsuranceType = type;
        _arrayAllThoseIndexPathes = [NSMutableArray arrayWithCapacity:5];
        if(![UIDevice isRunningOniPhone5]){
            CGFloat x = 0;
            CGFloat y = 45;
            CGFloat w = 320;
            CGFloat h = 350;
            
            self.table_View.translatesAutoresizingMaskIntoConstraints = YES;
            self.table_View.frame = CGRectMake(x, y, w, h);
        }
    }
    return self;
}


-(float) calculateRowHeight:(NSArray *)arr{
    
    NSString * question = arr[1];
    float rowHeight = 70.0f;
    
    if(question.length > 47){
        rowHeight += 15;
    }
    
    if([arr[0] isEqualToString:@"singleSel"] == YES)
    {
        NSArray * array = arr[2];
        int rowNum = ceilf(array.count / [arr[4] integerValue]);
        if(rowNum == 2){
            rowHeight += rowNum * 12.0;
        }else if (rowNum == 3){
             rowHeight += rowNum * 20.0;
        }
    }else{
        rowHeight += 10;
    }
    return rowHeight;
}





#pragma mark tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray * arr = [_dataArr objectAtIndex:indexPath.row];
    
    return [self calculateRowHeight:arr];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dataArr count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
   
    SingleSelCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"SingleSelCell" bundle:nil];
        [_table_View registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [_table_View dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CGFloat rowHeight = [self calculateRowHeight:[_dataArr objectAtIndex:indexPath.row]];
    [cell LoadUIAndData: [_dataArr objectAtIndex:indexPath.row] RowHeight:rowHeight];
    [_arrayAllThoseIndexPathes addObject:indexPath];
    return cell;
}


-(void)SaveValues:(AppDelegate *)app{
    //AppDelegate *app = [[UIApplication sharedApplication]delegate];
   
    //switch (_InsuranceType) {
    if([_InsuranceType isEqualToString: kTraditional_life]){
            
            for(int i=0; i< _arrayAllThoseIndexPathes.count; i++)
            {
                SingleSelCell *cell = (SingleSelCell *)[_table_View cellForRowAtIndexPath:[_arrayAllThoseIndexPathes objectAtIndex:i]];
                switch (i) {
                    case 0:
                        app.recodDb.isSmoke = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                        break;
                    case 1:
                        app.recodDb.travel = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                        break;
                    case 2:
                        app.recodDb.TotalInsurance = floorf(cell.slider.value) * UnitRMB;
                        break;
                    case 3:
                         app.recodDb.earnOfAfterTax = floorf(cell.slider.value) * UnitRMB;
                        break;
                    case 4:
                        app.recodDb.yearlyExpense = [cell.LabCurrentVlaue.text floatValue] * UnitRMB * 12;
                        break;
                    default:
                        break;
                }
            }
        
        if(app.answeredLife_accident_Question == NO)
        {
            if(app.recodDb.isSmoke.selectIndex > 1)
            { app.answeredLife_accident_Question = YES; }
            else if (app.recodDb.travel.selectIndex > 1)
            { app.answeredLife_accident_Question = YES; }
            else if(app.recodDb.TotalInsurance > 0 * UnitRMB)
            { app.answeredLife_accident_Question = YES; }
            else if(app.recodDb.earnOfAfterTax > 20 * UnitRMB)
            { app.answeredLife_accident_Question = YES; }
            else if(app.recodDb.yearlyExpense > 0 * UnitRMB)
            { app.answeredLife_accident_Question = YES; }
        }
    }
    else if ([_InsuranceType isEqualToString: kHealth_insurance])
    {
        for(int i=0; i < _arrayAllThoseIndexPathes.count; i++)
        {
           SingleSelCell *cell = (SingleSelCell *)[_table_View cellForRowAtIndexPath:[_arrayAllThoseIndexPathes objectAtIndex:i]];
            switch (i) {
                case 0:
                    app.recodDb.isSmoke = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                case 1:
                    app.recodDb.isHaveInsurance = floorf(cell.slider.value) * UnitRMB;
                    break;
                case 2:
                    app.recodDb.accidentInsurance = floorf(cell.slider.value) * UnitRMB;
                    break;
                case 3:
                    app.recodDb.isIllness = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                default:
                    break;
            }
        }
        if(app.answeredHealthQuestion == NO)
        {
            if(app.recodDb.isSmoke.selectIndex > 1)
            { app.answeredHealthQuestion = YES; }
            else if(app.recodDb.isHaveInsurance > 0 * UnitRMB)
            { app.answeredHealthQuestion = YES; }
            else if(app.recodDb.accidentInsurance > 0 * UnitRMB)
            { app.answeredHealthQuestion = YES; }
            else if (app.recodDb.isIllness.selectIndex > 1)
            { app.answeredHealthQuestion = YES; }
        }
    }
    else if ([_InsuranceType isEqualToString: kRetirement])
    {
        for(int i=0; i< _arrayAllThoseIndexPathes.count; i++)
        {
            SingleSelCell *cell = (SingleSelCell *)[_table_View cellForRowAtIndexPath:[_arrayAllThoseIndexPathes objectAtIndex:i]];
            switch (i) {
                case 0:
                    app.recodDb.occupation = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                case 1:
                    app.recodDb.investment = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                case 2:
                    app.recodDb.yearlyExpense = [cell.LabCurrentVlaue.text floatValue] * UnitRMB * 12;
                    break;
                case 3:
                    //储蓄型保险
                    app.recodDb.depositInsurance = floorf(cell.slider.value) * UnitRMB;
                    break;
                default:
                    break;
            }
        }
        if(app.answeredRetirementQuestion == NO)
        {
            if(app.recodDb.occupation.selectIndex > 1)
            { app.answeredRetirementQuestion = YES; }
            else if(app.recodDb.investment.selectIndex > 1)
            { app.answeredRetirementQuestion = YES; }
            else if(app.recodDb.yearlyExpense > 0 * UnitRMB)
            { app.answeredRetirementQuestion = YES; }
            else if(app.recodDb.depositInsurance > 10 * UnitRMB)
            { app.answeredRetirementQuestion = YES; }
        }
    }
    else if ([_InsuranceType isEqualToString: kChildren_education]){
    
        userFamily uf;
        for(int i=0; i< _arrayAllThoseIndexPathes.count; i++)
        {
            SingleSelCell *cell = (SingleSelCell *)[_table_View cellForRowAtIndexPath:[_arrayAllThoseIndexPathes objectAtIndex:i]];
            switch (i){
                case 0:
                    app.recodDb.occupation = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                case 1:
                    app.recodDb.investment = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                case 2:
                    app.recodDb.SchoolType = [[Question alloc]init:cell.sing.selectedValue selectIndex:cell.sing.selectedIndex];
                    break;
                case 3:
                    //小孩还有多少年上大学
                    uf = app.recodDb.family;
                    uf.attendSchoolInYears = floorf(cell.slider.value);
                    app.recodDb.family = uf;
                    break;
                default:
                    break;
            }
        }
        if(app.answeredEducationQuestion == NO)
        {
            if( app.recodDb.occupation.selectIndex > 1)
            { app.answeredEducationQuestion = YES; }
            else if(app.recodDb.investment.selectIndex > 1)
            { app.answeredEducationQuestion = YES; }
            else if(app.recodDb.SchoolType.selectIndex > 1)
            { app.answeredEducationQuestion = YES; }
            else if( uf.attendSchoolInYears > 0)
            { app.answeredEducationQuestion = YES; }
        }
    }
    NSInteger life_Insurance = app.recodDb.TotalInsurance - app.recodDb.depositInsurance - app.recodDb.accidentInsurance;
    if(life_Insurance < 0)
    {
        life_Insurance = 0;
    }
    app.recodDb.lifeInsurance = life_Insurance;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
