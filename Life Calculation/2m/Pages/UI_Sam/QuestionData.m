//
//  QuestionData.m
//  testRingBtn
//
//  Created by bizopstech on 13-12-12.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "QuestionData.h"
#import "AppDelegate.h"
@implementation QuestionData

+(NSArray *) getLife_AccidentData{
    /*
     @[@"singleSel", @"Do you smoke or not?",@[@"Yes",@"No"],@1,@2 ]
     arr[0] 数据类型（单选 、滑块）
     arr[1] 问题标题
     arr[2] 单选项标题
     arr[3] 默认选择第几项
     arr[4] 每行显示几个
     arr[5] 单位符号
     */
    
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    NSNumber *isSmokeSel,*travelSel,*TotalInsurance,*earnOfAfterTax,*yearlyExpense;
    NSArray * earnOfAfterTaxSliderData;
    switch (app.recodDb.earnOfAfterTaxSelectIndex)
    {
        case 1:
            earnOfAfterTaxSliderData = @[@"0",@"2万",@"0",@"2"];
            earnOfAfterTax = @0;
            break;
        case 2:
            earnOfAfterTaxSliderData = @[@"2万",@"10万",@"2",@"10"];
            earnOfAfterTax = @2;
            break;
        case 3:
            earnOfAfterTaxSliderData = @[@"10万",@">50万",@"10",@"50"];
            earnOfAfterTax = @10;
            break;
        case 4:
            earnOfAfterTaxSliderData = @[@"50万",@"500万",@"50",@"500"];
            earnOfAfterTax = @50;
            break;
        case 5:
            earnOfAfterTaxSliderData = @[@"500万",@">1000万",@"500",@"1000"];
            earnOfAfterTax = @500;
            break;
        default:
            NSLog(@"load earnOfAfterTax value error!");
            break;
    }
    if(app.answeredLife_accident_Question == YES)
    {
        travelSel = [NSNumber numberWithInt: app.recodDb.travel.selectIndex];
        TotalInsurance = [NSNumber numberWithInteger:  app.recodDb.TotalInsurance / UnitRMB];
        earnOfAfterTax = [NSNumber numberWithInt: app.recodDb.earnOfAfterTax / UnitRMB];
    }else{
        travelSel = @1;
        TotalInsurance = @0;
    }
    isSmokeSel = app.recodDb.isSmoke != nil ? [NSNumber numberWithInt: app.recodDb.isSmoke.selectIndex] : @1;
    yearlyExpense = app.recodDb.yearlyExpense > 0 ? [NSNumber numberWithFloat:(float)app.recodDb.yearlyExpense / UnitRMB /12] : @0;

    NSArray * source = @[
                            @[@"singleSel", @"您是否吸烟？",@[@"是",@"否"],isSmokeSel,@2 ],
                            @[@"singleSel",@"您出差或旅行的频率如何？",@[@"经常",@"一般",@"几乎没有"],travelSel,@3 ],
                            @[@"slider",@"您已有保单的保额共是多少（包括寿险及意外险）",@[@"0",@">400万",@"0",@"400"],TotalInsurance ,@"万"],
                            @[@"slider",@"您的年收入是多少？",earnOfAfterTaxSliderData,earnOfAfterTax,@"万"],
                            
                            @[@"slider",@"您的每月支出是多少？",@[@"0",@">10万",@"0",@"10" ],yearlyExpense,@"万"]
                        ];
    return source;
}

+(NSArray *) getHealthData{

    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    NSNumber *isSmokeSel,*isHaveInsurance,*accidentInsurance,*isIllnessSel;
    if(app.answeredHealthQuestion == YES)
    {
        isHaveInsurance = [NSNumber numberWithInteger: app.recodDb.isHaveInsurance / UnitRMB];
        accidentInsurance = [NSNumber numberWithInteger: app.recodDb.accidentInsurance / UnitRMB];
        isIllnessSel = [NSNumber numberWithInt: app.recodDb.isIllness.selectIndex];
    }else{
        isHaveInsurance = @0;
        accidentInsurance = @0;
        isIllnessSel = @1;
    }
    isSmokeSel = app.recodDb.isSmoke != nil ? [NSNumber numberWithInt: app.recodDb.isSmoke.selectIndex] : @1;
    
    NSArray * HealthSource = @[
                         @[@"singleSel", @"您是否吸烟？",@[@"是",@"否"],isSmokeSel,@2],
                         @[@"slider",@"您的社保余额是多少？",@[@"0",@">100万",@"0",@"100"],isHaveInsurance,@"万"],
                         @[@"slider",@"您已购买的重疾险的保额是多少？",@[@"0",@">100万",@"0",@"100"],accidentInsurance,@"万"],
                         @[@"singleSel",@"您是否有家庭重疾病史？",@[@"是",@"否"],isIllnessSel,@2]
                         ];
    return HealthSource;
}

+(NSArray *)getRetirementData{

    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    NSNumber *occupationSel,*investmentSel,*yearlyExpense,*depositInsurance;
    if(app.answeredRetirementQuestion == YES)
    {
        depositInsurance = [NSNumber numberWithInteger:app.recodDb.depositInsurance / UnitRMB];
    }else{
        depositInsurance = @10;
    }
    yearlyExpense = app.recodDb.yearlyExpense > 0 ? [NSNumber numberWithFloat: (float)app.recodDb.yearlyExpense / UnitRMB / 12] : @0;
    occupationSel = app.recodDb.occupation != nil ? [NSNumber numberWithInt: app.recodDb.occupation.selectIndex]:@1;
    investmentSel = app.recodDb.investment != nil ? [NSNumber numberWithInt: app.recodDb.investment.selectIndex]:@1;
    
    NSArray * RetirementSource = @[
                                   @[@"singleSel", @"您的职业是什么？",@[@"政府或国企员工",@"一般企业员工",@"企业高管",@"自由职业者",@"中小企业主",@"销售人员" ],occupationSel ,@2],
                               
                                   @[@"singleSel",@"您的投资偏好是什么？",@[@"保守型",@"谨慎型",@"积极型",@"激进型"],investmentSel ,@2],
                               @[@"slider",@"您的每月支出是多少？",@[@"0",@">10万",@"0",@"10"],yearlyExpense,@"万"],
                                   @[@"slider",@"您已购买了多少保额的两全险？",@[@"<1万",@">100万",@"1",@"100"],depositInsurance,@"万"]
                               ];
    return  RetirementSource;
}

+(NSArray *)getCollegeEducation{
    
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    NSNumber *occupationSel, *investmentSel, *SchoolTypeSel, *attendSchoolInYears;
    if(app.answeredEducationQuestion == YES)
    {
        SchoolTypeSel = [NSNumber numberWithInt: app.recodDb.SchoolType.selectIndex];
        userFamily uf = app.recodDb.family;
        attendSchoolInYears = [NSNumber numberWithInt: uf.attendSchoolInYears];
    }else{
        SchoolTypeSel = @1;
        attendSchoolInYears = @0;
    }
    occupationSel = app.recodDb.occupation != nil ? [NSNumber numberWithInt: app.recodDb.occupation.selectIndex]:@1;
    investmentSel = app.recodDb.investment != nil ? [NSNumber numberWithInt: app.recodDb.investment.selectIndex]:@1;
    
    NSArray * CollegeEducationSource = @[
                                   @[@"singleSel", @"您的职业是什么？",@[@"政府或国企员工",@"一般企业员工",@"企业高管",@"自由职业者",@"中小企业主",@"销售人员" ],occupationSel,@2 ],
                                   
                                   @[@"singleSel",@"您的投资偏好是什么？",@[@"保守型",@"谨慎型",@"积极型",@"激进型"],investmentSel,@2 ],
                                   
                                   @[@"singleSel",@"您想让小孩就读什么学校？",@[@"国内大学",@"国外大学",@"未确定"],SchoolTypeSel,@3 ],
                                   
                                   @[@"slider",@"您的小孩多少年后准备上大学？",@[@"0",@"18或以上",@"0",@"18"],attendSchoolInYears,@""]
                                   ];
   
    return CollegeEducationSource;
}

@end
