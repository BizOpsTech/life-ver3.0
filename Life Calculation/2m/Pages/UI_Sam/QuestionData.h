//
//  QuestionData.h
//  testRingBtn
//
//  Created by bizopstech on 13-12-12.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface QuestionData : NSObject

+(NSArray *) getLife_AccidentData;
+(NSArray *) getHealthData;

+(NSArray *)getRetirementData;
+(NSArray *)getCollegeEducation;
@end
