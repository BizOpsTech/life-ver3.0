//
//  SingleSelCell.m
//  testRingBtn
//
//  Created by bizopstech on 13-12-12.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "SingleSelCell.h"


@interface SingleSelCell()
{
    float maxFlow;
    float minFlow;
    BOOL useLogMethodBySlider;
    int SliderDecimal;
}
@end



@implementation SingleSelCell

@synthesize sing = _sing;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        useLogMethodBySlider = NO;
        SliderDecimal = 0;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (void)defineAlgorithmOfLogMethodBySlider:(UISlider*)swSlider{
    
    NSLog(@"max value %f",swSlider.maximumValue);
    double dA = 100 * log(maxFlow);
    double dB = 100 * log(minFlow == 0 ? 1 : minFlow);
    
    swSlider.maximumValue = [[NSNumber numberWithDouble:dA] floatValue];
    swSlider.minimumValue = [[NSNumber numberWithDouble:dB] floatValue];
    
    float sliderValue = swSlider.value;
    double showResult = exp(sliderValue/100);
    int showResultOfInt = 0;
    if ( showResult > (showResult/10-1)*10) {
        if (showResult >80) showResultOfInt = showResult + 1;
        else showResultOfInt = showResult;;
    }else{
        showResultOfInt = showResult;
    }
    
    if(showResult < 5){
        showResultOfInt = showResult -1;
    }
    _LabCurrentVlaue.text = [NSString stringWithFormat:@"%d%@",showResultOfInt,_unitSymbol];
    NSLog(@"selected value %f",swSlider.value);
    
}

- (IBAction)changeValue:(UISlider *)sender {
    
    if (maxFlow <= 0) {
        minFlow = sender.minimumValue;
        maxFlow = sender.maximumValue;
    }
    if (useLogMethodBySlider == YES) {
        [self defineAlgorithmOfLogMethodBySlider:sender];
        return;
    }
    if(SliderDecimal == 0){
        _LabCurrentVlaue.text = [NSString stringWithFormat:@"%0.f%@", floorf(sender.value),_unitSymbol];
    }
    else if (SliderDecimal == 1){
        _LabCurrentVlaue.text = [NSString stringWithFormat:@"%1.1f%@",sender.value,_unitSymbol];
    }
}


-(void)LoadUIAndData:(NSArray *)dataArr RowHeight:(CGFloat)height{
    
    // @[@"singleSel", @"Do you smoke or not?",@[@"Yes",@"No"],@1 ],
    NSString * type = dataArr[0];
    _questionTitle.text = dataArr[1];

    _LabCurrentVlaue.layer.borderWidth = 0.6f;
    _LabCurrentVlaue.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _LabCurrentVlaue.layer.cornerRadius = 4.0;
    
    
    if([type isEqualToString:@"slider"] == YES){
        _unitSymbol = dataArr[4];
        
        _SliderView.hidden = NO;
        _LabSliderMinValue.text = dataArr[2][0];
        _LabSliderMaxValue.text = dataArr[2][1];
        _LabCurrentVlaue.text = [NSString stringWithFormat:@"%@%@",dataArr[3],_unitSymbol];
        
        NSString * minimumValue = [[dataArr objectAtIndex:2] objectAtIndex:2];
        _slider.minimumValue = [minimumValue floatValue] ;
        NSString * maximumValue = dataArr[2][3];
        _slider.maximumValue = [maximumValue floatValue] ;
        _slider.value = [dataArr[3] floatValue];
        
        if(_slider.maximumValue - _slider.minimumValue >= 1000) //滑块间距> 1000
        {
            useLogMethodBySlider = YES;
        }
        if([_questionTitle.text isEqualToString:@"您的每月支出是多少？"])
        {
            SliderDecimal = 1;
        }
    }
    else{
        _SliderView.hidden = YES;
        CGRect rect = CGRectMake(0, 0, 305, height);
         _sing = [[singleSelect alloc] initWithFrame:rect];
        
        _sing.singleSelectNumInLine = [dataArr[4] integerValue];
        _sing.MuSelData = dataArr[2];
        _sing.selectedIndex = [dataArr[3] intValue];
        _sing.selectedValue = dataArr[2][0];
        [_sing addSubViewControl];
        [_SingleView addSubview:_sing];
        //[_SingleView updateConstraints];
    }
}
@end
