//
//  SingleSelCell.h
//  testRingBtn
//
//  Created by bizopstech on 13-12-12.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "singleSelect.h"

@interface SingleSelCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *questionTitle;

@property (strong, nonatomic) IBOutlet UIView *SliderView;

@property (strong, nonatomic) IBOutlet UIView *SingleView;

@property (strong, nonatomic) IBOutlet UILabel *LabSliderMinValue;

@property (strong, nonatomic) IBOutlet UILabel *LabSliderMaxValue;

@property (strong, nonatomic) IBOutlet UILabel *LabCurrentVlaue;

@property (strong, nonatomic) IBOutlet UISlider *slider;

@property (strong,nonatomic) singleSelect * sing;

@property(nonatomic,copy)NSString *unitSymbol;

-(void)LoadUIAndData:(NSArray *)dataArr RowHeight:(CGFloat)height;
@end
