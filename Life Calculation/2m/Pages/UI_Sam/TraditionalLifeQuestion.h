//
//  TraditionalLifeQuestion.h
//  testRingBtn
//
//  Created by bizopstech on 13-12-12.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import <UIKit/UIKit.h>

@interface TraditionalLifeQuestion : UIView

@property (strong, nonatomic) IBOutlet UITableView *table_View;

@property (strong, nonatomic) IBOutlet UILabel *LabTitle;

- (instancetype)init:(NSArray *)arr InsuranceType:(NSString *)type;

-(void)SaveValues:(AppDelegate *)app;

@end
