//
//  handleCalculate.h
//  testRingBtn
//
//  Created by bizopstech on 13-12-10.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UserDb;
@interface handleCalculate : NSObject

-(NSArray *)GetSortedArrObj:(UserDb *)source;

-(NSArray *)getInsuranceCoverage:(UserDb *)source;

@end
