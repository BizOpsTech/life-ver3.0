//
//  handleCalculate.m
//  testRingBtn
//
//  Created by bizopstech on 13-12-10.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.

#import "handleCalculate.h"
#import "Calculate.h"
#import "UserDb.h"
#import "ResultBase.h"
#import "calculatePriorityfor2Minute.h"
#import "AppDelegate.h"

@implementation handleCalculate


-(NSArray *)getInsuranceCoverage:(UserDb *)source{
    float yearlyExpense,retireAge,currentAge,cityType,expecteAge,isRetiredOfMan;
    float EndowmentInsurance,LifeInsurance,AccidentDisease,SocialInsurance;
    float yearlyIncom;     //收入
    BOOL IsForeignCollage; //是否就读国外学校
    
    
    switch (source.earnOfAfterTaxSelectIndex) {
        case 1:
            yearlyIncom = 15 * 1000.0;
            yearlyExpense = 8 * 1000;
            IsForeignCollage = NO;
            break;
        case 2:
            yearlyIncom = 60 * 1000.0;
            yearlyExpense = 20 * 1000;
            IsForeignCollage = NO;
            break;
        case 3:
            yearlyIncom = 200 * 1000.0;
            yearlyExpense = 50 * 1000;
            IsForeignCollage = NO;
            break;
        case 4:
            yearlyIncom = 1000 * 1000.0;
            yearlyExpense = 150 * 1000;
            IsForeignCollage = YES;
            break;
        case 5:
            yearlyIncom = 3000 * 1000.0;
            yearlyExpense = 500 * 1000;
            IsForeignCollage = YES;
            break;
        default:
            yearlyIncom = 15 * 1000.0;
            yearlyExpense = 8 * 1000;
            IsForeignCollage = NO;
            break;
    }
    
    if(source.earnOfAfterTax > 0)
    {
        yearlyIncom = source.earnOfAfterTax;
        yearlyExpense = source.yearlyExpense;
    }
    else if(source.yearlyExpense > 0)
    {
        yearlyExpense = source.yearlyExpense;
    }
    
    if(source.SchoolType != nil)
    {
        if(source.SchoolType.selectIndex == 2)// 国外
        {
            IsForeignCollage = YES;
        }else{
            IsForeignCollage = NO;
        }
    }
    
    switch (source.gender.selectIndex){
        case 1:
            //woman
            retireAge = 55;
            isRetiredOfMan = NO;
            break;
        case 2:
            //man
            retireAge = 60;
            isRetiredOfMan = YES;
            break;
        default:
            retireAge = 55;
            isRetiredOfMan = NO;
            break;
    }
    currentAge = source.age;
    cityType = 1.0;
    expecteAge = retireAge - currentAge;
    
    EndowmentInsurance = (float)source.depositInsurance;
    LifeInsurance = (float)source.lifeInsurance;
    AccidentDisease = (float)source.accidentInsurance;
    SocialInsurance = (float)source.isHaveInsurance;
    NSUInteger myKids = source.family.Kids;
    NSUInteger myCollageAge = 45 - currentAge;
    if(myCollageAge > 18){
        myCollageAge = 18;
    }
    ////////////////////////////////////////// 推荐保额 /////////////////////////////////////////////////////////
    float traditionalCoverage,healthyCoverage,retirementCoverage;
    float educationCoverage = 0;
    Calculate *calculate  = [Calculate new];

    
    traditionalCoverage = [calculate calculateAccidentCoverageByYearlyIncome:yearlyIncom
                                 withSelfYearlyExpense:yearlyExpense
                                    withIsRetiredOfMan:isRetiredOfMan
                                        withCurrentAge:currentAge
                                withEndowmentInsurance:EndowmentInsurance
                                              withLife:LifeInsurance
                                                withAD:AccidentDisease];
    traditionalCoverage = roundf(traditionalCoverage);

    healthyCoverage = [calculate calculateHealthCoverageByCityType:cityType
                                                    withCurrentAge:currentAge
                                               withSocialInsurance:SocialInsurance
                                                            withAD:AccidentDisease];
    healthyCoverage = roundf(healthyCoverage);
    
    retirementCoverage = [calculate calculateRetirementCoverageByExpectedAge:expecteAge
                                                          withIsRetiredOfMan:isRetiredOfMan
                                                              withCurrentAge:currentAge
                                                        withYearlyExpenseNow:yearlyExpense
                                                      withEndowmentInsurance:EndowmentInsurance];
    retirementCoverage = roundf(retirementCoverage);
    
    
    if(myKids > 0 && myCollageAge > 0){
        educationCoverage = [calculate calculateChildrenEducationCoverageByChilds:myKids
                                                               withCollageAge:myCollageAge
                                                         withIsForeignCollage:IsForeignCollage];
        educationCoverage = roundf(educationCoverage);
    }
    
     NSArray * coverageArr = [NSArray arrayWithObjects:[NSNumber numberWithFloat:traditionalCoverage],
                             [NSNumber numberWithFloat:retirementCoverage],
                             [NSNumber numberWithFloat:healthyCoverage],
                             [NSNumber numberWithFloat:educationCoverage],
                             nil];
    
    return coverageArr;
}

-(NSArray *)GetSortedArrObj:(UserDb *)source{
    
    calculatePriorityfor2Minute * calculatePriority = [[calculatePriorityfor2Minute alloc]init];
    calculatePriority.userDB = source;
    [calculatePriority transform];
    AppDelegate * app = [[UIApplication sharedApplication]delegate];
    NSArray * Coverage_arr = [self getInsuranceCoverage:source];
    ResultBase * life_Accident = [[ResultBase alloc]init];
    life_Accident.Priority = [calculatePriority getTraditionalLifePriority];
    
    
    if(app.answeredLife_accident_Question == NO)
    {
        life_Accident.unadjusted_Coverage = [self significantFigure:[[Coverage_arr objectAtIndex:0] floatValue]];
    }else{
        life_Accident.unadjusted_Coverage = [[Coverage_arr objectAtIndex:0] floatValue];
        life_Accident.IshavePreciseUnadjusted_Coverage = YES;
    }
    life_Accident.name = @"人寿和意外险";
    life_Accident.InsuranceType = insuranceModel_Traditional;
    
    ResultBase * Health = [[ResultBase alloc]init];
    Health.Priority = [calculatePriority getHealthInsurancePriority];
    
    if(app.answeredHealthQuestion == NO)
    {
        Health.unadjusted_Coverage = [self significantFigure:[[Coverage_arr objectAtIndex:2] floatValue]];
    }else{

        Health.unadjusted_Coverage = [[Coverage_arr objectAtIndex:2] floatValue];
        Health.IshavePreciseUnadjusted_Coverage = YES;
    }
    Health.name = @"健康险";
    Health.InsuranceType = insuranceModel_Healthy;
    
    ResultBase * Retirement = [[ResultBase alloc]init];
    Retirement.Priority = [calculatePriority getRetirementPriority];
    if(app.answeredRetirementQuestion == NO)
    {
        Retirement.unadjusted_Coverage = [self significantFigure:[[Coverage_arr objectAtIndex:1] floatValue]];
    }else{
        Retirement.unadjusted_Coverage = [[Coverage_arr objectAtIndex:1] floatValue];
        Retirement.IshavePreciseUnadjusted_Coverage = YES;
    }
    Retirement.name = @"养老险";
    Retirement.InsuranceType = insuranceModel_Pension;
    
    ResultBase * Education = [[ResultBase alloc]init];
    Education.Priority = [calculatePriority getChildrenEducationPriority];
    
    if(app.answeredEducationQuestion == NO)
    {
        Education.unadjusted_Coverage = [self significantFigure:[[Coverage_arr objectAtIndex:3] floatValue]];
    }else{
        Education.unadjusted_Coverage = [[Coverage_arr objectAtIndex:3] floatValue];
        Education.IshavePreciseUnadjusted_Coverage = YES;
    }
    Education.name = @"教育险";
    Education.InsuranceType = insuranceModel_Education;
    
    NSArray * objArr = [NSArray arrayWithObjects:life_Accident,Health,Retirement, Education,nil];
    
    //@“Priority”是对象属性
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc] initWithKey:@"Priority" ascending:NO];
    
    return [objArr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];//返回排序好的数组
}


-(float)significantFigure:(float)value{
    
    
    if(value >= 10.0f && value < 100.0f){
        value = roundf(value / 10) * powf(10,1);
    }
    else if(value >= 100.0f && value < 1000.0f){
        value = roundf(value / 100) * powf(10,2);
    }
    else if(value >= 1000.0f && value < 10000.0f)
    {
        value = roundf(value / 1000) * powf(10,3);
    }
    else if (value >= 10000.0f && value < 100000.0f)
    {
        value = roundf(value / 10000) * powf(10,4);
    }
    else if (value >= 100000.0f && value < 1000000.0f)
    {
        value = roundf(value / 100000) * powf(10,5);
    }
    else if (value >= 1000000.0f && value < 10000000.0f)
    {
        value = roundf(value / 1000000) * powf(10,6);
    }
    else if (value >= 10000000.0f && value < 100000000.0f)
    {
        value = roundf(value / 10000000) * powf(10,7);
    }
    else if (value >= 100000000.0f)
    {
        value = roundf(value / 100000000) * powf(10,8);
    }
    return value;
}


@end
