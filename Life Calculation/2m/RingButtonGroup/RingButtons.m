//
//  RingButtons.m
//  Life-insurance-v3
//
//  Created by BizOpsTech on 12/10/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "OBShapedButton.h"
#import "RingButtons.h"
@interface RingButtons()
@property(nonatomic,strong)NSArray *NormalArr;
@property(nonatomic,strong)NSArray *SelectArr;
@property(nonatomic,strong)NSArray *CompleteArr;
@property(nonatomic,strong)NSArray *ButtonArr;

@property (strong, nonatomic) IBOutlet OBShapedButton *btnAge;
@property (strong, nonatomic) IBOutlet OBShapedButton *btnGender;
@property (strong, nonatomic) IBOutlet OBShapedButton *btnMarriage;
@property (strong, nonatomic) IBOutlet OBShapedButton *btnKids;
@property (strong, nonatomic) IBOutlet OBShapedButton *btnFamliy;
@property (strong, nonatomic) IBOutlet OBShapedButton *btnIncome;

@end

@implementation RingButtons
@synthesize NormalArr = _NormalArr;
@synthesize SelectArr = _SelectArr;
@synthesize CompleteArr = _CompleteArr;
@synthesize ButtonArr = _ButtonArr;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"RingButtons" owner:self options:nil] lastObject];
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)didSelectButton:(OBShapedButton *)sender {
    for(NSUInteger i=0; i< _ButtonArr.count;  i++){
        OBShapedButton * currentButton = (OBShapedButton *)[_ButtonArr objectAtIndex:i];
        if([sender isEqual:currentButton]){
            if(_PrevButtonIndex != i){
                //上一个点击按钮设置为完成状态
                [self setButtonStateFromIndex:_PrevButtonIndex State:2];
                
                //设置当前状态为选中
                [self setButtonStateFromIndex:i State:1];
                
                //选中新按钮后，执行委托方法
                [self.delegate didSelectButton:i pervSelectButton:_PrevButtonIndex];
                
                //记录当前按钮索引
                _PrevButtonIndex = i;
            }
            break;
        }
    }
}

-(void)setButtonStateFromIndex:(NSUInteger)ButtonIndex State:(NSUInteger)stateIndex{
    UIImage *img;
    if(ButtonIndex <= 5){
    OBShapedButton *currentButton = (OBShapedButton *)[_ButtonArr objectAtIndex:ButtonIndex];
    switch(stateIndex){
        case 0:
            img = [_NormalArr objectAtIndex:ButtonIndex];
            break;
        case 1:
            img = [_SelectArr objectAtIndex:ButtonIndex];
            break;
        case 2:
            img = [_CompleteArr objectAtIndex:ButtonIndex];
            break;
    }
        [currentButton setImage:img forState:UIControlStateNormal];
    }
}

-(void)buttonStateGroupInit{
    _ButtonArr = [NSArray arrayWithObjects:_btnAge,_btnGender,_btnMarriage,_btnKids,_btnFamliy,_btnIncome, nil];

    _NormalArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"RingAgeNormal"],
                  [UIImage imageNamed:@"RingGenderNormal"],
                  [UIImage imageNamed:@"RingMarriageNormal"],
                  [UIImage imageNamed:@"RingKidsNormal"],
                  [UIImage imageNamed:@"RingFamilyNormal"],
                  [UIImage imageNamed:@"RingIncomeNormal"],
                  nil];
  
    _CompleteArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"RingAgeBlue"],
                    [UIImage imageNamed:@"RingGenderBlue"],
                    [UIImage imageNamed:@"RingMarriageBlue"],
                    [UIImage imageNamed:@"RingKidsBlue"],
                    [UIImage imageNamed:@"RingFamilyBlue"],
                    [UIImage imageNamed:@"RingIncomeBlue"],
                    nil];
    
    _SelectArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"RingAgeStroke"],
                  [UIImage imageNamed:@"RingGenderStroke"],
                  [UIImage imageNamed:@"RingMarriageStroke"],
                  [UIImage imageNamed:@"RingKidsStroke"],
                  [UIImage imageNamed:@"RingFamilyStroke"],
                  [UIImage imageNamed:@"RingIncomeStroke"],
                  nil];
    
}
@end
