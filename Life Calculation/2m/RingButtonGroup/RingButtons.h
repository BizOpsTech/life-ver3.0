//
//  RingButtons.h
//  Life-insurance-v3
//
//  Created by BizOpsTech on 12/10/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RingButtonsDelegate;
@interface RingButtons : UIView
@property(nonatomic)NSUInteger PrevButtonIndex;
@property(nonatomic)BOOL isComplete;
@property (nonatomic, assign)id<RingButtonsDelegate> delegate;
-(void)buttonStateGroupInit;
-(void)setButtonStateFromIndex:(NSUInteger)ButtonIndex State:(NSUInteger)stateIndex;
@end

@protocol RingButtonsDelegate <NSObject>
-(void)didSelectButton:(RingButtonType)currentButton pervSelectButton:(RingButtonType)pervButton;

@end
