//
//  SliderWithImage.h
//  Life-insurance-v3
//
//  Created by BizOpsTech on 12/10/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "UIDevice+Resolutions.h"
#import <UIKit/UIKit.h>
@protocol SliderChangeValueDelegate;
@interface SliderWithImage : UIView
@property(nonatomic,copy)NSString *symbol;
@property(nonatomic,strong)NSArray *itemTitele;
@property(nonatomic,strong)NSArray *itemImage;
@property(nonatomic)BOOL visibleOptionValue;
@property(nonatomic,assign)float currentValue;

@property(nonatomic,copy)NSString *sliderImageBg;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UITextField *txt;
@property(assign,nonatomic)NSArray *iconSize;


@property (nonatomic, assign)id<SliderChangeValueDelegate> delegate;
@property(nonatomic)RingButtonType UIType;

-(void)imageChangeFromIndex:(NSUInteger)index;
@end
@protocol SliderChangeValueDelegate <NSObject>
-(void)sliderValueChanged:(float)value UIType:(RingButtonType)State;
//-(void)sliderValueChanging:(float)value UIType:(RingButtonType)State;
@end
