//
//  SliderWithImage.m
//  Life-insurance-v3
//
//  Created by BizOpsTech on 12/10/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "SliderWithImage.h"
@interface SliderWithImage()

@property (strong, nonatomic) IBOutlet UIView *titleGroupView;

@end

@implementation SliderWithImage
@synthesize UIType = _UIType;
@synthesize visibleOptionValue = _visibleOptionValue;
@synthesize symbol = _symbol;
@synthesize itemTitele = _itemTitele;
@synthesize itemImage = _itemImage;
@synthesize currentValue = _currentValue;
@synthesize iconSize = _iconSize;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (instancetype)init{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"SliderWithImage" owner:self options:nil] lastObject];
    
//    if (self) {
//        self.rsView.delegate = self;
//    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)setIconSize:(NSArray *)iconSize{
    CGFloat x,y,w,h;
    if(![iconSize isEqual:nil] && iconSize.count >= 2){
        self.icon.translatesAutoresizingMaskIntoConstraints = YES;
        w = [[iconSize objectAtIndex:0]floatValue];
        h = [[iconSize objectAtIndex:1]floatValue];
        x = (self.icon.superview.frame.size.width - w)/2;
        y = (self.icon.superview.frame.size.height - h)/2;
        CGRect iconCGRect = CGRectMake(x, y, w, h);
        self.icon.frame = iconCGRect;
    }
}

-(void)setSliderImageBg:(NSString *)sliderImageBg{
    CGFloat capwidth = 295;
    if([[[UIDevice currentDevice]systemVersion]floatValue]>=7){
        capwidth = 300;
    }
    UIImage *baseImg = [[UIImage imageNamed:sliderImageBg]stretchableImageWithLeftCapWidth:capwidth topCapHeight:11];
    [_slider setMaximumTrackImage:baseImg forState:UIControlStateNormal];
    [_slider setMinimumTrackImage:baseImg forState:UIControlStateNormal];
}

-(void)setVisibleOptionValue:(BOOL)visibleOptionValue{
    _txt.hidden = !visibleOptionValue;
    
}

-(void)setItemTitele:(NSArray *)itemTitele{
    CGFloat offsetY = 80;

    if(itemTitele.count >0){
        for(NSUInteger i=0; i< itemTitele.count;i++){
            CGFloat unitSize = 300 / itemTitele.count;
            CGFloat offsetX = 10 + i*unitSize;
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(offsetX, offsetY, unitSize-10, 32)];
            label.text = [itemTitele objectAtIndex:i];
            if(i == 0){
                label.textAlignment = NSTextAlignmentLeft;
            }else if(i == itemTitele.count-1 && itemTitele.count >1){
                label.textAlignment = NSTextAlignmentRight;
            }else{
                label.textAlignment = NSTextAlignmentCenter;
            }
            label.numberOfLines = 2;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            label.font = [UIFont fontWithName:@"Arial" size:12];
            label.backgroundColor = [UIColor clearColor];
            [_titleGroupView addSubview:label];
        }
    }
}


-(void)imageChangeFromIndex:(NSUInteger)index{
    [_icon setImage:[_itemImage objectAtIndex:index]];
}

/////// 拖拽过程
- (IBAction)SliderValueChanging:(UISlider *)sender {
    NSString *unitSymbol = @"";
    if(![_symbol isEqual:nil] && _symbol.length >0){
        unitSymbol = _symbol;
    }
    switch (_UIType) {
        case RingButtonType_income:
            if(sender.value < 1.5){
                _txt.text = @"<2万";
            }else if (sender.value >=1.5 && sender.value < 2.5){
                _txt.text = @"2万~10万";
            }else if (sender.value >=2.5 && sender.value < 3.5){
                _txt.text =  @"10万~50万";
            }else if (sender.value >=3.5 && sender.value < 4.5){
                _txt.text = @"50万~500万";
            }else{
                _txt.text = @"500万以上";
            }
            break;
        case RingButtonType_famliy:
            if(sender.value <= 0.5){
                _txt.text = @"0";
            }else if(sender.value > 0.5 && sender.value <=1.5){
                _txt.text = @"1";
            }else if(sender.value > 1.5 && sender.value <=2.5){
                _txt.text = @"2";
            }else if(sender.value > 2.5 && sender.value <=3.5){
                _txt.text = @"3";
            }else if(sender.value > 3.5){
                _txt.text = @"4";
            }
        case RingButtonType_kids:
            if(sender.value <= 0.5){
                _txt.text = @"0";
            }else if(sender.value > 0.5 && sender.value <=1.5){
                _txt.text = @"1";
            }else if(sender.value > 1.5 && sender.value <=2.5){
                _txt.text = @"2";
            }else if(sender.value > 2.5 && sender.value <=3.5){
                _txt.text = @"3";
            }else if(sender.value > 3.5 && sender.value <=4.5){
                _txt.text = @"4";
            }else if(sender.value > 4.5){
                _txt.text = @"5";
            }
        break;
        break;
            
        default:
             _txt.text = [NSString stringWithFormat:@"%d%@",(int)sender.value,unitSymbol];
            break;
    }
    
    self.currentValue = sender.value;
   // [self.delegate sliderValueChanging:sender.value UIType:_UIType];
}

//////// 拖拽完成
- (IBAction)SliderValueChanged:(UISlider *)sender {
    [self.delegate sliderValueChanged:sender.value UIType:_UIType];
}


-(void)setCurrentValue:(float)currentValue{
    switch (_UIType) {
        case RingButtonType_age:
            if(currentValue >40 && currentValue <50){
                [self imageChangeFromIndex:1];
            }else if(currentValue >50){
                [self imageChangeFromIndex:2];
            }else{
                [self imageChangeFromIndex:0];
            }
            break;
        case RingButtonType_gender:
            if(currentValue < 1.5){
                [self imageChangeFromIndex:0];
            }else{
                [self imageChangeFromIndex:1];
            }
            break;
        case RingButtonType_marriage:
            if(currentValue < 1.5){
                [self imageChangeFromIndex:0];
            }else{
                [self imageChangeFromIndex:1];
            }
            break;

        case RingButtonType_income:
            [self findImageForIncomeWithValue:currentValue];
            break;
        default:
            
            break;
    }
    
    
}

-(void)findImageForIncomeWithValue:(float)value{
    float x = 5.0;
    float y = 1.0;
    float d = 24;
    float t = value;
    float z = (t-y)/((x-y)/d);
    int result = (z*10+9)/10;
    _icon.image = [UIImage imageNamed:[NSString stringWithFormat:@"saving%d",24-(result == 24 ? 23 : result)]];
}
@end