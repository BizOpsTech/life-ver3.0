//
//  Question.m
//  Life Calculation
//
//  Created by BizOpsTech on 11/21/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "Question.h"

@implementation Question
@synthesize  answer = _answer;
@synthesize selectIndex = _selectIndex;

-(id) init:(NSString *)answer selectIndex:(int)index{
    if(self == [super init])
    {
        _answer = answer;
        _selectIndex = index;
    }
    return (self);
}
@end
