//
//  SWContentView.m
//  Life_20131116
//
//  Created by Snow on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "SWContentView.h"
#include <math.h>

@implementation SWContentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)init{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"SWContentView" owner:self options:nil] lastObject];
    
    if (self) {
        self.rsView.delegate = self;
    }
    
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    //    _currentNumberLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    //    _maxLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    //    _minLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    //    _minSubLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    //    _maxSubLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    
}

- (void)defineAlgorithmOfLogMethodBySlider:(UISlider*)swSlider{
    
    NSLog(@"max value %f",swSlider.maximumValue);
    double dA = 100*log(self.maxLabel.text.integerValue);
    double dB = 100*log(self.minLabel.text.integerValue == 0 ? 1 : self.minLabel.text.integerValue);
    
    swSlider.maximumValue = [[NSNumber numberWithDouble:dA] floatValue];
    swSlider.minimumValue = [[NSNumber numberWithDouble:dB] floatValue];
    
    float sliderValue = swSlider.value;
    double showResult = exp(sliderValue/100);
    int showResultOfInt = 0;
    showResultOfInt = showResult;
    
    if ( showResult > (showResult/10-1)*10) {
        if (showResult >80) showResultOfInt = showResult + 1;
        else showResultOfInt = showResult;;
    }else{
        showResultOfInt = showResult;
    }
    
    if(showResult < 5){
        showResultOfInt = showResult -1;
    }
    
    _currentNumberLabel.text = [NSString stringWithFormat:@"%d万",showResultOfInt];
    [self.delegate changeValueDelegateWithObject:self withValue:showResultOfInt];
    

}

- (void)sliderValueChanged:(UISlider*)swSlider{
    
    if (swSlider.tag == 724) {
        
        double dA = 100*log(2);
        double dB = 100*log(500);
        
        swSlider.minimumValue = [[NSNumber numberWithDouble:dA] floatValue];
        swSlider.maximumValue = [[NSNumber numberWithDouble:dB] floatValue];
        
        float x = swSlider.maximumValue;
        float y = swSlider.minimumValue;
        float d = 24;
        float t = swSlider.value;
        float z = (t-y)/((x-y)/d);
        int result = (z*10+9)/10;
        
        self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"saving%d",24-(result == 24 ? 23 : result)]] ;
        /*******************************************************/
        
        float sliderValue = swSlider.value;
        
        double showResult = exp(sliderValue/100);
        
        int showResultOfInt = 0;
        
        if (showResult>200 && showResult<=500) {
            showResultOfInt = showResult/10;
            showResultOfInt = showResultOfInt*10;
            
        }else if (showResult>500 && showResult<=2000){
            showResultOfInt =  showResult/50;
            showResultOfInt = showResultOfInt*50;
        }else if (showResult>2000){
            showResultOfInt = showResult/100+1;
            showResultOfInt = showResultOfInt*100;
        }else{
            showResultOfInt = showResult;
        }
        //NSLog(@"%f",showResult);
        
        _currentNumberLabel.text = [NSString stringWithFormat:@"%d万",showResultOfInt];
        [self.delegate changeValueDelegateWithObject:self withValue:showResultOfInt];
        
        return;
    }else if(swSlider.tag == 725){
        
        int current  = swSlider.value;
        if (current>= 18 && current <= 40) {
            self.imageView.image = _images[0];
        }else if (current>40 && current <=50){
            self.imageView.image = _images[1];
        }else{
            self.imageView.image = _images[2];
        }
        
    }else if(swSlider.tag == 726){
        
        
        
    }else if (swSlider.tag == 727){
        //Set if method's range
        if (_maxLabel.text.integerValue >= 100*log(1000)) {
            [self defineAlgorithmOfLogMethodBySlider:swSlider];
            return;
        }else{
        _currentNumberLabel.text = [NSString stringWithFormat:@"%d万",[NSNumber numberWithInt:swSlider.value].intValue];
        [self.delegate changeValueDelegateWithObject:self withValue:swSlider.value];
        }
        return;
        
    }else if (swSlider.tag == 728){
        _currentNumberLabel.text = [NSString stringWithFormat:@"%1.1f万",[NSNumber numberWithFloat:swSlider.value].floatValue];
        [self.delegate changeValueDelegateWithObject:self withValue:swSlider.value];
        return;
    
    }else{
        int index = 1;
        UIImage *isImage = _images[index - 1];
        if (isImage) {
            UIImage *image;
            if (_images.count >= 24) {
                
            }else{
                image = _images[index-1];
            }
            self.imageView.image = image;
        }
    }
    
    
    
    
    _currentNumberLabel.text = [NSNumber numberWithInt:swSlider.value].stringValue;
    [self.delegate changeValueDelegateWithObject:self withValue:swSlider.value];
    
}


@end
