//
//  ConfirmInfo.m
//  Insurance
//
//  Created by BizOpsTech on 11/8/13.
//  Copyright (c) 2013 RbBtSn0w. All rights reserved.
//

#import "ConfirmInfo.h"
#import "AppDelegate.h"

@interface ConfirmInfo ()
@property(nonatomic,strong)NSDateFormatter *dateFormatter;
@property(nonatomic,strong)AppDelegate *myAppDelegate;
@end

@implementation ConfirmInfo

//-(void)configItemB{
//    CGFloat diffH = 60;
//    CGFloat diffY = 40;
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7){
//        diffH = 20;
//        diffY = 0;
//    }
//    CGFloat itemBH = self.view.frame.size.height - _ItemViewA.frame.size.height -_ItemViewA.frame.origin.y - diffH;
//    CGFloat itemBY = _ItemViewA.frame.size.height + _ItemViewA.frame.origin.y + diffY;
//    _itemB.frame = CGRectMake(20,itemBY,self.view.frame.size.width -40, itemBH);
//    
//    for(NSUInteger i=0; i<[[_itemB subviews]count]; i++){
//        if([[[_itemB subviews]objectAtIndex:i] isKindOfClass:[UITextView class]]){
//            UITextView *tv = [[_itemB subviews]objectAtIndex:i];
//            tv.frame = CGRectMake(0, 33, 280, itemBH-76);
//        }
//    }
//    
//    
//    UIColor *borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0];
//    _itemB.layer.borderColor = borderColor.CGColor;
//    _itemB.layer.cornerRadius = 4.f;
//    _itemB.layer.borderWidth = 1.0f;
//    
//}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    _myAppDelegate = [[UIApplication sharedApplication]delegate];
    
    _dateFormatter = [[NSDateFormatter alloc]init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:1];
    NSDate *endingday = [gregorian dateByAddingComponents:offsetComponents toDate:today options:0];
    
    
    _txtDateFrom.text = [_dateFormatter  stringFromDate:today];
    _txtDateTo.text = [_dateFormatter stringFromDate:endingday];
    
    
    [self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    //if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7){
        //self.edgesForExtendedLayout = UIRectEdgeNone;
    //}
    
	// Do any additional setup after loading the view.
    [self strokeForTextField:_ItemViewA];
    
//    UIEdgeInsets labelInsets;
//    labelInsets.top=labelInsets.bottom=0;
//    labelInsets.left=labelInsets.right=12;
//sender.contentEdgeInsets
    //[self configItemB];
    
   // _allPrice.text = ((AppDelegate*)[UIApplication sharedApplication].delegate).AllPrice;
    
    _allPrice.text = [NSString stringWithFormat:@"%@元",_myAppDelegate.SumPremium];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self exitkeyboard:_ItemViewA];
}

-(void)strokeForTextField:(UIView *)view{
    NSUInteger count = [[view subviews]count];
    UIView *tempView;
    UIColor *borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0];
    
    for(NSUInteger i=0; i< count; i++){
        tempView = [[view subviews]objectAtIndex:i];
        if([tempView isKindOfClass:[UITextField class]]){
            UITextField *txt = [[view subviews]objectAtIndex:i];
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            txt.layer.borderColor = borderColor.CGColor;
            txt.layer.borderWidth = 1.f;
            txt.layer.cornerRadius = 4.f;
            txt.borderStyle = UITextBorderStyleLine;
            txt.leftView = paddingView;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }
    }
}

-(void)exitkeyboard:(UIView *)view{
    NSUInteger count = [[view subviews]count];
    UIView *tempView;
    for(NSUInteger i=0; i< count; i++){
        tempView = [[view subviews]objectAtIndex:i];
        if([tempView isKindOfClass:[UITextField class]]){
            [tempView resignFirstResponder];
        }
    }
}
- (IBAction)anctionWithBuyButton:(myButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.bizopstech.com"]];
}
@end
