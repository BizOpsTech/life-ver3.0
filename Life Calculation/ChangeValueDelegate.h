//
//  ChangeValueDelegate.h
//  Life_20131116
//
//  Created by Snow on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChangeValueDelegate <NSObject>

@optional
- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value;
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status;
- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index;

@end
