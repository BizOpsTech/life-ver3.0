//
//  AppDelegate.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "UIDevice+Resolutions.h"
#import "UserDb.h"
#import <UIKit/UIKit.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)UserDb *recodDb;
@property(copy,nonatomic)NSString *SumPremium;
@property(copy,nonatomic)NSString *SumCoverage;

@property(nonatomic,strong)UIFont *myFont;
@property(nonatomic)int surveyIndex;

@property (assign,nonatomic) BOOL IsKeepABitEffectiveNum;
@property (assign,nonatomic) BOOL answeredLife_accident_Question;
@property (assign,nonatomic) BOOL answeredHealthQuestion;
@property (assign,nonatomic) BOOL answeredRetirementQuestion;
@property (assign,nonatomic) BOOL answeredEducationQuestion;

@property(assign,nonatomic)BOOL isNotFirstVisitedForBasic;
@property(assign,nonatomic)BOOL isNotFirstVisitedForSurvey;



-(void)fixedConstraintForIOS7:(UIView *)tagView SuperView:(UIView *)superView;
-(void)setBgTextStyle:(UITextView *)tagView;
-(void)autoFixedForButton:(NSArray*)buttonArr;
@end
