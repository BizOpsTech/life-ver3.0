//
//  Recommended.h
//  Life Calculation
//
//  Created by BizOpsTech on 11/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Recommended : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *dataGrid;
@property(nonatomic,assign)buyModel myBuyModel;
@property(nonatomic,strong)NSMutableArray *gridDataArr;

@property (strong, nonatomic) IBOutlet UILabel *labelCoverage;
@property (strong, nonatomic) IBOutlet UILabel *labelPremium;

@end
