//
//  SummaryCell.h
//  Life Calculation
//
//  Created by BizOpsTech on 12/1/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryCell : UITableViewCell
@property(nonatomic,copy)NSString *title;
@property(nonatomic,assign)NSNumber *coverage;
@end
