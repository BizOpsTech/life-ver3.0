//
//  CustomResultCell.h
//  Life Calculation
//
//  Created by BizOpsTech on 11/15/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomResultCell : UITableViewCell
@property(copy,nonatomic)NSString *icon;
@property(copy,nonatomic)NSString *title;
@property(assign,nonatomic)float coverage;
@property(nonatomic,assign)int inPutRiskLevel;
@property(nonatomic,getter=setRiskLevel)int riskValue;

//- (IBAction)updateRiskLevel:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@end
