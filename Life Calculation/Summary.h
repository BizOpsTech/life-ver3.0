//
//  Summary.h
//  Life Calculation
//
//  Created by BizOpsTech on 12/1/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "calculatePriority.h"
#import "Calculate.h"
#import "UserDb.h"
#import "ResultData.h"
@interface Summary : NSObject
-(NSArray *)getInsuranceLevel:(UserDb *)source;
-(NSArray *)getInsuranceCoverage:(UserDb *)source;
-(NSMutableArray *)getInsuranceResult:(UserDb *)source;
-(NSMutableArray *)getInsuranceResult:(UserDb *)source ResultBaseArr:(NSArray *)ResultBase_Arr;
-(buyModel)getBuyModel:(UserDb *)source;
-(NSString *)getCategoryName:(insuranceModel)model;

@end
