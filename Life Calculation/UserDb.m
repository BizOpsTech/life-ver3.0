//
//  UserDb.m
//  Life Calculation
//
//  Created by BizOpsTech on 11/14/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "UserDb.h"

@implementation UserDb
-(id)init{
    self = [super init];
    if (self) {
        self.depositInsurance = 0;
        self.lifeInsurance = 0;
        self.accidentInsurance = 0;
        self.isHaveInsurance = 0;
    }
    return self;
}
@end
