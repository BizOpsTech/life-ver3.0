//
//  ProductList.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductListDelegate;
@interface ProductList : UITableViewController
@property(nonatomic,assign)insuranceModel iModel;
@property(nonatomic,assign)productOfInsurance recommendProductId;
@property(nonatomic,assign)float recommendCoverage;
@property(nonatomic,assign)int recommendterm;
@property(nonatomic,assign)float recommendPremium;
@property (nonatomic, assign) id<ProductListDelegate> delegate;
@property (nonatomic) NSUInteger superCellindex;

@end


@protocol ProductListDelegate <NSObject>

- (void)productListDelegate:(ProductList*)aProductList withChangeTerm:(int)term withChangeCoverage:(int)coverage withPremium:(float)premium withInsuranceModel:(insuranceModel)aInsuranceModel withSelectProductId:(productOfInsurance)productId;

@end