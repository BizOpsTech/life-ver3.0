//
//  ProductDetail.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "ProductDetail.h"
@interface ProductDetail()
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ProductDetail

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        if(self.viewHeight >0){
            CGRect myCGRect = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.viewHeight);
            self.frame = myCGRect;
        }
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
 
 •
}
*/
-(void)strokeViewToButtomBorder{
    UIView *headView = (UIView *)[self viewWithTag:8];
    headView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    headView.layer.borderWidth = 1.0;
}

-(void)setDetailContent:(NSString *)detailContent{
    
    UITextView *tv = (UITextView *)[self viewWithTag:9];
    tv.frame = CGRectMake(10, 50, self.frame.size.width, self.frame.size.height -50);
    tv.text = detailContent;
    
    [self strokeViewToButtomBorder];
    
    [self.delegate didDetailViewStateChage:YES];
}

- (IBAction)actionWithCloseButton:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, self.superview.frame.size.height, self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
        [self.delegate didDetailViewStateChage:NO];
    }];
}

@end
