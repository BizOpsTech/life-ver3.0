//
//  QuestionWithInsurance.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithInsurance.h"
#import "SWContentView.h"

@interface QuestionWithInsurance ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;

@property(nonatomic,strong)AppDelegate *app;
@end

@implementation QuestionWithInsurance




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _app = [[UIApplication sharedApplication]delegate];
    
    _btnArr = [NSArray arrayWithObjects:_btnTotal,_btnEndowment,_btnLife,_btnDisease,nil];
    
    #pragma mark 填充用户内容
    [self setValueToPage];
    [self addCustomViewOnScroll];
    
    [_app autoFixedForButton:_btnArr];
   // [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionWithButton:(UIButton *)sender {
    //_btnTotal,_btnEndowment,_btnLife,_btnDisease
    int page = 0;
    if([sender isEqual:_btnTotal]){
        page = 0;
       // [self changeBackgroundForButton:YES ButtonIndex:0];
        
    }else if([sender isEqual:_btnEndowment]){
        page = 1;
       // [self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnLife]){
        page = 2;
       // [self changeBackgroundForButton:YES ButtonIndex:2];
        
    }else if([sender isEqual:_btnDisease]){
        page = 3;
     //   [self changeBackgroundForButton:YES ButtonIndex:3];
        
    }else{
        return;
    }
    
    
    
    CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
}

#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    UIColor *borderColor;
    
    if(isEditStatu){
        borderColor = [UIColor colorWithRed:0/255.f green:204/255.f blue:255/255.f alpha:1.0];
        
        ////// 修改按钮样式 ///////////////////////////////////////////////////////////////////
        
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 2.0;
        btn.layer.borderColor = borderColor.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@"buttonBG"] forState:UIControlStateNormal];
        
        ////// 取消其他按钮样式 //////////////////////////////////////////////////////////////////
        [self didEditCompleteFromButton:btnIndex];
        
        
    }else{
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        btn.layer.borderWidth = 0;
    }
}


#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{
    if(_app.recodDb.isHaveInsurance >0){
        [self setValueToButton:0 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.isHaveInsurance/ UnitRMB]];
    }
    if(_app.recodDb.depositInsurance >0){
        [self setValueToButton:1 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.depositInsurance/ UnitRMB]];
    }
    if(_app.recodDb.lifeInsurance >0){
        [self setValueToButton:2 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.lifeInsurance/ UnitRMB]];
    }
    if(_app.recodDb.accidentInsurance >0){
        [self setValueToButton:3 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.accidentInsurance/ UnitRMB]];
    }
}

/*
 @property(nonatomic,assign)NSUInteger isHaveInsurance; //是否有社保
 @property(nonatomic,assign)NSUInteger depositInsurance; //储蓄型保险
 @property(nonatomic,assign)NSUInteger  lifeInsurance;   //传统型寿险
 @property(nonatomic,assign)NSUInteger  accidentInsurance;   //意外型寿险
 
 _btnTotal,_btnEndowment,_btnLife,_btnDisease,
 */
#pragma mark 记录选项值
-(void)getValueFromButton{

    _app.recodDb.isHaveInsurance = [_btnTotal.titleLabel.text integerValue]* UnitRMB;
    _app.recodDb.depositInsurance = [_btnEndowment.titleLabel.text integerValue]* UnitRMB;
    _app.recodDb.lifeInsurance = [_btnLife.titleLabel.text integerValue]* UnitRMB;
    _app.recodDb.accidentInsurance = [_btnDisease.titleLabel.text integerValue]* UnitRMB;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
}





#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    if(![UIDevice isRunningOniPhone5]){
        [self.view sendSubviewToBack:_tabScroll];
    }
}

- (void)addCustomViewOnScroll{
    
    self.tabScroll.pagingEnabled = YES;
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    
    self.tabScroll.contentSize = CGSizeMake(width*4, height);

    
    SWContentView *content1 = [[SWContentView alloc] init];
    content1.images = @[ImageName(@"social_insurance")];
    content1.imageView.image = ImageName(@"social_insurance");
    content1.delegate = self;
    content1.tag = KQuestion1;
    content1.rsView.intervalValue = 0;
    content1.swSlider.tag = 727;
    content1.swSlider.minimumValue = 0;
    content1.swSlider.maximumValue = 100;
    content1.minLabel.text = @"0";
    content1.maxLabel.text = @"100万";
    content1.minSubLabel.hidden=YES;
    if(_app.recodDb.isHaveInsurance >0){
        content1.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.isHaveInsurance/UnitRMB];
        content1.swSlider.value = (float)_app.recodDb.isHaveInsurance/UnitRMB;
    }else{
        content1.currentNumberLabel.text = @"0";
        content1.swSlider.value = 0;
    }
    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    
    SWContentView *content2 = [[SWContentView alloc] init];
    content2.images = @[ImageName(@"endowment_insurance")];
    content2.imageView.image = ImageName(@"endowment_insurance");
    content2.delegate = self;
    content2.tag = KQuestion2;
    content2.rsView.intervalValue = 0;
    content2.swSlider.tag = 727;
    content2.swSlider.minimumValue = 0;
    content2.swSlider.maximumValue = 100;
    content2.minLabel.text = @"0";
    content2.maxLabel.text = @"100万";
    content2.minSubLabel.hidden=YES;
    if(_app.recodDb.depositInsurance >0){
        content2.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.depositInsurance/UnitRMB];
        content2.swSlider.value = (float)_app.recodDb.depositInsurance/UnitRMB;
    }else{
        content2.currentNumberLabel.text = @"0";
        content2.swSlider.value = 0;
    }
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    
    SWContentView *content3 = [[SWContentView alloc] init];
    content3.images = @[ImageName(@"traditional_life")];
    content3.imageView.image = ImageName(@"traditional_life");
    content3.delegate = self;
    content3.tag = KQuestion3;
    content3.swSlider.tag = 727;
    content3.rsView.intervalValue = 0;
    content3.swSlider.minimumValue = 0;
    content3.swSlider.maximumValue = 100;
    
    content3.minLabel.text = @"0";
    content3.maxLabel.text = @"100万";
    
    content3.minSubLabel.hidden=YES;
    if(_app.recodDb.lifeInsurance >0){
        content3.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.lifeInsurance/UnitRMB];
        content3.swSlider.value = (float)_app.recodDb.lifeInsurance/UnitRMB;
        
    }else{
        content3.currentNumberLabel.text = @"0";
        content3.swSlider.value = 0;
    }
    
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
    
    SWContentView *content4 = [[SWContentView alloc] init];
    content4.images = @[ImageName(@"disease_protection")];
    content4.imageView.image = ImageName(@"disease_protection");
    content4.delegate = self;
    content4.tag = KQuestion4;
    content4.rsView.intervalValue = 0;
    content4.swSlider.tag = 727;
    content4.swSlider.minimumValue = 0;
    content4.swSlider.maximumValue = 100;
    content4.minLabel.text = @"0";
    content4.maxLabel.text = @"100万";
    content4.minSubLabel.hidden=YES;
    if(_app.recodDb.accidentInsurance >0){
        content4.swSlider.value = (float)_app.recodDb.accidentInsurance/UnitRMB;
        content4.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.accidentInsurance/UnitRMB];
    }else{
        content4.swSlider.value = 0;
        content4.currentNumberLabel.text = @"0";
    }
    [self addViewObject:content4 withHeight:height withWidth:width withAtIndex:3];
 
}


- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
    
    NSString *title;
    UIButton *cusButton;
    
    switch (tagTag) {
        case KQuestion1:
            cusButton = _btnTotal;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        case KQuestion2:
            cusButton = _btnEndowment;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        case KQuestion3:
            cusButton = _btnLife;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        case KQuestion4:
            cusButton = _btnDisease;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        default:
            break;
    }
    title = [title stringByAppendingString:@"万"];
    [self myCustomButton:cusButton withTitle:title];
}
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}

@end
