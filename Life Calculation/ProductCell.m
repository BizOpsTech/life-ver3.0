//
//  ProductCell.m
//  Life Calculation
//
//  Created by BizOpsTech on 11/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell
@synthesize IsChecked = _IsChecked;
@synthesize productId = _productId;
@synthesize rowIndex = _rowIndex;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

        
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)didChangeValueForCheckBox:(BOOL)statu{
    UIImage *img;
    img = statu?ImageName(@"UICheckBoxSelect"):ImageName(@"UICheckBoxUnSelect");
    [self.btnUICheckBox setImage:img forState:UIControlStateNormal];
}


- (IBAction)actionWithButton:(UIButton *)sender {
    _IsChecked = !self.IsChecked;
    [self didChangeValueForCheckBox:self.IsChecked];
    [self.delegate didSelectForRow:self CellStatus:_IsChecked];
}
@end
