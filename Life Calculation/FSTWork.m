//
//  FSTWork.m
//  Life_20131116
//
//  Created by Snow on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "FSTWork.h"

@interface FSTWork ()

- (void)defaultClickImage;


@end

@implementation FSTWork

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"FSTWork" owner:self options:nil] lastObject];
    if (self) {
        nikeImage = [UIImage imageNamed:@"yes"];
    }
    
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (IBAction)selectActionButton:(id)sender {
    
    UIButton *currentTapButton = (UIButton*)sender;
    
    switch (currentTapButton.tag) {
        case 601:
            [self selectedListAtIndex:1];
            break;
        case 602:
            [self selectedListAtIndex:2];
            break;
        case 603:
            [self selectedListAtIndex:3];
            break;
        case 604:
            [self selectedListAtIndex:4];
            break;
        case 605:
            [self selectedListAtIndex:5];
            break;
        case 606:
            [self selectedListAtIndex:6];
            break;
            
        default:
            break;
    }
    
}

- (void)defaultClickImage{
    _click1.image = nil;
    _click2.image = nil;
    _click3.image = nil;
    _click4.image = nil;
    _click5.image = nil;
    _click6.image = nil;
}

- (void)selectedListAtIndex:(NSUInteger)index{
    [self defaultClickImage];
    switch (index) {
        case 1:
            _click1.image = nikeImage;
            break;
        case 2:
            _click2.image = nikeImage;
            break;
        case 3:
            _click3.image = nikeImage;
            break;
        case 4:
            _click4.image = nikeImage;
            break;
        case 5:
            _click5.image = nikeImage;
            break;
        case 6:
            _click6.image = nikeImage;
            break;
        default:
            break;
    }
    
    [self.delegate changeValueDelegateWithObject:self withSelectedhRadio:index];
}

- (void)setIsState:(BOOL)isState{
    _isState = isState;
}


- (void)selectedButtonState:(BOOL)state{
    [self setIsState:state];
    [self.delegate chooseBoolDelegateWithObject:self withBoolStatus:state];
}


@end
