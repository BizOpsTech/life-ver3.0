//
//  QuestionWithHabit.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithHabit : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnFrequency;
@property (strong, nonatomic) IBOutlet UIButton *btnMany;

@property (strong, nonatomic) IBOutlet UIButton *btnIsSmoke;
@property (strong, nonatomic) IBOutlet UIButton *btnIsHasIllness;


- (IBAction)actionWithButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;

@end
