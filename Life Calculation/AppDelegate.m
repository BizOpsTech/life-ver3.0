//
//  AppDelegate.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "AppDelegate.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    _myFont = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:18];
    _recodDb = [UserDb new];
    return YES;
}
							
-(void)fixedConstraintForIOS7:(UIView *)tagView SuperView:(UIView *)superView{
    
    if([UIDevice isRunningOniPhone5]){
        tagView.translatesAutoresizingMaskIntoConstraints = NO;
        NSLayoutConstraint *IOS7Constraint = [NSLayoutConstraint constraintWithItem:tagView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-29.0];        
        [superView addConstraint:IOS7Constraint];
    }
}
-(void)setBgTextStyle:(UITextView *)tagView{
        NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.alignment = NSTextAlignmentLeft;
        paragraph.firstLineHeadIndent = 0; //第一行第一个单词的缩进
        paragraph.paragraphSpacingBefore = 5.0; //第二行与前一行的间距
        paragraph.lineSpacing = 4; //在行与行之间再增加额外的间距
        paragraph.hyphenationFactor = 0.0; //断字因子
    
//        NSShadow* myShadow = [[NSShadow alloc] init];
//        myShadow.shadowBlurRadius = 2.0;
//        myShadow.shadowColor = [UIColor clearColor];
//        myShadow.shadowOffset = CGSizeMake(1,1);
    
        NSDictionary* attributes = @{
          //NSForegroundColorAttributeName: [UIColor blackColor],
         // NSShadowAttributeName: myShadow,
          NSParagraphStyleAttributeName:paragraph,
         // NSBackgroundColorAttributeName: [UIColor clearColor],
          NSFontAttributeName:_myFont
       };
    
    [self UITextViewSelectable:tagView Selectable:YES];
    NSMutableAttributedString* aString =  [[NSMutableAttributedString alloc] initWithAttributedString:tagView.attributedText];
    
   
    
    [aString addAttributes:attributes range:NSMakeRange(0,tagView.text.length-1)];
    tagView.attributedText = aString;
    [self UITextViewSelectable:tagView Selectable:NO];
}

-(void)UITextViewSelectable:(UITextView *)textView Selectable:(BOOL)state{
    if([[[UIDevice currentDevice]systemVersion]floatValue] >= 7){
        textView.selectable = state;
    }
}

-(void)autoFixedForButton:(NSArray*)buttonArr{
    if(![UIDevice isRunningOniPhone5]){
        for (NSUInteger i=0; i<buttonArr.count; i++) {
            UIButton *tempBtn = [buttonArr objectAtIndex:i];
            tempBtn.translatesAutoresizingMaskIntoConstraints = YES;

            tempBtn.frame = CGRectMake(tempBtn.frame.origin.x,
                                       tempBtn.frame.origin.y-4,
                                       tempBtn.frame.size.width,
                                       32);
        }
    }
}
@end
