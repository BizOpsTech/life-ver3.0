//
//  CVCell.m
//  CF_00
//
//  Created by BizOpsTech on 9/5/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "CVCell.h"
#import "CustomCellBackground.h"

#import <QuartzCore/QuartzCore.h>
#import <Quartzcore/CALayer.h>
@implementation CVCell
@synthesize icon;
@synthesize title;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        if(frame.size.width <130) frame.size.width = 130;
        if(frame.size.height <130) frame.size.height = 130;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // change to our custom selected background view
        CustomCellBackground *backgroundView = [[CustomCellBackground alloc] initWithFrame:CGRectZero];
        self.selectedBackgroundView = backgroundView;
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGRect iconRect = CGRectMake(25, 10, 80, 80);
    UIImageView *iconView = [[UIImageView alloc]initWithFrame:iconRect];
    iconView.backgroundColor=[UIColor clearColor];
    [iconView setImage:self.icon];
    [self addSubview:iconView];
    
    CGRect titleRect = CGRectMake(20, 100, 80, 21);
    UILabel *titleView = [[UILabel alloc]initWithFrame:titleRect];
    titleView.text = self.title;
    titleView.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    titleView.textColor = [UIColor lightTextColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.backgroundColor=[UIColor clearColor];
    
    [self addSubview:titleView];
    [self.layer setCornerRadius:8.0];
    UIColor *cellbg = [[UIColor alloc]initWithRed:190.f/255 green:190.f/255 blue:190.f/255 alpha:1.0];
    [self.layer setBackgroundColor:cellbg.CGColor];
    
}
@end
