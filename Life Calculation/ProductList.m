//
//  ProductList.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "CalculationInsurance.h"
#import "ProductDB.h"
#import "ProductList.h"
#import "CustomProductCell.h"
#import "PickerView.h"
#import "ProductDetail.h"

#import "DHxlsReader.h"
#import "AppDelegate.h"


////////////////////////////////// data grid //////////////
@interface ProductList ()<ProductViewDelegate,ProductDetailViewDelegate>
@property(nonatomic,strong)NSArray *ProductSource;//数据源

////////////////////////////////// data grid //////////////
@property(nonatomic,strong)NSMutableArray *dataProvider;
@property(strong,nonatomic)NSArray *yearRangArr;
@property(strong,nonatomic)NSArray *coverageRangArr;
@property(strong,nonatomic)CustomProductCell *currentSelectedRowView;
@property(strong,nonatomic)CustomProductCell * currentEditRowView;
///////////// pickerView ///////////////////////////
@property(strong, nonatomic) PickerView * pickerView;
@property(assign,nonatomic) BOOL IsOpenLocateView;
@property(strong,nonatomic) UITextField * TextFieldForPicker;


///////////// detailView ///////////////////////////
@property(strong,nonatomic)ProductDetail *detailView;
@property(nonatomic,assign)CGFloat cellHeight;
@property(nonatomic,assign)BOOL isVisible;
@property(nonatomic,assign)CGFloat offsetHeightForDiffIOS;

@property (nonatomic, strong)NSArray *rateInfosOfCurrentAge;

@end

@implementation ProductList


#pragma mark    -

- (void)setAtCell:(CustomProductCell*)cell withPremium:(float)premium withRermForP:(float)termP{
    
    int termP_I = termP;
    float rate = [(NSString*)[[self.rateInfosOfCurrentAge firstObject] objectForKey:[NSString stringWithFormat:@"%d",termP_I]] floatValue];
    premium = premium/10000 * rate;
    [cell setPremium:premium];
}

- (NSDictionary*)parseExcel{
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"product_name.xls"];
    //	NSString *path = @"/tmp/test.xls";
    
	// xls_debug = 1; // good way to see everything in the Excel file
	
	DHxlsReader *reader = [DHxlsReader xlsReaderWithPath:path];
	assert(reader);
    
    
    NSMutableArray *keyNames = [NSMutableArray array];
    NSMutableArray *ageNames = [NSMutableArray array];
    
    NSMutableArray *rows = nil;
    NSMutableDictionary *dicRows = nil;
    NSMutableDictionary *tables = [NSMutableDictionary dictionary];
    
    
	[reader startIterator:0];
	
	while(YES) {
		DHcell *cell = [reader nextCell];
		if(cell.type == cellBlank) break;
        
        if (cell.col>1) {
            if (cell.row == 1) {
                NSString *strInt = [NSString stringWithFormat:@"%d",cell.str.intValue];
                [keyNames addObject:strInt];
            }else{
                if (rows) {
                    [dicRows setValue:cell.str forKey:keyNames[cell.col-2]];
                }else{
                    rows = [NSMutableArray array];
                    dicRows = [NSMutableDictionary dictionary];
                    [dicRows setValue:cell.str forKey:keyNames[cell.col-2]];
                }
            }
        }else{
            if (cell.row > 1){
                NSString *strInt = [NSString stringWithFormat:@"%d",cell.val.intValue];
                [ageNames addObject:strInt];
                if (rows) {
                    [rows addObject:dicRows];
                    [tables setObject:rows forKey:[ageNames objectAtIndex:cell.row-3]];
                    dicRows = nil;
                    rows = nil;
                }else{
                    rows = [NSMutableArray array];
                    dicRows = [NSMutableDictionary dictionary];
                }
                
            }
        }
        
	}
    
    if (rows) {
        [rows addObject:dicRows];
        [tables setObject:rows forKey:ageNames.lastObject];
        dicRows = nil;
        rows = nil;
    }

    
    
    return tables;
}

- (NSArray*)parseExcelFromCurrentUserAge{
    
    int age = [(AppDelegate*)[UIApplication sharedApplication].delegate recodDb].age;
    NSString *strAge = [NSString stringWithFormat:@"%d",age];
    NSDictionary *dic = [self parseExcel];
    
    return [dic objectForKey:strAge];;
    
}



//****************************************************************************************************************//

- (NSDictionary*)loadFileOfPlit:(NSString*)name{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *fileName = [NSString stringWithFormat:@"%@.plist",name];
    NSString *filePath = [path stringByAppendingPathComponent:fileName];
    NSDictionary *plistData = [NSDictionary dictionaryWithContentsOfFile:filePath];
    return plistData;
}

-(void)setRangArrWithInsuranceModel:(insuranceModel)model{
    int begin,end,step;
    NSMutableArray *rangArr = [[NSMutableArray alloc]init];
    
    
    switch (model) {
        case insuranceModel_Traditional:
            begin = step = 10;end = 200;
            break;
        case insuranceModel_Accidental:
            begin = step = 10;end=200;
            break;
        case insuranceModel_Healthy:
            begin = step = 5; end=100;
            break;
        case insuranceModel_Pension:
            begin = step = 5; end=100;
            break;
        case insuranceModel_Education:
            begin = step = 2; end=20;
            /*
                term
                a:[1,3,6,9] b:[1,3,5,10] c:[1,3,5] d:[1,3,5]
             */
            break;
        default:
            begin = step = 10;end = 200;
            break;
    }
    
    
    ///////// 生成滚轮选项 ////////////
    for(int i=begin; i<= end;i=i+step){
        [rangArr addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    _coverageRangArr = rangArr;
}

#pragma mark 获取产品数据源
-(NSArray *)getProductSource:(insuranceModel)model{
    NSDictionary *list = [self loadFileOfPlit:@"ProductData"];
    NSArray *db;
    NSString *dbkey;
    switch (model) {
        case insuranceModel_Traditional:
            dbkey = @"Traditional";
            break;
        case insuranceModel_Accidental:
            dbkey = @"Accidental";
            break;
        case insuranceModel_Healthy:
            dbkey = @"Healthy";
            break;
        case insuranceModel_Pension:
            dbkey = @"Pension";
            break;
        case insuranceModel_Education:
            dbkey = @"Education";
            break;
        default:
            dbkey = @"Traditional";
            break;
    }
    db = [list objectForKey:dbkey];
    return db;
}

#pragma mark 获取产品类型
-(productOfInsurance)getProductType:(insuranceModel)model productIndex:(NSUInteger)index{
    productOfInsurance pType;
    switch (model) {
        case insuranceModel_Traditional:
            switch (index) {
                case 0:
                    pType = product_TermElite;
                break;
                case 1:
                    pType = product_TermEssential;
                break;
                case 2:
                    pType = product_EndowmentPremier;
                break;
                case 3:
                    pType = product_UniversalProtector;
                break;
                default:
                    pType = product_TermElite;
                break;
            }
            break;
        case insuranceModel_Accidental:
            switch (index) {
                case 0:
                    pType = product_AccidentalMedicalReimbursement;
                    break;
                case 1:
                    pType = product_AccidentComprehensive;
                    break;
                case 2:
                    pType = product_AccidentProtector;
                    break;
                case 3:
                    pType = product_AccidentBase;
                    break;
                default:
                    pType = product_AccidentalMedicalReimbursement;
                    break;
            }
            break;
        case insuranceModel_Healthy:
            switch (index) {
                case 0:
                    pType = product_SuperHealthCare;
                    break;
                case 1:
                    pType = product_CriticalIllnessA;
                    break;
                case 2:
                    pType = product_CriticalIllnessB;
                    break;
                case 3:
                    pType = product_RegalCriticalIllness;
                    break;
                default:
                    pType = product_SuperHealthCare;
                    break;
            }
            break;
        case insuranceModel_Pension:
            switch (index) {
                case 0:
                    pType = product_RetirementBaseA;
                    break;
                case 1:
                    pType = product_PremierRetirementA;
                    break;
                case 2:
                    pType = product_RetirementEndowment;
                    break;
                case 3:
                    pType = product_PremierRetirementB;
                    break;
                default:
                    pType = product_RetirementBaseA;
                    break;
            }
            break;
        case insuranceModel_Education:
            switch (index) {
                case 0:
                    pType = product_educationPlusA;
                    break;
                case 1:
                    pType = product_educationPlusB;
                    break;
                case 2:
                    pType = product_educationPremier;
                    break;
                case 3:
                    pType = product_educationBase;
                    break;
                default:
                    pType = product_educationPlusA;
                    break;
            }
            break;
    }
    return pType;
}
#pragma mark 视图加载
- (void)viewDidLoad
{
    [super viewDidLoad];

    
    _offsetHeightForDiffIOS = [[[UIDevice currentDevice]systemVersion]floatValue] >= 7 ? 60 : 0;
    
    
    _ProductSource = [self getProductSource:_iModel];
    [self setRangArrWithInsuranceModel:_iModel];
    
    ProductDB *p1 = [ProductDB new];
    ProductDB *p2 = [ProductDB new];
    ProductDB *p3 = [ProductDB new];
    ProductDB *p4 = [ProductDB new];
    
    //that's testing excel insert at row 2
    if (_iModel == insuranceModel_Healthy) {
        ProductDB *p5 = [ProductDB new];
        _dataProvider = [NSMutableArray arrayWithObjects:p1,p2,p3,p4,p5, nil];
        self.rateInfosOfCurrentAge = [self parseExcelFromCurrentUserAge];
    }else{
        _dataProvider = [NSMutableArray arrayWithObjects:p1,p2,p3,p4, nil];
    }
    
    
    for(NSUInteger i=0; i< _dataProvider.count; i++){
        ProductDB *tempDB = [_dataProvider objectAtIndex:i];
        tempDB.productType = [self getProductType:_iModel productIndex:i];
        if(i < 4){
            if(tempDB.productType == _recommendProductId){
                tempDB.isRecommended = YES;
                tempDB.years = _recommendterm;
                tempDB.coverage = _recommendCoverage;
                tempDB.premium =  _recommendPremium;
                tempDB.title = [[_ProductSource objectAtIndex:i]objectForKey:@"name"];
                tempDB.detailContent = [[[_ProductSource objectAtIndex:i]objectForKey:@"detail"]stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
                
            }else{
                tempDB.isRecommended = NO;
                tempDB.years = [[[[_ProductSource objectAtIndex:i]objectForKey:@"term"]objectAtIndex:0]intValue];
                tempDB.coverage = 0.0;
                tempDB.premium = [CalculationInsurance returnPremium:_iModel SpecificInsurance:tempDB.productType Coverage:tempDB.coverage Year:tempDB.years];
                tempDB.title = [[_ProductSource objectAtIndex:i]objectForKey:@"name"];
                tempDB.detailContent = [[[_ProductSource objectAtIndex:i]objectForKey:@"detail"]stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            }
            
        }else{
            tempDB.isRecommended = NO;
            tempDB.years = 0;
            tempDB.coverage = 0.0;
            tempDB.premium = [CalculationInsurance returnPremium:_iModel SpecificInsurance:tempDB.productType Coverage:tempDB.coverage Year:tempDB.years];
            
            tempDB.title = @"Test Excel";
            tempDB.detailContent =@"Description info from EXcel";
        }
    }
    
    
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(saveViewData:)];
    //[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystem target:self action:@selector(saveViewData:)];
    self.navigationItem.rightBarButtonItem = doneBarButton;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - tableView内容上下偏移
//// tableView 向上 或 向下活动
-(void)slideVerticalWithTableView:(CGFloat)slideLen{
    slideLen += _offsetHeightForDiffIOS;
    [UITableView animateWithDuration:0.3 animations:^{
        self.tableView.contentInset = UIEdgeInsetsMake(slideLen, 0, 0, 0);
    }];
    
}


/////////////////  productdetail delegate //////////////////////
#pragma mark - productdetail delegate
-(void)didDetailViewStateChage:(BOOL)visible{
    _isVisible = visible;
    if(!visible){
        [self slideVerticalWithTableView:0];
    }
}



/////////////////  productCell delegate //////////////////////
#pragma mark - productCell delegate
-(void)HandleSingleSelection:(CustomProductCell *)xibView{
    //当前选择行 和 记录的选择行 不同，且当前行状态为选中
    if([xibView isEqual:_currentSelectedRowView] == NO && xibView.statu == YES){
        ///// 取消之前的选中
        [_currentSelectedRowView actionWithTitleButton:nil];
        
        /////更新当前选择行
        _currentSelectedRowView = xibView;
        
    }else if ([xibView isEqual:_currentSelectedRowView] == YES && xibView.statu == NO){
        _currentSelectedRowView = nil;
    }

}


-(void)showDidSelectRowDetailView:(NSUInteger)rowIndex{
    if(!_isVisible){
        CGFloat tableHeight = self.view.frame.size.height;
        CGFloat tableWidth = self.view.frame.size.width;
        CGFloat detailHeight = tableHeight - _cellHeight - _offsetHeightForDiffIOS;
        CGFloat slidUpLen = rowIndex * _cellHeight;
        
        //////// 加载详细页面 //////
        ProductDB * db = [_dataProvider objectAtIndex:rowIndex];
        _detailView = [[[NSBundle mainBundle] loadNibNamed:@"ProductDetail" owner:self options:nil] lastObject];
        _detailView.delegate = self;
        _detailView.frame = CGRectMake(0, tableHeight, tableWidth,detailHeight);    
        _detailView.detailContent = db.detailContent;
        [self.view.superview addSubview:_detailView];
        
        ////// 详细页向上滑动
        [UIView animateWithDuration:0.3 animations:^{
            _detailView.frame = CGRectMake(0, _cellHeight+_offsetHeightForDiffIOS,tableWidth, detailHeight);
        }];
        
        ////// 主页向上滑动
        [self slideVerticalWithTableView:-slidUpLen];
    }
}

-(void)updatePremiumWithValue:(CustomProductCell *)cell{
    int term = [cell.txtYears.text intValue];
    int coverage = [cell.txtCoverage.text intValue]*10000;
    
    if (_iModel == insuranceModel_Healthy && [cell.btnTitle.titleLabel.text isEqualToString:@"Test Excel"]) {
        [self setAtCell:cell withPremium:coverage withRermForP:term];
    }else{
        float premium = [CalculationInsurance returnPremium:_iModel SpecificInsurance:cell.productType Coverage:coverage Year:term];
        [cell setPremium:premium];
    }
}

#pragma mark 显示编辑面板
-(void)showEditViewForTextField:(CustomProductCell *)xibView EditIndex:(int)index SelectRowIndex:(NSUInteger)rowIndex{

    CGFloat slidUpLen = rowIndex * _cellHeight;
    [self slideVerticalWithTableView:-slidUpLen];
    
    //记录当前编辑行
    _currentEditRowView = xibView;
    
    switch (index) {
        case 0:
            [self openSelect:xibView.txtCoverage dataSource:_coverageRangArr componentTitle:@"Coverage"];
            break;
            
        case 1:
            if(_iModel == insuranceModel_Education){
                switch (rowIndex) {
                    case 0:
                        _yearRangArr = [NSArray arrayWithObjects:@"1",@"3",@"5",@"9", nil];
                        break;
                    case 1:
                        _yearRangArr = [NSArray arrayWithObjects:@"1",@"3",@"5",@"10", nil];
                        break;
                    case 2:
                        _yearRangArr = [NSArray arrayWithObjects:@"1",@"3",@"5", nil];
                        break;
                    case 3:
                        _yearRangArr = [NSArray arrayWithObjects:@"1",@"3",@"5", nil];
                        break;
                }
            }else{
                _yearRangArr = [NSArray arrayWithObjects:@"1",@"5",@"10",@"20", nil];
                if (rowIndex == 4) {
                    
                    NSArray *ageS = [self.rateInfosOfCurrentAge.firstObject allKeys];
                    NSSortDescriptor *sortDesciptor = [NSSortDescriptor sortDescriptorWithKey:nil
                                                                                    ascending:YES
                                                                                   comparator:^NSComparisonResult(id obj1, id obj2) {
                                                                                       return [obj1 compare:obj2 options:NSNumericSearch];
                                                                                   }];
                    NSArray *sortDesciptors = [NSArray arrayWithObject:sortDesciptor];
                    NSArray *sortArray  = [ageS sortedArrayUsingDescriptors:sortDesciptors];
                    
                    if (!sortArray) _yearRangArr =[NSArray arrayWithObjects:@"Null Age",@"1",@"5",@"10",@"20", nil];
                    else self.yearRangArr = [NSArray arrayWithArray:sortArray];
                    
                }
            }            
            [self openSelect:xibView.txtYears dataSource:_yearRangArr componentTitle:@"Term"];
            break;
        default:
            break;
    }
}

/////////////////  pickerView delegate //////////////////////
-(void)openSelect:(UITextField *)textField dataSource:(NSArray *)arr componentTitle:(NSString *)title{
    if(self.pickerView == nil){
        self.pickerView = [[PickerView alloc] initWithTitle:title delegate:self dataSource:arr];
    }
    if(self.IsOpenLocateView == NO)
    {
        [self.pickerView ChageData:arr];
        [self.pickerView ChageTitle:title];
        self.IsOpenLocateView = YES;
        self.TextFieldForPicker = textField;
        [self.pickerView showInView:self.view.superview];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self slideVerticalWithTableView:0];
    self.IsOpenLocateView = NO;
    if(buttonIndex == 1){
        NSString *updateValue;
        PickerView * pickView = (PickerView *)actionSheet;
        updateValue = pickView.selectedValue;
        if(![updateValue isEqualToString:@""] && updateValue != nil){
            self.TextFieldForPicker.text = updateValue;
            [self updatePremiumWithValue:_currentEditRowView];
        }
    }
}

/////////////////  Table view data source //////////////////////
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    _cellHeight = (tableView.frame.size.height - _offsetHeightForDiffIOS)/_dataProvider.count;
    return _cellHeight;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataProvider count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //static BOOL nibsRegistered = NO;
    CustomProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"CustomProductCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        //nibsRegistered = YES;
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
        
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSUInteger row = [indexPath row];
    ProductDB *celldb = [_dataProvider objectAtIndex:row];
    cell.rowIndex = row;
    cell.delegate = self;
    cell.title = celldb.title;
    cell.premium = celldb.premium;
    cell.coverage = celldb.coverage;
    cell.years = celldb.years;
    cell.productType = celldb.productType;
    cell.isDefaultSelect = celldb.isRecommended;
    if(celldb.isRecommended){_currentSelectedRowView = cell;}
    return cell;
}


#pragma mark    -Save
- (void)saveViewData:(id)sender{
    productOfInsurance productId;
    int term,coverage;
    float premium;
    
    if(_currentSelectedRowView == nil){
        productId = product_None;
        term = 0;
        coverage = 0;
        premium = 0;
        
    }else{
        productId = _currentSelectedRowView.productType;
        term = [_currentSelectedRowView.txtYears.text intValue];
        coverage = [_currentSelectedRowView.txtCoverage.text intValue]*10000;
        premium = [CalculationInsurance returnPremium:_iModel SpecificInsurance:_currentSelectedRowView.productType Coverage:coverage Year:term];
        [_currentSelectedRowView setPremium:premium];
    }

    if ([self.delegate respondsToSelector:@selector(productListDelegate:withChangeTerm:withChangeCoverage:withPremium:withInsuranceModel: withSelectProductId:)]) {
        
        [self.delegate productListDelegate:self withChangeTerm:term withChangeCoverage:coverage withPremium:premium withInsuranceModel:_iModel withSelectProductId:productId];
        [self.navigationController popViewControllerAnimated:YES];
    }

}
@end
