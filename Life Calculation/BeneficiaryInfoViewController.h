//
//  BeneficiaryInfoViewController.h
//  Life Calculation
//
//  Created by Snow on 11/23/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BeneficiaryInfoDelegate;
@interface BeneficiaryInfoViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *idLabel;
@property (weak, nonatomic) IBOutlet UITextField *birthDataLabel;
@property (weak, nonatomic) IBOutlet UITextField *relationshipLabel;
@property (weak, nonatomic) IBOutlet UITextField *ratioLabel;

@property (nonatomic,assign) id<BeneficiaryInfoDelegate> delegate;
@end

@protocol BeneficiaryInfoDelegate <NSObject>

- (void)beneficiaryInfoViewController:(BeneficiaryInfoViewController*)aBIVC withInfos:(NSDictionary*)infos;

@end