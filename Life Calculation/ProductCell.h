//
//  ProductCell.h
//  Life Calculation
//
//  Created by BizOpsTech on 11/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProductCellViewDelegate;
@interface ProductCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *CategoryName;
@property (strong, nonatomic) IBOutlet UIButton *btnUICheckBox;
@property (strong, nonatomic) IBOutlet UILabel *labelCoverage;
@property (strong, nonatomic) IBOutlet UILabel *labelTerm;
@property (strong, nonatomic) IBOutlet UILabel *labelPremium;
@property(nonatomic)productOfInsurance productId;
@property(nonatomic)NSUInteger rowIndex;





@property(nonatomic,assign)id<ProductCellViewDelegate>delegate;
@property(nonatomic,assign)BOOL IsChecked;
-(void)didChangeValueForCheckBox:(BOOL)statu;
@end
@protocol ProductCellViewDelegate <NSObject>
-(void)didSelectForRow:(ProductCell *)xibCell CellStatus:(BOOL)statu;
@optional
@end
