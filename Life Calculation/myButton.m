//
//  myButton.m
//  CF_00
//
//  Created by BizOpsTech on 8/29/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

/*
beginColor = [[UIColor alloc]initWithRed:86.f/255 green:151.f/255 blue:245.f/255 alpha:1.0];
endColor = [[UIColor alloc]initWithRed:36.f/255 green:91.f/255 blue:174.f/255 alpha:1.0];
_ConfigureButton.bgColorArr=[NSArray arrayWithObjects:(id)beginColor.CGColor,(id)endColor.CGColor,nil];
 */

#import "myButton.h"
#import <QuartzCore/QuartzCore.h>
#import <Quartzcore/CALayer.h>

@interface myButton()
@property(nonatomic,retain)UIColor *beginColor;
@property(nonatomic,retain)UIColor *endColor;

@property(nonatomic,retain)UIColor *beginHoverColor;
@property(nonatomic,retain)UIColor *endHoverColor;

@end


@implementation myButton
@synthesize buttonRole = _buttonRole;
@synthesize cornerRadius = _cornerRadius;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        if(frame.size.height < 32){
            frame.size.height = 32;
        }
        
        if(frame.size.width < 60){
            frame.size.width = 60;
        }
        
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 
 if (_hoverColorArr == nil) {
 _beginHoverColor = [[UIColor alloc]initWithRed:180.f/255 green:180.f/255 blue:190.f/255 alpha:1.0];
 _endHoverColor = [[UIColor alloc]initWithRed:136.f/255 green:136.f/255 blue:150.f/255 alpha:1.0];
 _hoverColorArr=[NSArray arrayWithObjects:(id)_beginHoverColor.CGColor,(id)_endHoverColor.CGColor,nil];
 
 }

*/
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self.layer setCornerRadius:[_cornerRadius floatValue]];
    [self.layer setMasksToBounds:YES];
    
    if(_bgColorArr == nil){
        _beginColor = [[UIColor alloc]initWithRed:86.f/255 green:151.f/255 blue:245.f/255 alpha:1.0];
        _endColor = [[UIColor alloc]initWithRed:36.f/255 green:91.f/255 blue:174.f/255 alpha:1.0];
        _bgColorArr=[NSArray arrayWithObjects:(id)_beginColor.CGColor,(id)_endColor.CGColor,nil];
    };
    
    if (_hoverColorArr == nil) {
        _beginHoverColor = [[UIColor alloc]initWithRed:180.f/255 green:180.f/255 blue:190.f/255 alpha:1.0];
        _endHoverColor = [[UIColor alloc]initWithRed:136.f/255 green:136.f/255 blue:150.f/255 alpha:1.0];
        _hoverColorArr=[NSArray arrayWithObjects:(id)_beginHoverColor.CGColor,(id)_endHoverColor.CGColor,nil];
        
    }

    [self gradientConfig:rect ColorArr:_bgColorArr];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CAGradientLayer *layer = [self.layer.sublayers objectAtIndex:0];
    layer.colors = _hoverColorArr;
    
    [[self nextResponder]touchesBegan:touches withEvent:event];
    [super touchesBegan:touches withEvent:event];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    CAGradientLayer *layer = [self.layer.sublayers objectAtIndex:0];
    layer.colors = _bgColorArr;
    
    [[self nextResponder]touchesEnded:touches withEvent:event];
    [super touchesEnded:touches withEvent:event];
}

-(void)gradientConfig:(CGRect)rect ColorArr:(NSArray*)colorArr{
    CAGradientLayer *gradient = [CAGradientLayer layer];    
    gradient.frame = rect;    
    gradient.colors = colorArr;
    [self.layer insertSublayer:gradient atIndex:0];
}


@end
