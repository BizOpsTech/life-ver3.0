//
//  QuestionWithSpending.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithSpending : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;

- (IBAction)actionWithButton:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnSpending;
@property (strong, nonatomic) IBOutlet UIButton *btnRent;
@property (strong, nonatomic) IBOutlet UIButton *btnLoans;

@end
