//
//  AppointmentViewController.h
//  CF_00
//
//  Created by BizOpsTech on 9/5/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myButton.h"
@interface AppointmentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *appointmentLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *dataPicker;
- (IBAction)setAppointmentDate:(myButton *)sender;
@property (strong, nonatomic) IBOutlet myButton *UIButtonSetDatetime;



@end
