//
//  Calculate.h
//  Life Calculate
//
//  Created by Snow on 11/14/13.
//  Copyright (c) 2013 RbBtSn0w. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculate : NSObject


#pragma mark    Tranditional Life
/*
 Traditional
 @yearlyIncom:年收入
 @yearyExpense:年费用
 @retiredAge：退休年龄：女55 男60
 @currentAge：当前年龄
 */

- (float)calculateTraditionalLifeCoverageByYearlyIncome:(float)fA withSelfYearlyExpense:(float)fB withIsRetiredOfMan:(BOOL)isRetiredOfMan withCurrentAge:(float)fCA withEndowmentInsurance:(float)fEI withLife:(float)fLife;

#pragma mark    Accident
/*
 Accident
 @yearlyIncom:年收入
 @yearyExpense:年费用
 @retiredAge：退休年龄：女55 男60
 @currentAge：当前年龄
 */


- (float)calculateAccidentCoverageByYearlyIncome:(float)fA withSelfYearlyExpense:(float)fB withIsRetiredOfMan:(BOOL)isRetiredOfMan withCurrentAge:(float)fCA withEndowmentInsurance:(float)fEI withLife:(float)fLife withAD:(float)fADP;
#pragma mark    Health
/*
 Health
 @cityType:城市等级
 @currentAge：当前年龄
 */

- (float)calculateHealthCoverageByCityType:(float)fCT withCurrentAge:(float)fCA withSocialInsurance:(float)fSI withAD:(float)fADP;

#pragma mark    Retirement
/*
 Retirement
 @expectedAge:
 @isRetiredOfMan:是否退休
 @currentAge:当前年龄
 @yearlyExpenseNow:年费用
 */

- (float)calculateRetirementCoverageByExpectedAge:(float)fEA withIsRetiredOfMan:(BOOL)isRetiredOfMan withCurrentAge:(float)fCA withYearlyExpenseNow:(float)fYEN withEndowmentInsurance:(float)fEI;
#pragma mark    Children education
/*
 @fA:
 @fB:
 @fn：
 */

//Children education
//@ages:孩子年龄

- (float)calculateChildrenEducationCoverageByChilds:(NSUInteger)childsCount withCollageAge:(NSUInteger)collageAge withIsForeignCollage:(BOOL)isForeignCollage;


@end
