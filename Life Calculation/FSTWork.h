//
//  FSTWork.h
//  Life_20131116
//
//  Created by Snow on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeValueDelegate.h"

@protocol ChangeValueDelegate;
@interface FSTWork : UIView{
    UIImage *nikeImage;
}

@property (nonatomic, assign) id<ChangeValueDelegate> delegate;

@property (nonatomic, readonly) BOOL isState;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UIButton *button5;
@property (weak, nonatomic) IBOutlet UIButton *button6;

@property (weak, nonatomic) IBOutlet UIImageView *click1;
@property (weak, nonatomic) IBOutlet UIImageView *click2;
@property (weak, nonatomic) IBOutlet UIImageView *click3;
@property (weak, nonatomic) IBOutlet UIImageView *click4;
@property (weak, nonatomic) IBOutlet UIImageView *click5;
@property (weak, nonatomic) IBOutlet UIImageView *click6;
- (void)selectedListAtIndex:(NSUInteger)index;
@end
