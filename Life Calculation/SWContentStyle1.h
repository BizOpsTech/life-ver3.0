//
//  SWContentStyle1.h
//  Life_20131116
//
//  Created by Snow on 11/18/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioGroupView2.h"
#import "ChangeValueDelegate.h"

@protocol ChangeValueDelegate;
@interface SWContentStyle1 : UIView<RadioGroupViewDelegate>


@property (nonatomic, strong) NSArray *images;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (weak, nonatomic) IBOutlet RadioGroupView2 *rgView2;
@property (nonatomic, assign) id<ChangeValueDelegate> delegate;

@end
