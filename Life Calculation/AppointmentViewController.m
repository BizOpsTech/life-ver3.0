//
//  AppointmentViewController.m
//  CF_00
//
//  Created by BizOpsTech on 9/5/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "AppointmentViewController.h"

@interface AppointmentViewController ()<UIAlertViewDelegate>
@property(nonatomic,strong)NSDate *appointmentDatetime;
@end

@implementation AppointmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //购买按钮渐变配置
    UIColor *beginColor = [[UIColor alloc]initWithRed:230.f/255 green:189.f/255 blue:57.f/255 alpha:1.0];
    
    UIColor *endColor = [[UIColor alloc]initWithRed:195.f/255 green:145.f/255 blue:39.f/255 alpha:1.0];
    
    _UIButtonSetDatetime.bgColorArr = [NSArray arrayWithObjects:(id)beginColor.CGColor,(id)endColor.CGColor,nil];
    
    //获取系统当前时间
    NSDate *today = [NSDate date];
    [_dataPicker setMinimumDate:today];
    
}

-(void)viewWillAppear:(BOOL)animated{
    UITextField *txt = (UITextField *)[self.view viewWithTag:201];
    txt.layer.cornerRadius = 4.0;
    txt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txt.layer.borderWidth = 1.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dataPickerValueChange:(id)sender{
    
    NSDateFormatter*  dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd HH:mm a"];
    _appointmentDatetime = [_dataPicker date];
    _appointmentLabel.text = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate: _appointmentDatetime]];
    
    NSTimeInterval secondsBetweenDates= [[_dataPicker date] timeIntervalSinceDate:[NSDate date]];
    if((float)secondsBetweenDates < 0){
        _appointmentDatetime = nil;
    }
}

-(void)appointerWarning{
    UIAlertView *alertView =[[UIAlertView alloc]initWithTitle:@"警告" message:@"请选择正确的日期时间" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)setAppointmentDate:(myButton *)sender {
//    if(_appointmentDatetime != nil){
//        //[self performSegueWithIdentifier:@"GotoSelectAdvisorsPage" sender:sender];
//    }else{
//        [self appointerWarning];
//    }
    
    NSString *navTitle = self.navigationItem.title;
    //NSLog(@"===========navTitle %@",navTitle);
    NSString *contentText = nil;
    
    if ([navTitle isEqualToString:@"Our Advisors"]) {
        contentText = @"代理人会尽快与您取得联系！";
    }else if ([navTitle isEqualToString:@"Our TMR"]) {
        contentText = @"坐席会尽快与您联系！";
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"欢迎" message:contentText delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [av show];
    
    
}

#pragma mark    -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
 