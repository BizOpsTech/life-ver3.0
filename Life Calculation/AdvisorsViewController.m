//
//  AdvisorsViewController.m
//  CF_00
//
//  Created by BizOpsTech on 9/5/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "AdvisorsViewController.h"
#import "CVCell.h"
#import <QuartzCore/QuartzCore.h>
#import <Quartzcore/CALayer.h>
#import "myButton.h"

@interface AdvisorsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>
@property (nonatomic,strong)NSArray *EmployeeArr;
@property (weak, nonatomic) IBOutlet myButton *confirmButton;
@property(nonatomic)NSInteger EmployeeId;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
@end

@implementation AdvisorsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.numberOfSections
    self.EmployeeId = -1;
    [self pagedataConfig];
    [_myCollectionView setDelegate:self];
    [_myCollectionView setDataSource:self];
    
    //购买按钮渐变配置
    UIColor *beginColor = [[UIColor alloc]initWithRed:230.f/255 green:189.f/255 blue:57.f/255 alpha:1.0];
    UIColor *endColor = [[UIColor alloc]initWithRed:195.f/255 green:145.f/255 blue:39.f/255 alpha:1.0];
    
    self.confirmButton.bgColorArr = [NSArray arrayWithObjects:(id)beginColor.CGColor,(id)endColor.CGColor,nil];
}
-(void)viewWillAppear:(BOOL)animated{
    _phoneNumberTF = (UITextField *)[self.view viewWithTag:201];
    _phoneNumberTF.layer.cornerRadius = 4.0;
    _phoneNumberTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _phoneNumberTF.layer.borderWidth = 1.0;
    _phoneNumberTF.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)confirmAction:(id)sender {
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil message:@"The agent you appointed will contact you shortly!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [av show];
    
}


-(void)pagedataConfig{
    NSArray *nameArr = [[NSArray alloc]initWithObjects:@"王子鑫",@"薛明",@"刘莹",@"郑海伦", nil];
    NSArray *iconArr = [[NSArray alloc]initWithObjects:@"P1",@"P2",@"P3",@"P4", nil];
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    NSUInteger nIndex = 0;
    for(nIndex = 0;nIndex < 4; nIndex++){
        Employees *employee = [[Employees alloc]init];
        employee.Icon = [UIImage imageNamed:[iconArr objectAtIndex:nIndex]];
        employee.Name = [nameArr objectAtIndex:nIndex];
        [array addObject:employee];
    }
    _EmployeeArr = array;
}

//显示个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.EmployeeArr count];
}

//填充数据
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CVCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"advisorsCell" forIndexPath:indexPath];
    if(Cell == nil){
        Cell = [[CVCell alloc]initWithFrame:CGRectMake(0, 0, 120, 120)];
    }
    NSUInteger cellIndex =[indexPath item];
    Cell.title = [[_EmployeeArr objectAtIndex:cellIndex] Name];
    Cell.icon = [[_EmployeeArr objectAtIndex:cellIndex] Icon];
    return Cell;
}


//切换选中状态
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.EmployeeId =[indexPath item];
}

#pragma mark    -UITextFiled

//文本框事件委托
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self wallpaperSlidUp:textField];
    
    //启动键盘
    return YES;
}

//视图向上推送
-(void)wallpaperSlidUp:(UITextField *)textField{
    CGRect frame = textField.superview.frame;
    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0);//键盘高度216
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
    if(offset > 0)
        self.view.frame = CGRectMake(0.0f, -offset, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
//视图还原
-(void)wallpaperSlidDown{
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

//点击文本框以外的地方 就收起键盘
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self wallpaperSlidDown];
    //[self checkInputData];
    [self.phoneNumberTF resignFirstResponder];
}


@end
