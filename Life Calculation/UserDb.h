//
//  UserDb.h
//  Life Calculation
//
//  Created by BizOpsTech on 11/14/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//
#import"Question.h"
#import <Foundation/Foundation.h>

typedef struct {
    int Kids;       //养几个小孩
    int Elders;     //赡养几个老人
    int RaiseCosts; //赡养（抚养）费用
    int attendSchoolInYears;  //在指定类型学校读几年
    int Total;
}userFamily;

typedef struct{
    int AL_Option1;
    int AL_Option2;
    int AL_Option3;
    int AL_Option4;
} userAL;


@interface UserDb : NSObject

//Page1 About you
@property(nonatomic,assign)int age;            //年龄

@property(nonatomic,strong)Question *gender;

@property(nonatomic,strong)Question *isMarried;

@property(nonatomic,assign)int kids;    //小孩个数


@property(nonatomic,strong)Question *occupation;

//Page2 for habit
@property(nonatomic,strong)Question * travel;

@property(nonatomic,strong)Question *exercise;

//@property(nonatomic,assign)BOOL isSmoke; //是否吸烟
@property(nonatomic,strong)Question *isSmoke;

@property(nonatomic,strong)Question *isIllness;

//Page3 Family
@property(nonatomic,assign)userFamily family;
@property(nonatomic,strong)Question *SchoolType;


//Page4 收入与支出  asset
@property(nonatomic,assign)int earnOfAfterTax;  //税后收入
@property(nonatomic,assign)NSUInteger earnOfAfterTaxSelectIndex;

@property(nonatomic,strong)Question *statusOfHouse;
@property(nonatomic,strong)Question *statusOfCar;

@property(nonatomic,assign)NSUInteger familyAssets;  //家庭资产
@property(nonatomic,assign)NSUInteger debtCounts;    //债务总额


//Page5 支出spending
@property(nonatomic,assign)NSUInteger LifeCost;   //生活费用
@property(nonatomic,assign)NSUInteger rental;     //房租
@property(nonatomic,assign)NSUInteger Repayment;  //还贷



//Page6 投资investment
//@property(nonatomic,assign)NSUInteger investment;        //投资
@property(nonatomic,strong)Question *investment;
@property(nonatomic,assign)int deposit; //存款
@property(nonatomic,assign)int countInvestment;  //zhong投资
@property(nonatomic,assign)NSUInteger AdditionalFunding; //额外资金


//Page7 社保insurance
@property(nonatomic,assign)NSUInteger isHaveInsurance; //是否有社保
@property(nonatomic,assign)NSUInteger depositInsurance; //储蓄型保险
@property(nonatomic,assign)NSUInteger  lifeInsurance;   //传统型寿险
@property(nonatomic,assign)NSUInteger  accidentInsurance;   //意外型寿险
@property (nonatomic,assign) NSUInteger TotalInsurance;     //增加了已购买保险总额
@property (nonatomic,assign) NSUInteger yearlyExpense;    //年开支
//===================================================================
//@property(nonatomic,assign)NSString *address;
//@property(nonatomic,assign)int valueOfHouse;//房子价值
//@property(nonatomic,assign)int costOfHouse;//住房消费
//@property(nonatomic,assign)BOOL isHaveEducationFees;//有准备教育消费

//
//
////资产 和 借贷
//@property(nonatomic,assign)userAL ALWithPersonal;
//@property(nonatomic,assign)userAL ALWithSpecialPay;
//@property(nonatomic,assign)userAL ALWithSaving;

/*
 isMarried == Yes
 => peopleAffected += 1;
 
 ALWithSpecialPay.AL_Option2 >0
 => peopleAffected +=1;
 
 peopleAffected += family.toddler;
 peopleAffected += family. elementary;
 peopleAffected += family. teenager;
 
 */
@property(nonatomic,assign)int peopleAffected;



@end
