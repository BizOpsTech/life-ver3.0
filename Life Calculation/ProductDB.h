//
//  ProductDB.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef NS_ENUM(NSUInteger,insuranceModel){
//    insuranceModel_Traditional =0,
//    insuranceModel_Accidental =1,
//    insuranceModel_Healthy =2,
//    insuranceModel_Pension =3,
//    insuranceModel_Education =4
//};



@interface ProductDB : NSObject
@property(nonatomic,copy)NSString *title;
@property(nonatomic,assign)float premium;
@property(nonatomic,assign)float coverage;
@property(nonatomic,assign)int years;
@property(nonatomic,copy)NSString *detailContent;
@property(nonatomic,assign)productOfInsurance productType;
@property(nonatomic)BOOL isRecommended;

-(NSString *)getDetailContentWithModel:(insuranceModel)model productId:(int)index;
@end
