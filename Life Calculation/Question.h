//
//  Question.h
//  Life Calculation
//
//  Created by BizOpsTech on 11/21/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject
@property(nonatomic,copy)NSString *answer;
@property(nonatomic,assign)int selectIndex;

-(id) init:(NSString *)answer selectIndex:(int)index;
@end
