//
//  ConfirmInfo.h
//  Insurance
//
//  Created by BizOpsTech on 11/8/13.
//  Copyright (c) 2013 RbBtSn0w. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myButton.h"

@interface ConfirmInfo : UIViewController
@property (strong, nonatomic) IBOutlet UIView *ItemViewA;
@property (strong, nonatomic) IBOutlet UILabel *labelClaim;
@property (strong, nonatomic) IBOutlet UIView *itemB;
@property (strong, nonatomic) IBOutlet UITextField *txtPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtDateFrom;
@property (strong, nonatomic) IBOutlet UITextField *txtDateTo;
@property (weak, nonatomic) IBOutlet UITextField *allPrice;
- (IBAction)anctionWithBuyButton:(myButton *)sender;

@end
