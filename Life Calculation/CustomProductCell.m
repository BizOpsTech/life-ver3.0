//
//  CustomProductCell.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
/*
 [_carRadioButton setImage:[UIImage imageNamed:@"UICheckBoxUn.png"] forState:UIControlStateNormal];
 UIEdgeInsets btnTxtInsets;
 btnTxtInsets.right = 10;
 btnTxtInsets.left =btnTxtInsets.top =btnTxtInsets.bottom = 0;
 _carRadioButton.imageEdgeInsets = btnTxtInsets;
 */
#import "CustomProductCell.h"
@interface CustomProductCell()<UITextFieldDelegate>

@property(nonatomic,strong)NSNumberFormatter *dataFormat;
@end

@implementation CustomProductCell
@synthesize statu = _statu;
@synthesize productType = _productType;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
            }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)drawRect:(CGRect)rect{
    [self basicSetting];
}

-(void)basicSetting{
    [self setButtonImageView];
    self.txtYears.delegate = self;
    self.txtCoverage.delegate = self;
    
    NSArray *txtArr = [NSArray arrayWithObjects:self.txtYears,self.txtCoverage, nil];
    for(NSUInteger i=0; i<txtArr.count; i++){
        UITextField *tempTxt = (UITextField *)[txtArr objectAtIndex:i];
        tempTxt.layer.cornerRadius = 4.0;
        tempTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
        tempTxt.layer.borderWidth = 1.0;
    }
}

-(void)setIsDefaultSelect:(BOOL)isDefaultSelect{
    if(isDefaultSelect){
        self.statu = YES;
        [self.btnTitle setImage:[UIImage imageNamed:@"UICheckBoxSelect"] forState:UIControlStateNormal];
    }
}

-(void)setButtonImageView{
    UIEdgeInsets btnTxtInsets;
    btnTxtInsets.top = btnTxtInsets.bottom = btnTxtInsets.left = btnTxtInsets.right =10;
    self.btnTitle.imageEdgeInsets = btnTxtInsets;
    
    UIEdgeInsets contInsets;
    contInsets.top = contInsets.bottom = contInsets.right = 0;
    contInsets.left = 20;
    self.btnTitle.titleEdgeInsets = contInsets;
}

-(void)setTitle:(NSString *)title{
    [self.btnTitle setTitle:title forState:UIControlStateNormal];
}

-(void)setPremium:(float)premium{
    _dataFormat = [[NSNumberFormatter alloc]init];
    _dataFormat.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *formatPremium =  [_dataFormat stringFromNumber:[NSNumber numberWithInt:premium]];
    self.labelPremium.text = [NSString stringWithFormat:@"￥%@",formatPremium];
}

-(void)setYears:(int)years{
    self.txtYears.text = [NSString stringWithFormat:@"%d",years];
}

-(void)setCoverage:(float)coverage{
    self.txtCoverage.text = [NSString stringWithFormat:@"%.f",coverage/10000.0f];
}

- (IBAction)actionWithTitleButton:(UIButton *)sender {
    NSString *strIcon = self.statu ? @"UICheckBoxUnSelect" : @"UICheckBoxSelect";
   [self.btnTitle setImage:[UIImage imageNamed:strIcon] forState:UIControlStateNormal];
    _statu = !self.statu;
    if(sender != nil){
        [self.delegate HandleSingleSelection:self];
    }
}

- (IBAction)actionWithDetailButton:(UIButton *)sender {
    [self.delegate showDidSelectRowDetailView:self.rowIndex];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if([textField isEqual:_txtCoverage]){
        [self.delegate showEditViewForTextField:self EditIndex:0 SelectRowIndex:_rowIndex];
    }else{
        [self.delegate showEditViewForTextField:self EditIndex:1 SelectRowIndex:_rowIndex];
    }
    return NO;
}
@end