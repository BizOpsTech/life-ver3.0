//
//  CVCell.h
//  CF_00
//
//  Created by BizOpsTech on 9/5/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCell : UICollectionViewCell
@property(nonatomic,strong)UIImage *icon;
@property(nonatomic,strong)NSString *title;
@end
