//
//  RadioGroupView.m
//  Life Calculation
//
//  Created by BizOpsTech on 11/7/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "RadioGroupView.h"

@implementation RadioGroupView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        //
        nikeImage = [UIImage imageNamed:@"yes"];
        self.selectIndex = 1;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    _leftNameLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    _middleNameLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    _rightNameLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
}


-(IBAction)selectedRadioItemAction:(UIButton *)sender{
    NSUInteger selectIndex;
    if([sender isEqual:leftButton]){
        selectIndex = 1;
        [middleButton setImage:nil forState:UIControlStateNormal];
        [rightButton setImage:nil forState:UIControlStateNormal];
        
    }else if([sender isEqual:middleButton]){
        selectIndex = 2;
        [leftButton setImage:nil forState:UIControlStateNormal];
        [rightButton setImage:nil forState:UIControlStateNormal];
        
    }else{
        selectIndex = 3;
        [leftButton setImage:nil forState:UIControlStateNormal];
        [middleButton setImage:nil forState:UIControlStateNormal];
    }
    self.selectIndex = selectIndex;
    //当前按钮显示选中的图片
    [sender setImage:nikeImage forState:UIControlStateNormal];
    UIEdgeInsets btnInsets;
    btnInsets.top = -40;
    btnInsets.left = 18;
    btnInsets.bottom =  btnInsets.right = 0;
    sender.contentEdgeInsets = btnInsets;
    
    if([self.delegate respondsToSelector:@selector(selectedButtonStateWithRadio:)]){
        [self.delegate selectedButtonStateWithRadio:selectIndex];
    }
}


- (void)selectedButtonAtIndex:(NSUInteger)index{
 
    UIButton * tapButton = nil;
    
    switch (index) {
        case 1:
            tapButton = leftButton;
            break;
        case 2:
            tapButton = middleButton;
            break;
        case 3:
            tapButton = rightButton;
            break;
        default:
            break;
    }
    
    [self selectedRadioItemAction:tapButton];
}

@end
