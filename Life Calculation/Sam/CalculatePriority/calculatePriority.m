//
//  calculatePriority.m
//  Lifecalculator
//
//  Created by bizopstech on 13-11-14.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "calculatePriority.h"

@interface calculatePriority()

@property (weak,nonatomic) NSString * age;             //年龄 7个人生阶段
@property (weak,nonatomic) NSString * gender;
@property (weak,nonatomic) NSString * marriageState;
@property (weak,nonatomic) NSString * smoke;
@property (weak,nonatomic) NSString * socialInsurance; //是否有社保
@property (weak,nonatomic) NSString * incomeIndex;     //收入水平
@property (weak,nonatomic) NSString * occupation;      //职业
@property (weak,nonatomic) NSString * familyIllnessHistory; //家族病史
@property (weak,nonatomic) NSString * childInfo;            //未成年子女年龄
@property (weak,nonatomic) NSString * investmentPerference; //投资偏好
@property (weak,nonatomic) NSString * travelTimes;          //出差旅行频率
@property (weak,nonatomic) NSString * peopleAffected;       //受影响人数

@property (strong,nonatomic) NSDictionary * plistData;
@end


@implementation calculatePriority
@synthesize userDB = _userDB;

-(id)init{
    self = [super init];
    if (self) {
        NSString * mainBundlepath = [[NSBundle mainBundle] bundlePath];
        NSString * finalPath = [mainBundlepath stringByAppendingPathComponent:@"PropertyList.plist"];
        _plistData = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    }
    return self;
}


//-(void)setUserDB:(UserDb *)userDB{
//    self.userDB = userDB;
//    [self transform];
//
//}
//-(UserDb *)UserDB{
//    self.userDB = _userDB;
//    return self.userDB;
//}



-(void)transform{
    
  //7个人生阶段
  if(_userDB.age < 25)
   {
       self.age = kOld_18_25;
   }
   else if (_userDB.age >= 25 && _userDB.age < 30)
   {
       self.age = kOld_25_30;
   }
   else if (_userDB.age >= 30 && _userDB.age < 35)
   {
       self.age = kOld_30_35;
   }
   else if (_userDB.age >= 35 && _userDB.age < 45)
   {
       self.age = kOld_35_45;
   }
   else if (_userDB.age >= 45 && _userDB.age < 50)
   {
       self.age = kOld_45_50;
   }
   else if (_userDB.age >= 50 && _userDB.age < 60)
   {    self.age = kOld_50_60;
   }
   else{
       self.age = kOld_above_60;
   }
    
   //性别
   if(_userDB.gender.selectIndex == 2)
   {
       self.gender = kMale;
   }
   else{
       self.gender = kFemale;
   }
   
//    //婚姻状态
//    if(_userDB.isMarried == YES)
//    {
//        self.marriageState = kYes;
//    }else{
//    
//        self.marriageState = kNo;
//    }
    
    //抽烟
    if(_userDB.isSmoke.selectIndex == 1)
    {
        self.smoke = kNo;
    }
    else{
        self.smoke = kYes;
    }
    
   //是否有社保
    if(_userDB.isHaveInsurance > 0)
    {
        self.socialInsurance = kYes;
    }else
    {
        self.socialInsurance = kNo;
    }
    
   
    //收入平水 (income Index)
    if(_userDB.earnOfAfterTax < 30000)
    {
        self.incomeIndex = kLess30K_Year;
    }
    else if (_userDB.earnOfAfterTax >= 30000 && _userDB.earnOfAfterTax < 100000)
    {
        self.incomeIndex = k30k_100k_Year;
    }
    else if (_userDB.earnOfAfterTax >= 100000 && _userDB.earnOfAfterTax < 500000)
    {
        self.incomeIndex = k100k_500k_Year;
    }
    else if (_userDB.earnOfAfterTax >= 500000 && _userDB.earnOfAfterTax < 5000000)
    {
        self.incomeIndex = k500k_5m_Year;
    }else{
        self.incomeIndex = kAbove5m_Year;
    }
    
    //职业
    switch (_userDB.occupation.selectIndex) {
        case 1:
            self.occupation = kSOE_and_government;
            break;
        case 2:
            self.occupation = kCompany_emplyee;
            break;
        case 3:
            self.occupation = kHigh_level_manger;
            break;
        case 4:
            self.occupation = kLiberal_professions;
            break;
        case 5:
            self.occupation = kSME_owner;
            break;
        case 6:
            self.occupation = kSales;
            break;
        default:
            self.occupation = kSOE_and_government;
            break;
    }

    
    //家庭病史
    if(_userDB.isIllness.selectIndex == 1)
    {
        self.familyIllnessHistory = kNo;
    }else{
        self.familyIllnessHistory = kYes;
        
    }
    
    //未成年子女年龄(Child info)
    if(_userDB.family.Kids > 0)
    {
        //多少年之后上大学
        if(_userDB.family.attendSchoolInYears < 5)
        {
            self.childInfo = kChildInfo_Less5Year;
        }else if (_userDB.family.attendSchoolInYears >= 5 && _userDB.family.attendSchoolInYears < 10)
        {
            self.childInfo = kChildInfo_5_10Year;
        }
        else
        {
            self.childInfo = kChildInfo_Above10Year;
        }
    }

    
    //投资偏好 (investment perference)
    switch (_userDB.investment.selectIndex) {
        case 1:
            self.investmentPerference = kA1;
            break;
        case 2:
            self.investmentPerference = kA2;
            break;
        case 3:
            self.investmentPerference = kA3;
            break;
        case 4:
             self.investmentPerference = kA4;
            break;
        default:
            break;
    }
    
    //出差旅行频率
    switch (_userDB.travel.selectIndex){
        case 1:
            self.travelTimes = kVery_often;
            break;
        case 2:
            self.travelTimes = kNormal;
            break;
        case 3:
             self.travelTimes = kNot_very_often;
            break;
    
    }
    
    //people affected 受影响人数
    //孩子个数＋老人个数＋配偶
    //已婚 ＝ _userDB.isMarried.selectIndex ＝ 1
    int spouse = 0;
    if(_userDB.isMarried.selectIndex  == 1){ spouse = 1; }
    int  poples = _userDB.family.Kids + _userDB.family.Elders + spouse;
   
    switch (poples) {
        case 0:
             self.peopleAffected = kEqual_1;
            break;
        case 1:
            self.peopleAffected = kEqual_1;
            break;
        case 2:
            self.peopleAffected = kEqual_2;
            break;
        case 3:
            self.peopleAffected = kEqual_3;
            break;
        default:
            self.peopleAffected = kAbove_3;
            break;
    }
}


-(float)calculateResult:(NSString *)InsuranceType{

    float resultValue = 1;
    
    //7个人生阶段
    NSDictionary * Seven_Stage = [_plistData objectForKey:k7_Stage_priority_level];
    NSDictionary * insurances = [Seven_Stage objectForKey:self.age];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //性别
    NSDictionary * Gender_Stage = [_plistData objectForKey:kGender];
    insurances = [Gender_Stage objectForKey:self.gender];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //婚姻状态 不参与计算
    //NSDictionary * Marriage = [_plistData objectForKey:kMarriage];
    //insurances = [Marriage objectForKey:self.marriageState];
    //if(insurances != nil){
    //    resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    //}
    
    //吸烟与否
    NSDictionary * IsSmoke = [_plistData objectForKey:kSmoke];
    insurances = [IsSmoke objectForKey:self.smoke];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //是否有社保
    NSDictionary * SocialInsurance = [_plistData objectForKey:kSocial_insurance];
    insurances = [SocialInsurance objectForKey:self.socialInsurance];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //收入水平
    NSDictionary * incomeIndex = [_plistData objectForKey:kIncome_index];
    insurances = [incomeIndex objectForKey:self.incomeIndex];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //职业
    NSDictionary * occupation = [_plistData objectForKey:kOccupation];
    insurances = [occupation objectForKey:self.occupation];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //家族病史
    NSDictionary * familyIllnessHistory = [_plistData objectForKey:kFamily_illness_history];
    insurances = [familyIllnessHistory objectForKey:self.familyIllnessHistory];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //未成年子女年龄
    NSDictionary * childInfo = [_plistData objectForKey:kChild_info];
    insurances = [childInfo objectForKey:self.childInfo];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //投资偏好
    NSDictionary * investmentPerference = [_plistData objectForKey:kInvestment_perference];
    insurances = [investmentPerference objectForKey:self.investmentPerference];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //出差旅行频率
    NSDictionary * travelTimes = [_plistData objectForKey:kTravel_times];
    insurances = [travelTimes objectForKey:self.travelTimes];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    
    //受影影人数 people affected
    NSDictionary * peopleAffected = [_plistData objectForKey:kPeople_affected];
    insurances = [peopleAffected objectForKey:self.peopleAffected];
    if(insurances != nil){
        resultValue = resultValue * [[insurances objectForKey:InsuranceType] floatValue];
    }
    return resultValue;
}




-(float) getTraditionalLifePriority{
    //最后需要乘以调整系数
    return [self calculateResult:kTraditional_life] * 0.61f;
}

-(float)getAccidentPriority{
    //最后需要乘以调整系数
    return [self calculateResult:kAccident] * 0.33f;
}

-(float)getHealthInsurancePriority{
    //最后需要乘以调整系数
    return [self calculateResult:kHealth_insurance] * 0.54f;
}

-(float)getRetirementPriority{
    //最后需要乘以调整系数
    return [self calculateResult:kRetirement] * 0.67f ;
}

-(float)getChildrenEducationPriority{
    //最后需要乘以调整系数
    float value = [self calculateResult:kChildren_education] * 1.0f;
    if(_userDB.kids == 0 ){
        value = 1;
    }
    return value;
}






@end
