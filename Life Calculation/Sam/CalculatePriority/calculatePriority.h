//
//  calculatePriority.h
//  Lifecalculator
//
//  Created by bizopstech on 13-11-14.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDb.h"


@interface calculatePriority : NSObject


@property (strong,nonatomic) UserDb * userDB;
-(void)transform;

-(float)getTraditionalLifePriority;

-(float)getAccidentPriority;

-(float)getHealthInsurancePriority;

-(float)getRetirementPriority;

-(float)getChildrenEducationPriority;


@end
