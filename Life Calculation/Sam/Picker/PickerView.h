//
//  UICityPicker.h
//  DDMates
//
//  Created by ShawnMa on 12/16/11.
//  Copyright (c) 2011 TelenavSoftware, Inc. All rights reserved.
//弹出选择界面
//- (IBAction)popSelect:(UIButton *)sender {
//    if(self.pickerView == nil){
//        NSArray * arr = [NSArray arrayWithObjects:@"5K",@"10K",@"15K",@"20K",@"25K", nil];
//        self.pickerView = [[PickerView alloc] initWithTitle:@"" delegate:self dataSource:arr];
//    }
//    if(self.IsOpenLocateView == NO)
//    {
//        self.IsOpenLocateView = YES;
//        [self.pickerView showInView:self.view];
//    }
//}
//取值
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    self.IsOpenLocateView = NO;
//    if(buttonIndex == 1){
//        PickerView * pickView = (PickerView *)actionSheet;
//        NSString * str = pickView.selectedValue;
//        NSLog(@"%@",str);
//    }
//}





#import <QuartzCore/QuartzCore.h>

@interface PickerView : UIActionSheet<UIPickerViewDelegate, UIPickerViewDataSource> {
}

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIPickerView *locatePicker;
@property (strong,nonatomic)  NSArray * arrData;
@property (strong,nonatomic) NSString * selectedValue;

- (id)initWithTitle:(NSString *)title delegate:(id /*<UIActionSheetDelegate>*/)delegate dataSource:(NSArray *)arr;
- (void)showInView:(UIView *)view;
-(void)ChageData:(NSArray *)newArr;
-(void)ChageTitle:(NSString *)title;
@end
