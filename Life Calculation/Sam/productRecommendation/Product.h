//
//  Product.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-23.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

-(id) init;

-(NSDictionary  *) getProductDetail:(insuranceModel)InsuranceType productID:(productOfInsurance)productID;

@end
