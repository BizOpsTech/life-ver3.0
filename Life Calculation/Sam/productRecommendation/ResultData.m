//
//  ResultData.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-23.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "ResultData.h"
#import "UserDb.h"

@interface ResultData()
//@property (assign, nonatomic) int buyingPurpose;
//@property (assign,nonatomic) int age;
//@property (assign,nonatomic) int CareerType;
@end



@implementation ResultData

@synthesize LifeObj = _LifeObj;
@synthesize EducationObj = _EducationObj;
@synthesize HealthObj = _HealthObj;
@synthesize AccidentObj = _AccidentObj;
@synthesize PensionObj = _PensionObj;


- (id)init:(int)buyingPurpose
       age:(int)userage
careerType:(int)careerType

LifeCoverage:(float)life_Coverage
LifePriority:(float)Life_Priority

EducationCoverage:(float)education_Coverage
EducationPriority:(float)education_Priority

HealthCoverage:(float)health_Coverage
HealthPriority:(float)health_Priority

AccidentCoverage:(float)accident_Coverage
AccidentPriority:(float)accident_Priority

PensionCoverage:(float)pension_Coverage
PensionPriority:(float)pension_Priority
YearlyIncome:(float)yearlyIncome
    userDB:(UserDb *)userdb
{
    self = [super init];
    if (self) {


        
        _LifeObj = [[Life alloc] init:buyingPurpose age:userage CareerType:careerType YearlyIncome:yearlyIncome];
        
        userFamily uf = userdb.family;
        _EducationObj = [[Education alloc] init:buyingPurpose age:userage CareerType:careerType YearlyIncome:yearlyIncome attendSchoolInYears:uf.attendSchoolInYears];
        _HealthObj = [[Health alloc] init:buyingPurpose age:userage CareerType:careerType YearlyIncome:yearlyIncome];
        _AccidentObj = [[Accident alloc] init:buyingPurpose age:userage CareerType:careerType YearlyIncome:yearlyIncome];
        _PensionObj = [[Pension alloc] init:buyingPurpose age:userage CareerType:careerType YearlyIncome:yearlyIncome];
        
        _LifeObj.unadjusted_Coverage = life_Coverage;
        _EducationObj.unadjusted_Coverage = education_Coverage;
        _HealthObj.unadjusted_Coverage = health_Coverage;
        _AccidentObj.unadjusted_Coverage = accident_Coverage;
        _PensionObj.unadjusted_Coverage = pension_Coverage;
 
        
        if(life_Coverage > 200 * 10000)
        { life_Coverage = 200 * 10000; }
        
        _LifeObj.Coverage = life_Coverage;
        _LifeObj.Priority = Life_Priority;
        
        if(education_Coverage > 20 * 10000)
        { education_Coverage = 20 * 10000; }
        _EducationObj.Coverage = education_Coverage;
        _EducationObj.Priority = education_Priority;
        
        if(health_Coverage > 100 * 10000)
        { health_Coverage = 100 * 10000; }
        _HealthObj.Coverage = health_Coverage;
        _HealthObj.Priority =  health_Priority;
        
        
        if(accident_Coverage > 200 * 10000)
        { accident_Coverage = 200 * 10000; }
        _AccidentObj.Coverage = accident_Coverage;
        _AccidentObj.Priority = accident_Priority;
        
        if(pension_Coverage > 100 * 10000)
        { pension_Coverage = 100 * 10000; }
        _PensionObj.Coverage =pension_Coverage;
        _PensionObj.Priority = pension_Priority;

        
        
        
        [_LifeObj getSelectStatus];
        [_LifeObj CalculationTerm];
        [_LifeObj getProduct];
        
        [_EducationObj getSelectStatus];
        [_EducationObj CalculationTerm];
        [_EducationObj getProduct];
        
        [_HealthObj getSelectStatus];
        [_HealthObj CalculationTerm];
        [_HealthObj getProduct];
        
        [_AccidentObj getSelectStatus];
        [_AccidentObj CalculationTerm];
        [_AccidentObj getProduct];

        [_PensionObj getSelectStatus];
        [_PensionObj CalculationTerm];
        [_PensionObj getProduct];
        
    }
    return self;
}



-(void)ReCalculation:(float)yearlyIncome{

    //重新计算
    [ReCalculationCoverage ReCalculation:_LifeObj Education:_EducationObj Health:_HealthObj Accident:_AccidentObj Pension:_PensionObj YearlyIncome:yearlyIncome];
}



-(NSMutableArray *)Sort{
    NSMutableArray * arr =[[NSMutableArray alloc] initWithCapacity:5];
    
    [arr addObject:_LifeObj];
    [arr addObject:_EducationObj];
    [arr addObject:_HealthObj];
    [arr addObject:_AccidentObj];
    [arr addObject:_PensionObj];
    
    
    //@“Priority”是对象属性
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc] initWithKey:@"Priority" ascending:NO];
    [arr sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];//返回排序好的数组
    
    return arr;
}



@end
