//
//  ResultBase.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultBase : NSObject

//输出的属性
@property (assign,nonatomic) BOOL IsSelect;  //是否选中

@property (assign,nonatomic) float Permier;
@property (assign,nonatomic) int Term;

@property (assign,nonatomic) insuranceModel InsuranceType;
@property (assign,nonatomic) productOfInsurance productID;
@property (strong,nonatomic) NSString * name;



//输入的属性
@property (assign,nonatomic) int buyingIntention;
@property (assign,nonatomic) int age;
@property (assign,nonatomic) int CareerType; //职业
@property (assign,nonatomic) float yearlyIncome; // 年收入

//输入输出
@property (assign,nonatomic) float Coverage;
@property (assign,nonatomic) float Priority; //优先级

@property (assign,nonatomic) float unadjusted_Coverage;

@property (assign,nonatomic) BOOL IshavePreciseUnadjusted_Coverage;

@end
