//
//  Education.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "Education.h"
#import "CalculationInsurance.h"


@interface Education()

@property (nonatomic,assign) NSInteger attendSchoolInYears;


@end

@implementation Education


-(id) init:(int)buyingIntention
       age:(int)userAge
CareerType:(int)careerType
YearlyIncome:(float)yearlyIncome
attendSchoolInYears:(int)attendSchoolyear
{
    self = [super init];
    if(self)
    {
        self.buyingIntention = buyingIntention;
        self.age = userAge;
        self.CareerType = careerType;
        self.InsuranceType = insuranceModel_Education;
        self.yearlyIncome = yearlyIncome;
        self.attendSchoolInYears = attendSchoolyear;
    }
    return self;
}

-(BOOL)getSelectStatus{

    if(self.Coverage <= 0){
        self.IsSelect = NO;
    }
    else
    {
        if(self.buyingIntention == 2 || self.buyingIntention == 3)
        {
            if(self.Priority > 3.5 )
            {
                self.IsSelect = YES;
            }
            else
            {
                self.IsSelect = NO;
            }
        }
        else{
            self.IsSelect = NO;
        }
    }
    return self.IsSelect;
}


-(int)CalculationTerm{
    if(self.attendSchoolInYears <= 5){
        self.Term = 1;
    }
    else if (self.attendSchoolInYears > 5 && self.attendSchoolInYears <= 10)
    {
        self.Term = 5;
    }
    else{
        self.Term = 10;
    }
    return self.Term;
}


-(void)getProduct{
    if( (int)self.Coverage % 20000 != 0)
    {
        self.Coverage = ceilf(self.Coverage / 20000) * 20000;
    }
    [self CalculateCoverage];
}

-(void)CalculateCoverage{
    float premium = 0;
    if(self.IsSelect == YES)
    {
        if(self.buyingIntention == 2)
        {
            if(self.yearlyIncome > 1000000)
            {
                premium = [CalculationInsurance returnPremium:insuranceModel_Education SpecificInsurance:product_educationPlusB Coverage:self.Coverage Year:self.Term];
                self.productID = product_educationPlusB;
            }
            else{
                premium = [CalculationInsurance returnPremium:insuranceModel_Education SpecificInsurance:product_educationPlusA Coverage:self.Coverage Year:self.Term];
                self.productID = product_educationPlusA;
            }
        }
        else if(self.buyingIntention == 3){
        
            if(self.yearlyIncome > 1000000)
            {
                premium = [CalculationInsurance returnPremium:insuranceModel_Education SpecificInsurance:product_educationPlusB Coverage:self.Coverage Year:self.Term];
                self.productID = product_educationPlusB;
            }
            else{
                premium = [CalculationInsurance returnPremium:insuranceModel_Education SpecificInsurance:product_educationPlusA Coverage:self.Coverage Year:self.Term];
                self.productID = product_educationPlusA;
            }
        }
        else{
            premium = [CalculationInsurance returnPremium:insuranceModel_Education SpecificInsurance:product_educationPlusA Coverage:self.Coverage Year:self.Term];
            self.productID = product_educationPlusA;
        }
        
    }else{
        premium = [CalculationInsurance returnPremium:insuranceModel_Education SpecificInsurance:product_educationPlusA Coverage:self.Coverage Year:self.Term];
        self.productID = product_educationPlusA;
        
    }
    self.Permier = premium;
}



@end
