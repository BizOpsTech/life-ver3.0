//
//  ReCalculationCoverage.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "Life.h"
#import "Education.h"
#import "Health.h"
#import "Accident.h"
#import "Pension.h"



@interface ReCalculationCoverage : NSObject


+(void)ReCalculation:(Life *)lifeObj
           Education:(Education *)EducationObj
              Health:(Health *)HealthObj
            Accident:(Accident *)AccidentObj
             Pension:(Pension *) Pension
        YearlyIncome:(float)yearlyIncome;


@end
