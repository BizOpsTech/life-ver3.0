//
//  ReCalculationCoverage.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "ReCalculationCoverage.h"

@implementation ReCalculationCoverage


+(void)ReCalculation:(Life *)lifeObj
           Education:(Education *)EducationObj
              Health:(Health *)HealthObj
            Accident:(Accident *)AccidentObj
             Pension:(Pension *) Pension
        YearlyIncome:(float)yearlyIncome //年收入
{

    
    BOOL adjustCoverage = YES; //yes 为需要调整
    while (adjustCoverage){
        
        float Totalpermier = 0;
        if(lifeObj.IsSelect == YES)
        {
            Totalpermier +=  lifeObj.Permier;
        }
        
        if(EducationObj.IsSelect == YES)
        {
            Totalpermier +=  EducationObj.Permier;
        }
        
        if(HealthObj.IsSelect == YES)
        {
            Totalpermier += HealthObj.Permier;
        }
        
        if(AccidentObj.IsSelect == YES)
        {
            Totalpermier += AccidentObj.Permier;
        }
        
        if(Pension.IsSelect == YES)
        {
            Totalpermier += Pension.Permier;
        }
        
        if(Totalpermier >  yearlyIncome * 0.1)
        {
            if(lifeObj.IsSelect == YES)
            {
                lifeObj.Coverage = lifeObj.Coverage -100000 ;
                
                if(lifeObj.Coverage <= 0){
                    lifeObj.Coverage = 100000;
                    lifeObj.IsSelect = NO;
                }
                //重新计算保费
                [lifeObj CalculateCoverage];
            }
            if(EducationObj.IsSelect == YES)
            {
                EducationObj.Coverage = EducationObj.Coverage -20000;
                if(EducationObj.Coverage <= 0){
                    EducationObj.Coverage = 20000;
                    EducationObj.IsSelect = NO;
                }
                //重新计算保费
                [EducationObj CalculateCoverage];
            }
            if(HealthObj.IsSelect == YES)
            {
                HealthObj.Coverage = HealthObj.Coverage -50000;
                if(HealthObj.Coverage <= 0){
                    HealthObj.Coverage = 50000;
                    HealthObj.IsSelect = NO;
                }
                //重新计算保费
                [HealthObj CalculateCoverage];
            }
            if(AccidentObj.IsSelect == YES)
            {
                AccidentObj.Coverage = AccidentObj.Coverage - 100000;
                if(AccidentObj.Coverage <= 0){
                    AccidentObj.Coverage = 100000;
                    AccidentObj.IsSelect = NO;
                }
                //重新计算保费
                [AccidentObj CalculateCoverage];
            }
            if(Pension.IsSelect == YES)
            {
                Pension.Coverage = Pension.Coverage - 50000;
                if(Pension.Coverage <= 0){
                    Pension.Coverage = 50000;
                    Pension.IsSelect = NO;
                }
                //重新计算保费
                [Pension CalculateCoverage];
            }
        }
        else
        {
            adjustCoverage = NO;
        }
    }
}


@end
