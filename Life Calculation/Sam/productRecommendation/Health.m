//
//  Health.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "Health.h"
#import "CalculationInsurance.h"

@interface Health()

@property (assign,nonatomic) int Term;

@end

@implementation Health



-(id) init:(int)buyingIntention
       age:(int)userAge
CareerType:(int)careerType
YearlyIncome:(float)yearlyIncome
{
    self = [super init];
    if(self)
    {
        self.buyingIntention = buyingIntention;
        self.age = userAge;
        self.CareerType = careerType;
        self.InsuranceType = insuranceModel_Healthy;
        self.yearlyIncome = yearlyIncome;
    }
    return (self);
}



-(BOOL)getSelectStatus{
    
    if(self.Coverage <= 0){
        self.IsSelect = NO;
    }
    else
    {
        if(self.buyingIntention == 0 || self.buyingIntention == 1)
        {
            self.IsSelect = YES;
        }
        else if(self.buyingIntention == 2 || self.buyingIntention == 3)
        {
            if(self.Priority > 3.5 )
            {
                self.IsSelect = YES;
            }
            else
            {
                self.IsSelect = NO;
            }
        }
        else{
            self.IsSelect = NO;
        }
    }
    return self.IsSelect;
}


-(int)CalculationTerm{
    
    if(self.age <= 30)
    {
        self.Term = 20;
    }
    else if (self.age >30 && self.age <=40)
    {
        self.Term = 10;
    }
    else if (self.age >40 && self.CareerType > 3)
    {
        if(self.yearlyIncome > 200 * 10000){
            self.Term = 1;
        }
        else
        {
            self.Term = 5;
        }
    }
    else{
        self.Term = 5;
    }
    return self.Term;
}




-(void)getProduct{
    if((int)self.Coverage % 50000 != 0)
    {
        self.Coverage = ceilf(self.Coverage / 50000) * 50000;
    }
    [self CalculateCoverage];
}


-(void)CalculateCoverage{

    float premium = 0;
    if(self.IsSelect == YES)
    {
        switch (self.buyingIntention){
            case 0:
                premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_SuperHealthCare Coverage:self.Coverage Year:self.Term];
                self.productID = product_SuperHealthCare;
                break;
            case 1:
                premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_CriticalIllnessB Coverage:self.Coverage Year:self.Term];
                self.productID = product_CriticalIllnessB;
                break;
            case 2:
                if(self.yearlyIncome > 1000000)
                {
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_CriticalIllnessA Coverage:self.Coverage Year:self.Term];
                    self.productID = product_CriticalIllnessA;
                }
                else{
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_CriticalIllnessB Coverage:self.Coverage Year:self.Term];
                    self.productID = product_CriticalIllnessB;
                }
                break;
            case 3:
                if(self.yearlyIncome > 1000000)
                {
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_CriticalIllnessA Coverage:self.Coverage Year:self.Term];
                    self.productID = product_CriticalIllnessA;
                }
                else{
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_RegalCriticalIllness Coverage:self.Coverage Year:self.Term];
                    self.productID = product_RegalCriticalIllness;
                }
                break;
        }
    }else{
        premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_CriticalIllnessB Coverage:self.Coverage Year:self.Term];
        self.productID = product_CriticalIllnessB;
        
    }
    self.Permier = premium;
}





@end
