//
//  Pension.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "Pension.h"
#import "CalculationInsurance.h"

@interface  Pension()
@end



@implementation Pension


-(id) init:(int)buyingIntention
       age:(int)userAge
CareerType:(int)careerType
YearlyIncome:(float)yearlyIncome
{
    self = [super init];
    if(self)
    {
        self.buyingIntention = buyingIntention;
        self.age = userAge;
        self.CareerType = careerType;
        self.InsuranceType = insuranceModel_Pension;
        self.yearlyIncome = yearlyIncome;
    }
    return (self);
}


-(BOOL)getSelectStatus{
    
    if(self.Coverage <= 0){
        self.IsSelect = NO;
    }
    else
    {
        if(self.buyingIntention == 2 || self.buyingIntention == 3)
        {
            if(self.Priority > 3.5 )
            {
                self.IsSelect = YES;
            }
            else
            {
                self.IsSelect = NO;
            }
        }
        else{
            self.IsSelect = NO;
        }
    }
    return self.IsSelect;
}


-(int)CalculationTerm{
    
    if(self.age <= 40)
    {
        self.Term = 20;
    }
    else if (self.age >40 && self.age <=50)
    {
        self.Term = 10;
    }
    else{
        self.Term = 5;
    }
    return self.Term;
}


-(void)getProduct{
    if( (int)self.Coverage % 50000 != 0)
    {
        self.Coverage = ceilf(self.Coverage /50000) * 50000;
    }
    [self CalculateCoverage];
}

-(void)CalculateCoverage{
    float premium = 0;
    if(self.IsSelect == YES)
    {
        if(self.buyingIntention == 2)
        {
            if(self.yearlyIncome > 1000000)
            {
                premium = [CalculationInsurance returnPremium:insuranceModel_Pension SpecificInsurance:product_RetirementEndowment Coverage:self.Coverage Year:self.Term];
                self.productID = product_RetirementEndowment;
            }else{
                premium = [CalculationInsurance returnPremium:insuranceModel_Pension SpecificInsurance:product_RetirementBaseA Coverage:self.Coverage Year:self.Term];
                self.productID = product_RetirementBaseA;
            }
        }
        else if(self.buyingIntention == 3){
            
            if(self.yearlyIncome > 1000000)
            {
                premium = [CalculationInsurance returnPremium:insuranceModel_Pension SpecificInsurance:product_PremierRetirementA Coverage:self.Coverage Year:self.Term];
                self.productID = product_PremierRetirementA;
            }else{
                    premium = [CalculationInsurance returnPremium:insuranceModel_Pension SpecificInsurance:product_RetirementBaseA Coverage:self.Coverage Year:self.Term];
                    self.productID = product_RetirementBaseA;
             }
        }else{
            premium = [CalculationInsurance returnPremium:insuranceModel_Pension SpecificInsurance:product_RetirementBaseA Coverage:self.Coverage Year:self.Term];
            self.productID = product_RetirementBaseA;
        }
        
    }else{
        premium = [CalculationInsurance returnPremium:insuranceModel_Pension SpecificInsurance:product_RetirementBaseA Coverage:self.Coverage Year:self.Term];
        self.productID = product_RetirementBaseA;
        
        
    }
    self.Permier = premium;
}


@end
