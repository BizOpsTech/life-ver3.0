//
//  Pension.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "ResultBase.h"
@interface Pension: ResultBase


-(id) init:(int)buyingIntention
       age:(int)userAge
CareerType:(int)careerType
YearlyIncome:(float)yearlyIncome;

-(BOOL)getSelectStatus;

-(int)CalculationTerm;

-(void)getProduct;

-(void)CalculateCoverage;

@end
