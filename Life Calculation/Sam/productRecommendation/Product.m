 //
//  Product.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-23.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "Product.h"

@interface Product()
    @property (strong,nonatomic) NSDictionary * plistData;
    @property (strong,nonatomic) NSDictionary * arrItem;
@end



@implementation Product



-(id) init
{
    self = [super init];
    if(self)
    {
        NSString * mainBundlepath = [[NSBundle mainBundle] bundlePath];
        NSString * finalPath = [mainBundlepath stringByAppendingPathComponent:@"ProductData.plist"];
        _plistData = [NSDictionary dictionaryWithContentsOfFile:finalPath];
  
    }
    return (self);
}




-(NSDictionary  *) getProductDetail:(insuranceModel)InsuranceType productID:(productOfInsurance)productID
{
    NSArray * ProductsInCategory;
    
    switch (InsuranceType) {
        case insuranceModel_Traditional:
            ProductsInCategory = [_plistData objectForKey:@"Traditional"];
            break;
        case insuranceModel_Accidental:
            ProductsInCategory = [_plistData objectForKey:@"Accidental"];
            break;
        case insuranceModel_Healthy:
            ProductsInCategory = [_plistData objectForKey:@"Healthy"];
            break;
        case insuranceModel_Pension:
            ProductsInCategory = [_plistData objectForKey:@"Pension"];
            break;
        case insuranceModel_Education:
            ProductsInCategory = [_plistData objectForKey:@"Education"];
            break;
        default:
            ProductsInCategory = [_plistData objectForKey:@"Traditional"];
            break;
    }
    

    
    for(int i=0; i< ProductsInCategory.count; i++)
    {
        NSDictionary *tempArrItem = [ProductsInCategory objectAtIndex:i];
        int idValue = [[tempArrItem objectForKey:@"ID"] intValue];
        if(idValue == (int)productID)
        {
            self.arrItem = tempArrItem;
        }
    }
    return self.arrItem;
}








@end
