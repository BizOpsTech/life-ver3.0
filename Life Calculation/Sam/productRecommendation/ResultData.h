//
//  ResultData.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-23.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//


#import "ReCalculationCoverage.h"

@class UserDb;

@interface ResultData : NSObject


@property (strong,nonatomic) Life * LifeObj;
@property (strong,nonatomic) Education * EducationObj;
@property (strong,nonatomic) Health * HealthObj;
@property (strong,nonatomic) Accident * AccidentObj;
@property (strong,nonatomic) Pension * PensionObj;




- (id)init:(int)buyingPurpose
       age:(int)userage
careerType:(int)careerType

LifeCoverage:(float)life_Coverage
LifePriority:(float)Life_Priority

EducationCoverage:(float)education_Coverage
EducationPriority:(float)education_Priority

HealthCoverage:(float)health_Coverage
HealthPriority:(float)health_Priority

AccidentCoverage:(float)accident_Coverage
AccidentPriority:(float)accident_Priority

PensionCoverage:(float)pension_Coverage
PensionPriority:(float)pension_Priority
YearlyIncome:(float)yearlyIncome
    userDB:(UserDb *)userdb;



-(NSMutableArray *)Sort;

-(void)ReCalculation:(float)yearlyIncome;
@end
