//
//  Life.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-22.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "Life.h"
#import "CalculationInsurance.h"

@interface Life()

@end


@implementation Life



-(id) init:(int)buyingIntention
       age:(int)userAge
CareerType:(int)careerType
YearlyIncome:(float)yearlyIncome
{
    self = [super init];
    if(self)
    {
        self.buyingIntention = buyingIntention;
        self.age = userAge;
        self.CareerType = careerType;
        self.InsuranceType = insuranceModel_Traditional;
        self.yearlyIncome = yearlyIncome;
    }
    return (self);
}

-(BOOL)getSelectStatus{

    if(self.Coverage <= 0){
        self.IsSelect = NO;
    }
    else
    {
        if(self.buyingIntention == 0)
        {
            self.IsSelect = YES;
        }
        else if(self.buyingIntention == 1)
        {
            self.IsSelect = YES;
        }
        else{
            if(self.Priority > 3.5 )
            {
                self.IsSelect = YES;
            }
            else
            {
                self.IsSelect = NO;
            }
        }
    }
    return self.IsSelect;
}


-(int)CalculationTerm{
    
    if(self.age <= 30)
    {
        self.Term = 20;
    }
    else if (self.age >30 && self.age <=40)
    {
        self.Term = 10;
    }
    else if (self.age >40 && self.CareerType > 3)
    {
        if(self.yearlyIncome > 200 * 10000){
            self.Term = 1;
        }
        else
        {
            self.Term = 5;
        }
    }
    else{
        self.Term = 5;
    }
    return self.Term;
}


-(void)getProduct{
    if( (int)self.Coverage % 100000 != 0)
    {
        self.Coverage = ceilf(self.Coverage /100000) * 100000;
    }
   [self CalculateCoverage];
}


-(void)CalculateCoverage{

    float premium = 0;
    if(self.IsSelect == YES)
    {
        
        switch (self.buyingIntention) {
            case 0:
                if(self.age <= 40)
                {
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_TermElite Coverage:self.Coverage Year:self.Term];
                    self.productID = product_TermElite;
                }else{
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_TermEssential Coverage:self.Coverage Year:self.Term];
                    self.productID = product_TermEssential;
                }
                break;
            case 1:
                premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_TermEssential Coverage:self.Coverage Year:self.Term];
                self.productID = product_TermEssential;
                break;
            case 2:
                if(self.yearlyIncome > 1000000)
                {
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_EndowmentPremier Coverage:self.Coverage Year:self.Term];
                    self.productID = product_EndowmentPremier;
                }
                else{
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_TermEssential Coverage:self.Coverage Year:self.Term];
                    self.productID = product_TermEssential;
                }
                break;
            case 3:
                if(self.yearlyIncome > 1000000)
                {
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_UniversalProtector Coverage:self.Coverage Year:self.Term];
                    self.productID = product_UniversalProtector;
                }
                else{
                    premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_TermEssential Coverage:self.Coverage Year:self.Term];
                    self.productID = product_TermEssential;
                }
                break;
        }
    }else{
        premium = [CalculationInsurance returnPremium:self.InsuranceType SpecificInsurance:product_TermEssential Coverage:self.Coverage Year:self.Term];
        self.productID = product_TermEssential;
        
    }
    self.Permier = premium;
}





@end
