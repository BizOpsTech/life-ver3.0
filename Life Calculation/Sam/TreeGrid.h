//
//  TreeGrid.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-24.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TreeGrid : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *dataGrid;
@property(nonatomic,copy)NSString *Caption;
@property (assign,nonatomic) float LifeCoverage;
@property (assign,nonatomic) float AccidentCoverage;
@property (assign,nonatomic) float EducationCoverage;
@property (assign,nonatomic) float HealthCoverage;
@property (assign,nonatomic) float PenSionCoverage;

@property (strong, nonatomic) IBOutlet UIButton *BtnAccident;
@property (strong, nonatomic) IBOutlet UIButton *BtnEducation;
@property (strong, nonatomic) IBOutlet UIButton *BtnLife;
@property (strong, nonatomic) IBOutlet UIButton *BtnHealth;
@property (strong, nonatomic) IBOutlet UIButton *BtnPension;

@property (strong, nonatomic) IBOutlet UILabel *InsuranceTitle;
@property (weak,nonatomic) UINavigationController * parentNC;

- (IBAction)transformEducation:(id)sender;
- (IBAction)transformAccident:(UIButton *)sender;
- (IBAction)transformLife:(id)sender;
- (IBAction)transformHealth:(UIButton *)sender;
- (IBAction)transformPension:(UIButton *)sender;

@property(nonatomic)insuranceModel iModel;
@end