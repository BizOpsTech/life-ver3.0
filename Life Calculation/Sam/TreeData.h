//
//  TreeData.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-25.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserDb;

@interface TreeData : NSObject

-(id) init:(UserDb *)UserDB;

@property (strong,nonatomic) NSArray * LifeTreeValues;

@property(strong,nonatomic) NSArray * AccidentTreeValues;

@property (strong,nonatomic) NSArray * HealthTreeValues;

@property (strong,nonatomic) NSArray * PensionTreeValues;

@property (strong,nonatomic) NSArray * EducationTreeValues;



@end
