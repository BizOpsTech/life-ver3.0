//
//  NowYouCan.h
//  TwoPage
//
//  Created by bizopstech on 13-10-31.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NowYouCan : UITableViewController
@property (strong, nonatomic) IBOutlet UILabel *TotalPremium;
@property (strong, nonatomic) IBOutlet UILabel *TotalCoverage;
@property (strong, nonatomic) IBOutlet UILabel *labelOfPermium;
@property (strong, nonatomic) IBOutlet UILabel *labelOfCoverage;
@property (strong, nonatomic) IBOutlet UIView *groupView;
@property (nonatomic, copy) NSString *coverageOfAll;
@property (nonatomic, copy) NSString *permiumOfAll;

@end
