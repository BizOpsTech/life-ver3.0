//
//  PensionInsurance.m
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "PensionInsurance.h"

@implementation PensionInsurance




-(float)Calculation:(productOfInsurance)specificType{
    
    float premium = 0;
    if(specificType == product_RetirementBaseA)
    {
        switch (self.year) {
            case 1:
                premium = 500 * self.coverage / 1000;
                break;
            case 5:
                premium = 128.7 * self.coverage / 1000;
                break;
            case 10:
                premium = 67.8 * self.coverage / 1000;
                break;
            case 20:
                premium = 37.3 * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_PremierRetirementA)
    {
        switch (self.year){
            case 1:
                premium = 200 * self.coverage / 401;
                break;
            case 5:
                premium = 200 * self.coverage / 1899;
                break;
            case 10:
                premium = 200 * self.coverage / 3423;
                break;
            case 20:
                premium = 200 * self.coverage / 5362;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_RetirementEndowment)
    {
        switch (self.year) {
            case 1:
                premium = 543.2f * self.coverage / 1000;
                break;
            case 5:
                premium = 156 * self.coverage / 1000;
                break;
            case 10:
                premium = 93 * self.coverage / 1000;
                break;
            case 20:
                premium = 62.3f * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_PremierRetirementB)
    {
        switch (self.year) {
            case 1:
                premium = 87 * self.coverage / 164;
                break;
            case 5:
                premium = 23 * self.coverage / 660;
                break;
            case 10:
                premium = 16 * self.coverage / 1209;
                break;
            case 20:
                premium = 12 * self.coverage / 2098;
                break;
            default:
                break;
        }
    }
    
    return  premium;
}





@end
