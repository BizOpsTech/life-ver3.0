//
//  HealthyInsurance.m
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "HealthyInsurance.h"

@implementation HealthyInsurance


-(float)Calculation:(productOfInsurance)specificType{
    
    float premium = 0;
    if(specificType == product_SuperHealthCare)
    {
        switch (self.year) {
            case 1:
                premium = 28.6f * self.coverage / 1000;
                break;
            case 5:
                premium = 17.3f * self.coverage / 1000;
                break;
            case 10:
                premium = 9.5f * self.coverage / 1000;
                break;
            case 20:
                premium = 6.3f * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_CriticalIllnessA)
    {
        switch (self.year){
            case 1:
                premium = 100 * self.coverage / 2700;
                break;
            case 5:
                premium = 100 * self.coverage / 4500;
                break;
            case 10:
                premium = 100 * self.coverage / 8100;
                break;
            case 20:
                premium = 100 * self.coverage / 13000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_CriticalIllnessB)
    {
        switch (self.year) {
            case 1:
                premium = 45 * self.coverage / 1000;
                break;
            case 5:
                premium = 12 * self.coverage / 1000;
                break;
            case 10:
                premium = 7.0 * self.coverage / 1000;
                break;
            case 20:
                premium = 4.8 * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_RegalCriticalIllness)
    {
        switch (self.year) {
            case 1:
                premium = 397.2 * self.coverage / 1000;
                break;
            case 5:
                premium = 89.4 * self.coverage / 1000;
                break;
            case 10:
                premium = 44.7 * self.coverage / 1000;
                break;
            case 20:
                premium = 25.5f * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    
    return  premium;
}




@end
