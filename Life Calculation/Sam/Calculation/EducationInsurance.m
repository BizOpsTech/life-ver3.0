//
//  EducationInsurance.m
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "EducationInsurance.h"

@implementation EducationInsurance

-(float)Calculation:(productOfInsurance)specificType{
    
    float premium = 0;
    if(specificType == product_educationPlusA)
    {
        switch (self.year) {
            case 1:
                premium = 1000 * self.coverage / 1236;
                break;
            case 3:
                premium = 1000 * self.coverage / 3610;
                break;
            case 5:
                premium = 1000 * self.coverage / 7009;
                break;
            case 10:
                premium = 1000 * self.coverage / 13209;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_educationPlusB)
    {
        switch (self.year){
            case 1:
                premium = 1000 * self.coverage / 1400;
                break;
            case 3:
                premium = 1000 * self.coverage / 3600;
                break;
            case 5:
                premium = 1000 * self.coverage / 7800;
                break;
            case 10:
                premium = 1000 * self.coverage / 10000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_educationPremier)
    {
        switch (self.year) {
            case 1:
                premium = 1000 * self.coverage / 1250;
                break;
            case 3:
                premium = 1000 * self.coverage / 2500;
                break;
            case 5:
                premium = 1000 * self.coverage / 4300;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_educationBase)
    {
        switch (self.year) {
            case 1:
                premium = 1000 * self.coverage / 1320;
                break;
            case 3:
                premium = 1000 * self.coverage / 3010;
                break;
            case 5:
                premium = 1000 * self.coverage / 5899;
                break;
            default:
                break;
        }
    }
    
    return  premium;
}



@end
