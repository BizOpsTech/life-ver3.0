//
//  CalculationInsurance.h
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.

//调用方法
//算传统寿险
//float premium  = [CalculationInsurance returnPremium:Traditional_Life SpecificInsurance:TermLifeA Coverage:100 Year:5];
//NSLog(@"传统premium＝ %f",premium);
//算意外
//premium  = [CalculationInsurance returnPremium:Accidental_Life SpecificInsurance:AccidentalMedical Coverage:100 Year:5];
//NSLog(@"意外premium＝ %f",premium);


#import <Foundation/Foundation.h>
#import "TraditionalLife.h"
#import "AccidentalLife.h"
#import "HealthyInsurance.h"
#import "PensionInsurance.h"
#import "EducationInsurance.h"







@interface CalculationInsurance : NSObject

+(float) returnPremium:(insuranceModel)type
              SpecificInsurance:(productOfInsurance)specific
                       Coverage:(float)coverage
                           Year:(int)year;

@end