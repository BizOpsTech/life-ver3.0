//
//  TraditionalLife.m
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "TraditionalLife.h"

@implementation TraditionalLife



-(float)Calculation:(productOfInsurance)specificType{
    
    float premium = 0;
    if(specificType == product_TermElite)
    {
            switch (self.year) {
                case 1:
                    premium = 48 * self.coverage / 1000;
                    break;
                case 5:
                    premium = 15 * self.coverage / 1000;
                    break;
                case 10:
                    premium = 9.5 * self.coverage / 1000;
                    break;
                case 20:
                    premium = 6 * self.coverage / 1000;
                    break;
                default:
                    break;
            }
    }
    else if (specificType == product_TermEssential)
    {
        switch (self.year) {
            case 1:
                premium = 34 * self.coverage / 1000;
                break;
            case 5:
                premium = 11 * self.coverage / 1000;
                break;
            case 10:
                premium = 6 * self.coverage / 1000;
                break;
            case 20:
                premium = 4 * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_EndowmentPremier)
    {
        switch (self.year) {
            case 1:
                premium = 280 * self.coverage / 1000;
                break;
            case 5:
                premium = 74 * self.coverage / 1000;
                break;
            case 10:
                premium = 40 * self.coverage / 1000;
                break;
            case 20:
                premium = 23 * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_UniversalProtector)
    {
        switch (self.year) {
            case 1:
                premium = 411 * self.coverage / 1000;
                break;
            case 5:
                premium = 101 * self.coverage / 1000;
                break;
            case 10:
                premium = 66 * self.coverage / 1000;
                break;
            case 20:
                premium = 39 * self.coverage / 1000;
                break;
            default:
                break;
        }
     }

    return  premium;
}


@end
