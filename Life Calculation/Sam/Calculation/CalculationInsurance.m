//
//  Fileaaa.c
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "CalculationInsurance.h"

@implementation CalculationInsurance


+(float) returnPremium:(insuranceModel)type
                          SpecificInsurance:(productOfInsurance)specific
                                   Coverage:(float)coverage
                                       Year:(int)year
{

    CalculationRule * calculationRule = nil;
    switch (type) {
        case insuranceModel_Traditional:
            calculationRule = [TraditionalLife new];
            break;
        case insuranceModel_Accidental:
            calculationRule = [AccidentalLife new];
            break;
        case insuranceModel_Healthy:
            calculationRule = [HealthyInsurance new];
            break;
        case insuranceModel_Pension:
            calculationRule = [PensionInsurance new];
            break;
        case insuranceModel_Education:
            calculationRule = [EducationInsurance new];
            break;
        default:
            break;
    }
    calculationRule.coverage = coverage;
    calculationRule.year = year;
    return [calculationRule Calculation:specific];
}


@end