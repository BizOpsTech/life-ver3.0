//
//  AccidentalLife.m
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "AccidentalLife.h"

@implementation AccidentalLife


-(float)Calculation:(productOfInsurance)specificType{
    
    float premium = 0;
    if(specificType == product_AccidentalMedicalReimbursement)
    {
        switch (self.year) {
            case 1:
                premium =  100 * self.coverage / 1000;
                break;
            case 5:
                premium =  24 * self.coverage / 1000;
                break;
            case 10:
                premium =  15 * self.coverage / 1000;
                break;
            case 20:
                premium =  10 * self.coverage / 1000;;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_AccidentComprehensive)
    {
        switch (self.year) {
            case 1:
                premium = 57 * self.coverage / 1000 +200 * self.coverage/1000;
                break;
            case 5:
                premium = 14 * self.coverage / 1000 + 50 * self.coverage/1000;
                break;
            case 10:
                premium = 10 * self.coverage / 1000 + 25 * self.coverage/1000;
                break;
            case 20:
                premium = 5 * self.coverage / 1000 + 17 * self.coverage/1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_AccidentProtector)
    {
        switch (self.year) {
            case 1:
                premium = 50 * self.coverage / 1000;
                break;
            case 5:
                premium = 12 * self.coverage / 1000;
                break;
            case 10:
                premium = 7 * self.coverage / 1000;
                break;
            case 20:
                premium = 4.1 * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    else if (specificType == product_AccidentBase)
    {
        switch (self.year) {
            case 1:
                premium = 21 * self.coverage / 1000;
                break;
            case 5:
                premium = 5.2f * self.coverage / 1000;
                break;
            case 10:
                premium = 3.1f * self.coverage / 1000;
                break;
            case 20:
                premium = 2.0f * self.coverage / 1000;
                break;
            default:
                break;
        }
    }
    
    return  premium;
}

@end
