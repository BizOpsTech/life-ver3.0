//
//  CalculationRule.h
//  Lifecalculator
//
//  Created by bizopstech on 13-11-20.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface CalculationRule : NSObject
@property (assign,nonatomic) int year;
@property (assign,nonatomic) float coverage;
-(float)Calculation:(productOfInsurance)specificType;
@end
