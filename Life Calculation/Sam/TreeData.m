//
//  TreeData.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-25.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import "TreeData.h"
#import "UserDb.h"

@interface TreeData()
    @property (strong,nonatomic) UserDb * userDb;
@end




@implementation TreeData
@synthesize userDb = _userDb;


-(id) init:(UserDb *)UserDB
{
    if(self == [super init])
    {
        _userDb = UserDB;
        [self LifeData];
        [self AccidentData];
        [self HealthData];
        [self PensionData];
        [self EducationData];
        
    }
    return (self);
}


-(void)LifeData{

    NSNumber * YearlynetIncome = [NSNumber numberWithInteger:_userDb.earnOfAfterTax - _userDb.LifeCost * 12];
    
    int retirement_Age;
    if(_userDb.gender.selectIndex == 2)
    {
        retirement_Age = 60;
    }
    else{
        retirement_Age = 55;
    }
    
    NSNumber * BeforeRetirementAge = [NSNumber numberWithInteger:retirement_Age - _userDb.age];
    NSNumber * CurrentLifeInsurance = [NSNumber numberWithInteger:_userDb.lifeInsurance];
    NSNumber * CurrentEndowmentInsurance = [NSNumber numberWithInteger:_userDb.depositInsurance];
    
    self.LifeTreeValues = [NSArray arrayWithObjects:
                           [NSString stringWithFormat:@"%d 万", (int)[YearlynetIncome intValue] /10000],
                           [NSString stringWithFormat:@"%d 年", [BeforeRetirementAge intValue]] ,
                           [NSString stringWithFormat:@"%d 万", (int)[CurrentLifeInsurance intValue] /10000],
                           [NSString stringWithFormat:@"%d 万", (int)[CurrentEndowmentInsurance intValue] /10000]
                           ,nil];
}

-(void)AccidentData{
    
    NSNumber * YearlynetIncome = [NSNumber numberWithInteger:_userDb.earnOfAfterTax - _userDb.LifeCost * 12];
    
    int retirement_Age;
    if(_userDb.gender.selectIndex == 2)
    {
        retirement_Age = 60;
    }
    else{
        retirement_Age = 55;
    }
    
    NSNumber * BeforeRetirementAge = [NSNumber numberWithInteger:retirement_Age - _userDb.age];
    NSNumber * CurrentLifeInsurance = [NSNumber numberWithInteger:_userDb.lifeInsurance];
    NSNumber * CurrentAccidentInsurance = [NSNumber numberWithInteger:_userDb.accidentInsurance];
    NSNumber * CurrentEndowmentInsurance = [NSNumber numberWithInteger:_userDb.depositInsurance];
    
    self.AccidentTreeValues = [NSArray arrayWithObjects:
                               [NSString stringWithFormat:@"%d 万", (int)[YearlynetIncome intValue] /10000],
                               [NSString stringWithFormat:@"%d 年", [BeforeRetirementAge intValue]],
                               [NSString stringWithFormat:@"%d 万", (int)[CurrentLifeInsurance intValue] /10000],
                               [NSString stringWithFormat:@"%d 万", (int)[CurrentAccidentInsurance intValue] /10000],
                               [NSString stringWithFormat:@"%d 万", (int)[CurrentEndowmentInsurance intValue] /10000],
                               nil];
}

-(void)HealthData{
    
    NSNumber * AvgSpending = [NSNumber numberWithInteger:500000];
    NSNumber * BeforeSixtyYearOld = [NSNumber numberWithInteger:65 - _userDb.age];
    NSNumber * SocialInsurance = [ NSNumber numberWithInteger:_userDb.isHaveInsurance];
    NSNumber * CurrentAccidentInsurance = [NSNumber numberWithInteger:_userDb.accidentInsurance];
    self.HealthTreeValues = [NSArray arrayWithObjects:
                             [NSString stringWithFormat:@"%d 万", (int)[AvgSpending intValue] /10000],
                             [NSString stringWithFormat:@"%d 年", [BeforeSixtyYearOld intValue]],
                             [NSString stringWithFormat:@"%d 万", (int)[SocialInsurance intValue] /10000],
                             [NSString stringWithFormat:@"%d 万", (int)[CurrentAccidentInsurance intValue] /10000],
                             nil];
}

-(void) PensionData{
    NSNumber * YearlySpending = [NSNumber numberWithInteger:_userDb.LifeCost * 12];
    
    int retirement_Age;
    int RetirementToDie;
    if(_userDb.gender.selectIndex == 2)
    {
        retirement_Age = 60;
        RetirementToDie = 21;
    }
    else{
        retirement_Age = 55;
        RetirementToDie = 26;
    }
    
    NSNumber * BeforeRetirementAge = [NSNumber numberWithInteger:retirement_Age - _userDb.age];
    
    NSNumber *YearLiveafterRetirement = [NSNumber numberWithInteger:RetirementToDie];
    
    NSNumber * CurrentEndowmentInsurance = [NSNumber numberWithInteger:_userDb.depositInsurance];
    
    self.PensionTreeValues = [NSArray arrayWithObjects:
                              [NSString stringWithFormat:@"%d 万", (int)[YearlySpending intValue] /10000] ,
                              [NSString stringWithFormat:@"%d 年", [BeforeRetirementAge intValue]],
                              [NSString stringWithFormat:@"%d 年", [YearLiveafterRetirement intValue]] ,
                              [NSString stringWithFormat:@"%d 万", (int)[CurrentEndowmentInsurance intValue] /10000] ,
                              nil];
}

-(void)EducationData{
    int collegeTuition;
    if(_userDb.SchoolType.selectIndex == 2)
    {
        collegeTuition = 100 * 10000;
    }
    else
    {
        collegeTuition = 10 * 10000;
    }
    NSNumber * CurrentCollegeTuitionFee = [NSNumber numberWithInteger:collegeTuition];
    NSNumber * NumberOfKids = [NSNumber numberWithInteger:_userDb.kids];
    NSNumber * YearsToComeBefore = [NSNumber numberWithInteger:_userDb.family.attendSchoolInYears];
    
    self.EducationTreeValues = [NSArray arrayWithObjects:
                                [NSString stringWithFormat:@"%d 万", (int)[CurrentCollegeTuitionFee intValue] /10000] ,
                                [NSString stringWithFormat:@"%d ",[NumberOfKids intValue]],
                                [NSString stringWithFormat:@"%d 年", [YearsToComeBefore intValue]] ,
                                nil];
}




@end
