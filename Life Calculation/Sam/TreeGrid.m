//
//  TreeGrid.m
//  Life_20131116
//
//  Created by bizopstech on 13-11-24.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "treeCell2.h"
#import "TreeGrid.h"
#import "TreeData.h"


@interface TreeGrid ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) NSArray *LifeData;
@property (nonatomic,strong)NSArray * AccidentData;
@property (nonatomic,strong) NSArray * HealthData;
@property (nonatomic,strong) NSArray * PensionData;
@property (nonatomic,strong) NSArray * EducationData;
@property(nonatomic,strong)AppDelegate *app;
@property (nonatomic,strong) NSArray * CurrentData;

@property (nonatomic,strong) TreeData * tree_Data;

@property (nonatomic,strong) NSArray * CurrentValueData;

@end

@implementation TreeGrid
@synthesize LifeCoverage = _LifeCoverage;
@synthesize AccidentCoverage = _AccidentCoverage;
@synthesize EducationCoverage = _EducationCoverage;
@synthesize HealthCoverage = _HealthCoverage;
@synthesize PenSionCoverage = _PenSionCoverage;
@synthesize Caption = _Caption;
@synthesize iModel = _iModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)cancelAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getTreeSource:(insuranceModel)model{
    switch (model) {
        case insuranceModel_Traditional:
            _CurrentData = self.LifeData;
            _CurrentValueData = _tree_Data.LifeTreeValues;
            break;
        case insuranceModel_Accidental:
            _CurrentData = self.AccidentData;
            _CurrentValueData = _tree_Data.AccidentTreeValues;
            break;
        case insuranceModel_Healthy:
            _CurrentData = self.HealthData;
            _CurrentValueData = _tree_Data.HealthTreeValues;
            break;
        case insuranceModel_Pension:
            _CurrentData = self.PensionData;
            _CurrentValueData = _tree_Data.PensionTreeValues;
            break;
        case insuranceModel_Education:
            _CurrentData = self.EducationData;
            _CurrentValueData = _tree_Data.EducationTreeValues;
            break;
        default:
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(cancelAction:)];
    self.navigationItem.leftBarButtonItem = leftBar;
    
    UIFont *myFont = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:10];
    NSArray *btnArr = [NSArray arrayWithObjects:_BtnEducation,_BtnAccident,_BtnLife,_BtnHealth,_BtnPension, nil];
    
    for(NSUInteger i=0; i< btnArr.count; i++){
        UIButton *tempBtn = (UIButton *)[btnArr objectAtIndex:i];
        tempBtn.titleLabel.numberOfLines = 0;
        tempBtn.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        tempBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        //tempBtn.titleLabel.font = myFont;
    }
    
    UIFont *myFont18 = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:18];
    _InsuranceTitle.font = myFont18;
    
    self.LifeData = [NSArray arrayWithObjects:
                     @{@"title":@"每年纯收入",@"InPage":@4},
                     @{@"title":@"退休前工作年限",@"InPage":@1},
                     @{@"title":@"当前已有保障",@"InPage":@7},
                     nil];
    
    self.HealthData = [NSArray arrayWithObjects:
                       @{@"title":@"重疾的平均花费",@"InPage":@0},
                       @{@"title":@"距离65岁的年限",@"InPage":@1},
                       @{@"title":@"社保余额",@"InPage":@7},
                       @{@"title":@"当前已有的重疾保额",@"InPage":@7},
                       nil];
    
    self.PensionData = [NSArray arrayWithObjects:
                        @{@"title":@"每年花费",@"InPage":@3},
                        @{@"title":@"退休前工作年限",@"InPage":@1},
                        @{@"title":@"退休后的预期生活年限",@"InPage":@1},
                        @{@"title":@"当前已有养老保障",@"InPage":@7},
                        nil];
    
    self.EducationData = [NSArray arrayWithObjects:
                          @{@"title":@"当前大学学费",@"InPage":@3},
                          @{@"title":@"需要供养的小孩数量",@"InPage":@3},
                          @{@"title":@"距离上大学年限",@"InPage":@3},
                          nil];
   

    
    if([[[UIDevice currentDevice]systemVersion]floatValue] >= 7.0){
        UIEdgeInsets gridInsets;
        gridInsets.top = -60;
        gridInsets.bottom = gridInsets.left = gridInsets.right = 0;
        [_dataGrid setContentInset:gridInsets];
    }

    ///// 数据源 /////////
    _app = [[UIApplication sharedApplication]delegate];
    UserDb *source = _app.recodDb;
    _dataGrid.delegate = self;
    _dataGrid.dataSource = self;
    
    _tree_Data = [[TreeData alloc] init:source];
    ///// 数据源 /////////
    [self getTreeSource:_iModel];

    NSNumberFormatter *dataFormat = [[NSNumberFormatter alloc]init];
    dataFormat.numberStyle = NSNumberFormatterDecimalStyle;
    
    int UnitVal = 10000;
    NSString *formatLifeCoverage =  [dataFormat stringFromNumber:[NSNumber numberWithInt:(int)_LifeCoverage/UnitVal]];
    NSString *formatAccidentCoverage =  [dataFormat stringFromNumber:[NSNumber numberWithInt:(int)_AccidentCoverage/UnitVal]];
    NSString *formatEducationCoverage =  [dataFormat stringFromNumber:[NSNumber numberWithInt:(int)_EducationCoverage/UnitVal]];
    NSString *formatHealthCoverage =  [dataFormat stringFromNumber:[NSNumber numberWithInt:(int)_HealthCoverage/UnitVal]];
    NSString *formatPenSionCoverage =  [dataFormat stringFromNumber:[NSNumber numberWithInt:(int)_PenSionCoverage/UnitVal]];
    
    UILabel *labelForLife = (UILabel*)[self.view viewWithTag:1201];
    UILabel *labelAccident = (UILabel*)[self.view viewWithTag:1202];
    UILabel *labelPension = (UILabel*)[self.view viewWithTag:1203];
    UILabel *labelHealth = (UILabel*)[self.view viewWithTag:1204];
    UILabel *labelEducation = (UILabel*)[self.view viewWithTag:1205];
    
    NSString *UnitSymbol = @"万";
    labelForLife.text = [NSString stringWithFormat:@"%@%@",formatLifeCoverage,UnitSymbol];
    labelForLife.font = myFont;
    
    labelAccident.text = [NSString stringWithFormat:@"%@%@",formatAccidentCoverage,UnitSymbol];
    labelAccident.font = myFont;
    
    labelPension.text = [NSString stringWithFormat:@"%@%@",formatPenSionCoverage,UnitSymbol];
    labelPension.font = myFont;
    
    labelHealth.text = [NSString stringWithFormat:@"%@%@",formatHealthCoverage,UnitSymbol];
    labelHealth.font = myFont;
    
    labelEducation.text = [NSString stringWithFormat:@"%@%@",formatEducationCoverage,UnitSymbol];
    labelEducation.font = myFont;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _CurrentData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    treeCell2 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"treeCell2" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    int index = (int)indexPath.row;
    if(index % 2 == 0)
    {
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    cell.titleLabel.text = [[_CurrentData objectAtIndex:index] objectForKey:@"title"];
    NSArray * arr = [[_CurrentValueData objectAtIndex:index] componentsSeparatedByString:@" "];
    cell.numberLabel.text = [arr objectAtIndex:0];
    if(arr.count > 1){
        cell.unitLabel.text = [arr objectAtIndex:1];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    int pageNum = [[[_CurrentData objectAtIndex:index] objectForKey:@"InPage"] intValue];
    [self dismissViewControllerAnimated:YES completion:^(void){
        [self.parentNC popToViewController: [self.parentNC.viewControllers objectAtIndex: (pageNum - 1)] animated:YES];
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    self.InsuranceTitle.text = _Caption;
}

- (IBAction)transformEducation:(id)sender {
    _CurrentData = self.EducationData;
    self.InsuranceTitle.text = @"教育险";
    self.CurrentValueData = _tree_Data.LifeTreeValues;
    [_dataGrid reloadData];
}

- (IBAction)transformAccident:(UIButton *)sender {
    _CurrentData = self.AccidentData;
    self.InsuranceTitle.text = @"意外险";
    self.CurrentValueData = _tree_Data.AccidentTreeValues;
    [_dataGrid reloadData];
}

- (IBAction)transformLife:(id)sender {
    _CurrentData = self.LifeData;
    self.InsuranceTitle.text = @"人寿和意外险";
    self.CurrentValueData = _tree_Data.LifeTreeValues;
    [_dataGrid reloadData];
}

- (IBAction)transformHealth:(UIButton *)sender {
    _CurrentData = self.HealthData;
     self.InsuranceTitle.text = @"健康险";
    self.CurrentValueData = _tree_Data.HealthTreeValues;
    [_dataGrid reloadData];
}

- (IBAction)transformPension:(UIButton *)sender {
    _CurrentData = self.PensionData;
     self.InsuranceTitle.text = @"养老险";
    self.CurrentValueData = _tree_Data.PensionTreeValues;
    [_dataGrid reloadData];
}
@end
