//
//  NowYouCan.m
//  TwoPage
//
//  Created by bizopstech on 13-10-31.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "NowYouCan.h"
@interface NowYouCan ()
@property(nonatomic,copy)NSString *companynotice;
@property(nonatomic,copy)NSString *companyphone;
@end

@implementation NowYouCan


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)strokeForView{
    NSUInteger childViewCount = [[_groupView subviews]count];
    UIView *tempView;
    UIColor *borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0];
    for(NSUInteger i=0; i<childViewCount;i++){
        tempView = [[_groupView subviews]objectAtIndex:i];
        tempView.layer.borderColor = borderColor.CGColor;
        tempView.layer.borderWidth = 1.f;
        tempView.layer.cornerRadius = 4.f;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.TotalPremium.numberOfLines = 0;
    self.TotalPremium.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.TotalCoverage.numberOfLines = 0;
    self.TotalCoverage.lineBreakMode = NSLineBreakByWordWrapping;

    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    _labelOfCoverage.text = app.SumCoverage == nil ?  @"0" : app.SumCoverage;
    _labelOfPermium.text = app.SumPremium == nil ?  @"0" : app.SumPremium;
    

    [self strokeForView];
    
    
    if([kVersion isEqualToString:@"CPIC"]){
        _companynotice = @"您即将拨打太平洋保险公司95500销售热线";
        _companyphone = @"tel://95500";
        
    }else if ([kVersion isEqualToString:@"PingAn"]){
        _companynotice = @"您即将拨打平安保险公司95511销售热线";
        _companyphone = @"tel://95511";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
////#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
}

- (IBAction)CallTMR:(id)sender {
    UIAlertView *alertView =[[UIAlertView alloc]initWithTitle:@"通知" message:_companynotice delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消",nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_companyphone]];
    }
}



@end
