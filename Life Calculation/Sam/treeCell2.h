//
//  treeCell2.h
//  Life_20131116
//
//  Created by bizopstech on 13-11-24.
//  Copyright (c) 2013年 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface treeCell2 : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *numberLabel;
@property (strong, nonatomic) IBOutlet UILabel *unitLabel;
@end
