//
//  QuestionWithFamily.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithFamily : UIViewController
- (IBAction)actionWithButton:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnOfKids;
@property (strong, nonatomic) IBOutlet UIButton *btnOfSeniors;
@property (strong, nonatomic) IBOutlet UIButton *btnOfSpending;
@property (strong, nonatomic) IBOutlet UIButton *btnOfSchool;
@property (strong, nonatomic) IBOutlet UIButton *btnOfYear;
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;

@end
