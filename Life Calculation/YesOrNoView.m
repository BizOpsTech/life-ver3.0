//
//  YesOrNoView.m
//  Life Calculation
//
//  Created by Snow on 9/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "YesOrNoView.h"

@implementation YesOrNoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        //
        nikeImage = [UIImage imageNamed:@"yes"];
        self.on = NO;
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (IBAction)selectLeftButtonAction:(id)sender {
    [leftButton setImage:nikeImage forState:UIControlStateNormal];
    [rightButton setImage:nil forState:UIControlStateNormal];
    self.on = YES;
    
    if ([self.delegate respondsToSelector:@selector(selectedButtonState:)]) {
        [self.delegate selectedButtonState:YES];
    }
}

- (IBAction)selectRightButtonAction:(id)sender {
    [leftButton setImage:nil forState:UIControlStateNormal];
    [rightButton setImage:nikeImage forState:UIControlStateNormal];
    self.on = NO;
    
    if ([self.delegate respondsToSelector:@selector(selectedButtonState:)]) {
        [self.delegate selectedButtonState:NO];
    }
    
}



@end
