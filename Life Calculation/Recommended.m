//
//  Recommended.m
//  Life Calculation
//
//  Created by BizOpsTech on 11/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "AppDelegate.h"
#import "Summary.h"
#import "ProductList.h"
#import "ProductCell.h"
#import "Recommended.h"
#import "Product.h"
#import "NowYouCan.h"
#import "SWShareScreenShot.h"

@interface Recommended ()<UITableViewDataSource,UITableViewDelegate,ProductCellViewDelegate,ProductListDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property(nonatomic,strong)AppDelegate *app;
@property(nonatomic,strong)Summary *SDB;
@property(nonatomic,strong) Product * productObj;
@property(nonatomic,assign)NSUInteger CategoryIndex;
@property(nonatomic,strong)NSNumberFormatter *dataFormat;

@end

@implementation Recommended

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)turnPageBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:@"结果展示" style:UIBarButtonItemStyleDone target:self action:@selector(turnPageBack)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    
    _dataFormat = [[NSNumberFormatter alloc]init];
    _dataFormat.numberStyle = NSNumberFormatterDecimalStyle;
    
    /////////////////////////////////////////////////////
    
    _app = [[UIApplication sharedApplication]delegate];
    _dataGrid.delegate = self;
    _dataGrid.dataSource = self;
    _SDB = [Summary new];
    
    _productObj = [[Product alloc] init];
    [self countCoverageAndPremium];
    
    
}

-(void)countCoverageAndPremium{
    float totalCoverage,totalPremium;
    totalCoverage = 0;
    totalPremium = 0;
    
    for(NSUInteger i=0; i< _gridDataArr.count; i++){
        if([[_gridDataArr objectAtIndex:i]IsSelect]){
            totalCoverage += [[_gridDataArr objectAtIndex:i]Coverage];
            totalPremium += [[_gridDataArr objectAtIndex:i]Permier];
        }
    }

    _labelCoverage.text = [NSString stringWithFormat:@"%.f 万",totalCoverage/10000];
    _labelPremium.text = [_dataFormat stringFromNumber:[[NSNumber alloc]initWithInt:totalPremium]];
    
    _app.SumPremium = _labelPremium.text;
    _app.SumCoverage = _labelCoverage.text;

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)screenShot{
    [[SWShareScreenShot shareManager] keepImageByCurrentViewController:self withName:@"Recommended"];
}

- (IBAction)nextAction :(id)sender{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"您希望以邮件或彩信的方式接受我们的推荐吗？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"Email",@"彩信",@"不需要", nil];
    [actionSheet showInView:self.view];
    
}


#pragma mark - ProductCellViewDelegate
-(void)didSelectForRow:(ProductCell *)xibCell CellStatus:(BOOL)statu{
    ResultBase *baseDatas = [_gridDataArr objectAtIndex:xibCell.rowIndex];
    baseDatas.IsSelect = statu;
    [self countCoverageAndPremium];
}

#pragma mark - UITableViewDataSource,UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (tableView.frame.size.height / _gridDataArr.count);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _gridDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        static NSString *CellIdentifier = @"PCell";
        ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            UINib *nib = [UINib nibWithNibName:@"ProductCell" bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
            //nibsRegistered = YES;
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        NSUInteger row = [indexPath row];
        ResultBase *cellData = [_gridDataArr objectAtIndex:row];
    
        ///////////产品类别和产品名称 ///////////    
        NSString *category = [_SDB getCategoryName:cellData.InsuranceType];
        NSString * productname;
        if(cellData.productID == product_None){
            productname = @"NA";
        }else{
            productname = [[_productObj getProductDetail:cellData.InsuranceType productID:cellData.productID] objectForKey:@"name"];
        }
        NSString *formatPermier = [_dataFormat stringFromNumber:[[NSNumber alloc]initWithInt:cellData.Permier]];
    
        cell.delegate = self;
        cell.rowIndex = row;
        cell.CategoryName.text= [NSString stringWithFormat:@"%@ | %@",category,productname];
        cell.labelCoverage.text = [NSString stringWithFormat:@"%.f 万",cellData.Coverage/10000];
        cell.labelTerm.text = [NSString stringWithFormat:@"%d",cellData.Term];
        cell.labelPremium.text = [NSString stringWithFormat:@"￥%@",formatPermier] ;
        cell.IsChecked = cellData.IsSelect;
        [cell didChangeValueForCheckBox:cellData.IsSelect];
        return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _CategoryIndex = indexPath.row;
    [self performSegueWithIdentifier:@"gotoProductList" sender:[NSNumber numberWithInteger:indexPath.row]];
 
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"gotoProductList"]) {
        ResultBase *cellData = [_gridDataArr objectAtIndex:_CategoryIndex];
        NSNumber *number = (NSNumber*)sender;
        ProductList *pl = [segue destinationViewController];
        pl.iModel = cellData.InsuranceType;
        pl.recommendCoverage = cellData.Coverage;
        pl.recommendPremium = cellData.Permier;
        pl.recommendterm = cellData.Term;
        pl.recommendProductId = cellData.productID;
        pl.delegate = self;
        pl.superCellindex = number.intValue;
    }
    else if ([segue.identifier isEqualToString:@"NextYourchoices"]){

        
    }
}


- (void)productListDelegate:(ProductList*)aProductList withChangeTerm:(int)term withChangeCoverage:(int)coverage withPremium:(float)premium withInsuranceModel:(insuranceModel)aInsuranceModel withSelectProductId:(productOfInsurance)productId{

    ResultBase *baseDatas = [_gridDataArr objectAtIndex:aProductList.superCellindex];
    baseDatas.productID = productId;
    baseDatas.Term = term;
    baseDatas.Coverage = coverage;
    baseDatas.Permier = premium;
    
    [self.dataGrid reloadData];
    
    [self countCoverageAndPremium];
    
}

#pragma mark    -Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *string = [actionSheet buttonTitleAtIndex:buttonIndex];
    NSLog(@"Action sheet name %@ at %ld",string,(long)buttonIndex);
    
    switch (buttonIndex) {
            //Skip
        case 2:
            if ([self shouldPerformSegueWithIdentifier:@"NextYourchoices" sender:actionSheet])
            [self performSegueWithIdentifier:@"NextYourchoices" sender:actionSheet];
            break;
        default:
            
            break;
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self screenShot];
    switch (buttonIndex) {
            //Email
        case 0:
        {
            UIViewController *vc = [[SWShareScreenShot shareManager] shareToEmailSheetByDelegate:self withImageName:@"Recommended"];
            if (!vc)return;
            [self presentViewController:vc animated:YES completion:nil];
        }
            break;
            //MMS
        case 1:
        {
            UIViewController *vc = [[SWShareScreenShot shareManager] shareToSMSSheetByDelegate:self withImageName:@"Recommended"];
            if (!vc)return;
            [self presentViewController:vc animated:YES completion:nil];
        }
            break;
        default:
            
            break;
    }
}


#pragma mark    - Mail && Message Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self shouldPerformSegueWithIdentifier:@"NextYourchoices" sender:nil])
        [self performSegueWithIdentifier:@"NextYourchoices" sender:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self shouldPerformSegueWithIdentifier:@"NextYourchoices" sender:nil])
        [self performSegueWithIdentifier:@"NextYourchoices" sender:nil];
    
}


@end
