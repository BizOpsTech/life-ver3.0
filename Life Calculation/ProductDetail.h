//
//  ProductDetail.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProductDetailViewDelegate;
@interface ProductDetail : UIView
@property(nonatomic,assign)CGFloat viewHeight;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *detailContent;

@property(nonatomic,assign)id<ProductDetailViewDelegate>delegate;
@end

@protocol ProductDetailViewDelegate <NSObject>
-(void)didDetailViewStateChage:(BOOL)visible;
@optional
@end