//
//  QuestionWithSpending.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithSpending.h"
#import "SWContentView.h"


@interface QuestionWithSpending ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;
@property(nonatomic,strong)AppDelegate *app;
@end

@implementation QuestionWithSpending

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _app = [[UIApplication sharedApplication]delegate];
    _btnArr = [NSArray arrayWithObjects:_btnSpending,_btnRent,_btnLoans,nil];
   
    
    #pragma mark 填充用户内容
    [self setValueToPage];
    
    [self addCustomViewOnScroll];
    
    [_app autoFixedForButton:_btnArr];
    [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionWithButton:(UIButton *)sender {
    int page = 0;
    if([sender isEqual:_btnSpending]){
        page = 0;
       // [self changeBackgroundForButton:YES ButtonIndex:0];
        
    }else if([sender isEqual:_btnRent]){
        page = 1;
       // [self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnLoans]){
        page = 2;
       // [self changeBackgroundForButton:YES ButtonIndex:2];
    }
    
    CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
    
}

#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    UIColor *borderColor;
    
    if(isEditStatu){
        borderColor = [UIColor colorWithRed:0/255.f green:204/255.f blue:255/255.f alpha:1.0];
        
        ////// 修改按钮样式 ///////////////////////////////////////////////////////////////////
        
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 2.0;
        btn.layer.borderColor = borderColor.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@"buttonBG"] forState:UIControlStateNormal];
        
        ////// 取消其他按钮样式 //////////////////////////////////////////////////////////////////
        [self didEditCompleteFromButton:btnIndex];
        
        
    }else{
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        btn.layer.borderWidth = 0;
    }
}

#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{
    
    if(_app.recodDb.LifeCost >0){
//        [self setValueToButton:0 componentVal:[NSString stringWithFormat:@"%0.1f万",_app.recodDb.LifeCost/ UnitRMB]];
        [self setValueToButton:0 componentVal:[self valueFormat:(float)_app.recodDb.LifeCost/ UnitRMB]];
    }
    
    if(_app.recodDb.rental >0){
        //[self setValueToButton:1 componentVal:[NSString stringWithFormat:@"%0.1f万",_app.recodDb.rental/ UnitRMB]];
        [self setValueToButton:1 componentVal:[self valueFormat:(float)_app.recodDb.rental/ UnitRMB]];
    }
    
    if(_app.recodDb.Repayment >0){
        //[self setValueToButton:2 componentVal:[NSString stringWithFormat:@"%0.1f万",_app.recodDb.Repayment/ UnitRMB]];
        [self setValueToButton:2 componentVal:[self valueFormat:(float)_app.recodDb.Repayment/ UnitRMB]];
        
    }
    
}

-(NSString*)valueFormat:(float)value{
    NSString *strdb;
    if(value == 0.0){
        strdb=  [NSString stringWithFormat:@"%d",0];
    }else{
        if(value != (int)value){
            strdb=  [NSString stringWithFormat:@"%1.1f万",value];
        }else{
            strdb=  [NSString stringWithFormat:@"%d万",(int)value];
        }
    }
    return strdb;
}

/*
 @property(nonatomic,assign)NSUInteger LifeCost;   //生活费用
 @property(nonatomic,assign)NSUInteger rental;     //房租
 @property(nonatomic,assign)NSUInteger Repayment;  //还贷
 
 _btnSpending,_btnRent,_btnLoans
 */
#pragma mark 记录选项值
-(void)getValueFromButton{

    _app.recodDb.LifeCost = [_btnSpending.titleLabel.text floatValue]* UnitRMB ;
    _app.recodDb.rental = [_btnRent.titleLabel.text floatValue]* UnitRMB;
    _app.recodDb.Repayment = [_btnLoans.titleLabel.text floatValue]* UnitRMB;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
}





#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    
}

- (void)addCustomViewOnScroll{
    
    self.tabScroll.pagingEnabled = YES;
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    
    self.tabScroll.contentSize = CGSizeMake(width*3, height);
    
    SWContentView *content1 = [[SWContentView alloc] init];
    content1.imageView.image = ImageName(@"expense");
    content1.images = @[ImageName(@"expense")];
    content1.tag = KQuestion1;
    content1.delegate = self;
    content1.swSlider.tag = 728;
    content1.swSlider.minimumValue = 0;
    content1.swSlider.maximumValue = 10;
    content1.minLabel.text = @"0";
    content1.maxLabel.text = @"10万";
    content1.minSubLabel.hidden=YES;
    if(_app.recodDb.LifeCost >0){
        content1.swSlider.value = (float)_app.recodDb.LifeCost/UnitRMB ;
        content1.currentNumberLabel.text = [NSString stringWithFormat:@"%1.1f",(float)_app.recodDb.LifeCost/UnitRMB];
    }else{
        content1.swSlider.value = 0;
        content1.currentNumberLabel.text = @"0";
    }

    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    
    SWContentView *content2 = [[SWContentView alloc] init];
    content2.imageView.image = ImageName(@"rent");
    content2.images = @[ImageName(@"rent")];
    content2.tag = KQuestion2;
    content2.delegate = self;
    content2.swSlider.tag = 728;
    content2.swSlider.minimumValue = 0;
    content2.swSlider.maximumValue = 10;
    content2.minLabel.text = @"0";
    content2.maxLabel.text = @"10万";
    content2.minSubLabel.hidden = YES;
    if(_app.recodDb.rental >0){
        content2.currentNumberLabel.text = [NSString stringWithFormat:@"%1.1f",(float)_app.recodDb.rental/UnitRMB];
        content2.swSlider.value = (float)_app.recodDb.rental/UnitRMB;
    }else{
        content2.currentNumberLabel.text = @"0";
        content2.swSlider.value = 0;
    }
    
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    
    SWContentView *content3 = [[SWContentView alloc] init];
    content3.imageView.image = ImageName(@"loans");
    content3.images = @[ImageName(@"loans")];
    content3.tag = KQuestion3;
    content3.delegate = self;
    content3.swSlider.tag = 728;
    content3.swSlider.minimumValue = 0;
    content3.swSlider.maximumValue = 10;
    content3.minLabel.text = @"0";
    content3.maxLabel.text = @"10万";
    content3.minSubLabel.hidden=YES;
    if(_app.recodDb.Repayment >0){
        content3.swSlider.value = (float)_app.recodDb.Repayment / UnitRMB;
        content3.currentNumberLabel.text = [NSString stringWithFormat:@"%1.1f",(float)_app.recodDb.Repayment / UnitRMB];
    }else{
        content3.swSlider.value = 0;
        content3.currentNumberLabel.text = @"0";
    }
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
}


- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
    
    NSString *title;
    UIButton *cusButton;
    
    switch (tagTag) {
        case KQuestion1:
            cusButton = _btnSpending;
            break;
            
        case KQuestion2:
            cusButton = _btnRent;
            break;
        case KQuestion3:
            cusButton = _btnLoans;
            break;
            
        default:
            break;
    }
    
    
    if(value == 0.0){
        title = [NSString stringWithFormat:@"%d",0];
    }else{
        if(value != (int)value){
            title = [NSString stringWithFormat:@"%0.1f万",value];
        }else{
            title = [NSString stringWithFormat:@"%d万",(int)value];
        }
        
    }

    [self myCustomButton:cusButton withTitle:title];
}
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}

@end
