//
//  RuleSliderView.h
//  Life Calculation
//
//  Created by Snow on 9/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RuleSliderViewDelegate;
@interface RuleSliderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *sliderTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentNumberLabel;
@property (weak, nonatomic) IBOutlet UISlider *swSlider;
@property (weak, nonatomic) IBOutlet UILabel *maxLabel;
@property (weak, nonatomic) IBOutlet UILabel *minLabel;
@property (weak, nonatomic) IBOutlet UILabel *minSubLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxSubLabel;
@property (nonatomic) NSUInteger intervalValue;
@property (nonatomic, copy) NSString *defaultUnits;
@property (nonatomic, copy) NSString *defaultSuffix;
@property (nonatomic,assign) id<RuleSliderViewDelegate> delegate;

- (void)isHiddenMinAndMaxLabel:(BOOL)hidden;
- (void)isHiddenSlideTitleLabel:(BOOL)hidden;
- (void)setSliderValue:(float)value;

@end


@protocol RuleSliderViewDelegate <NSObject>
@optional
- (void)sliderValueChanged:(UISlider*)swSlider;

@end