//
//  QuestionWithInvestment.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithInvestment : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;
@property (strong, nonatomic) IBOutlet UIButton *btnInvestor;
@property (strong, nonatomic) IBOutlet UIButton *btnSaving;
@property (strong, nonatomic) IBOutlet UIButton *btnInvestment;
@property (strong, nonatomic) IBOutlet UIButton *btnProvide;


- (IBAction)actionWithButton:(UIButton *)sender;
@end
