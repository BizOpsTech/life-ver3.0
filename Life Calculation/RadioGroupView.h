//
//  RadioGroupView.h
//  Life Calculation
//
//  Created by BizOpsTech on 11/7/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RadioGroupViewDelegate;
@interface RadioGroupView : UIView{
    __weak IBOutlet UIImageView *leftBoxImageView;
    __weak IBOutlet UIImageView *middleBoxImageView;
    __weak IBOutlet UIImageView *rigthBoxImageView;
    
    __weak IBOutlet UIButton *leftButton;
    __weak IBOutlet UIButton *middleButton;
    __weak IBOutlet UIButton *rightButton;
    
    UIImage *nikeImage;
}

@property(nonatomic,assign)id<RadioGroupViewDelegate>delegate;
@property (strong, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *middleNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightNameLabel;
@property(assign,nonatomic)NSUInteger checkedIndex;

@property (nonatomic,getter=didSelectIndex)NSUInteger selectIndex;

- (void)selectedButtonAtIndex:(NSUInteger)index;

@end

@protocol RadioGroupViewDelegate <NSObject>
@optional
-(void)selectedButtonStateWithRadio:(NSUInteger)index;
@end