//
//  BeneficiaryInfoViewController.m
//  Life Calculation
//
//  Created by Snow on 11/23/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "BeneficiaryInfoViewController.h"

#define kBeneficiaryName         @"name"
#define kBeneficiaryID           @"id"
#define kBeneficiaryBirthday     @"birthdata"
#define kBeneficiaryRelationship @"relationship"
#define kBeneficiaryRatio        @"ratio"

@interface BeneficiaryInfoViewController ()<UITextFieldDelegate>

@end

@implementation BeneficiaryInfoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self customTextField:self.nameLabel];
    [self customTextField:self.idLabel];
    [self customTextField:self.birthDataLabel];
    [self customTextField:self.relationshipLabel];
    [self customTextField:self.ratioLabel];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark    - Help methods

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)doneAction:(id)sender {
    
    
    if ([self.nameLabel.text length] && [self.nameLabel.text length]) {
        
        NSDictionary *driverInfo = @{kBeneficiaryName: self.nameLabel.text,
                                      kBeneficiaryID: self.idLabel.text,
                                      kBeneficiaryBirthday: self.birthDataLabel.text,
                                      kBeneficiaryRelationship: self.relationshipLabel.text,
                                      kBeneficiaryRatio: self.ratioLabel.text};

        [self.delegate beneficiaryInfoViewController:self withInfos:driverInfo];
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
    
}

- (void)customTextField:(UITextField*)textField{

    textField.font = [UIFont systemFontOfSize:14.0f];
    textField.borderStyle = UITextBorderStyleLine;
//    textField.text = @"TextField";
    textField.contentMode = UIViewContentModeCenter;
    textField.layer.borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0].CGColor;
    textField.layer.borderWidth = 1.f;
    textField.layer.cornerRadius = 4.f;
}


- (IBAction)textFieldDone:(id)sender {
    UITextField *tf = sender;
    [tf resignFirstResponder];
}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */



#pragma mark    -UITextFiled

//文本框事件委托
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    //视图向上推
    
    //        if([textField isEqual:_nameFieldText] || [textField isEqual:_phoneFieldText] || [textField isEqual:_emailFieldText]){
    [self wallpaperSlidUp:textField];
    //      }
    
    //启动键盘
    return YES;
    
}

//视图向上推送
-(void)wallpaperSlidUp:(UITextField *)textField{
    CGRect frame = textField.frame;
    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0);//键盘高度216
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
    if(offset > 0)
        self.view.frame = CGRectMake(0.0f, -offset, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
//视图还原
-(void)wallpaperSlidDown{
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

//点击文本框以外的地方 就收起键盘
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self wallpaperSlidDown];
    //[self checkInputData];
    //[self.phoneNumberTF resignFirstResponder];
}




@end
