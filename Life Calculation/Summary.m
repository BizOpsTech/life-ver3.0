//
//  Summary.m
//  Life Calculation
//
//  Created by BizOpsTech on 12/1/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "Summary.h"
@implementation Summary
-(NSArray *)getInsuranceLevel:(UserDb *)source{
    calculatePriority * cal = [[calculatePriority alloc] init];
    cal.userDB = source;
    [cal transform];
    float traditionalLevel,retirementLevel,healthyLevel,accidentLevel,educationLevel;
    
    traditionalLevel = [cal getTraditionalLifePriority];
    retirementLevel = [cal getRetirementPriority];
    healthyLevel = [cal getHealthInsurancePriority];
    accidentLevel = [cal getAccidentPriority];
    educationLevel = [cal getChildrenEducationPriority];
    
    NSArray * levelArr = [NSArray arrayWithObjects:[NSNumber numberWithFloat:traditionalLevel],
                                 [NSNumber numberWithFloat:retirementLevel],
                                 [NSNumber numberWithFloat:healthyLevel],
                                 [NSNumber numberWithFloat:accidentLevel],
                                 [NSNumber numberWithFloat:educationLevel],
                                 nil];
    
    return levelArr;
}

-(NSArray *)getInsuranceCoverage:(UserDb *)source{
    float yearlyIncom,yearlyExpense,retireAge,currentAge,cityType,expecteAge,isRetiredOfMan;
    float EndowmentInsurance,LifeInsurance,AccidentDisease,SocialInsurance;
    yearlyIncom = source.earnOfAfterTax; //收入
    yearlyExpense = source.LifeCost *12; //年开销
    switch (source.gender.selectIndex) {
        case 1:
            //woman
            retireAge = 55;
            isRetiredOfMan = NO;
            break;
        case 2:
            //man
            retireAge = 60;
            isRetiredOfMan = YES;
            break;
        default:
            //man
            retireAge = 60;
            isRetiredOfMan = YES;
            break;
    }
    currentAge = source.age;
    cityType = 1.0;
    expecteAge = retireAge - currentAge;
    
    EndowmentInsurance = (float)source.depositInsurance;
    LifeInsurance = (float)source.lifeInsurance;
    AccidentDisease = (float)source.accidentInsurance;
    SocialInsurance = (float)source.isHaveInsurance;
    NSUInteger myKids= source.family.Kids;
    NSUInteger myCollageAge =  source.family.attendSchoolInYears;
    BOOL IsForeignCollage = source.SchoolType.selectIndex == 2 ? YES:NO;
    
    ////////////////////////////////////////// 推荐保额 /////////////////////////////////////////////////////////
    float traditionalCoverage,accidentCoverage,healthyCoverage,retirementCoverage,educationCoverage;
    Calculate *calculate  = [Calculate new];
    traditionalCoverage = [calculate calculateTraditionalLifeCoverageByYearlyIncome:yearlyIncom
                                                               withSelfYearlyExpense:yearlyExpense
                                                                  withIsRetiredOfMan:isRetiredOfMan
                                                                      withCurrentAge:currentAge
                                                              withEndowmentInsurance:EndowmentInsurance
                                                                            withLife:LifeInsurance];
    
    accidentCoverage = [calculate calculateAccidentCoverageByYearlyIncome:yearlyIncom
                                                     withSelfYearlyExpense:yearlyExpense
                                                        withIsRetiredOfMan:isRetiredOfMan
                                                            withCurrentAge:currentAge
                                                    withEndowmentInsurance:EndowmentInsurance
                                                                  withLife:LifeInsurance
                                                                    withAD:AccidentDisease];
    
    healthyCoverage = [calculate calculateHealthCoverageByCityType:cityType
                                                     withCurrentAge:currentAge
                                                withSocialInsurance:SocialInsurance
                                                             withAD:AccidentDisease];
    
    retirementCoverage = [calculate calculateRetirementCoverageByExpectedAge:expecteAge
                                                           withIsRetiredOfMan:isRetiredOfMan
                                                               withCurrentAge:currentAge
                                                         withYearlyExpenseNow:yearlyExpense
                                                       withEndowmentInsurance:EndowmentInsurance];
    
    educationCoverage = [calculate calculateChildrenEducationCoverageByChilds:myKids
                                                                withCollageAge:myCollageAge
                                                          withIsForeignCollage:IsForeignCollage];
    
    
    NSArray * coverageArr = [NSArray arrayWithObjects:[NSNumber numberWithFloat:traditionalCoverage],
                             [NSNumber numberWithFloat:retirementCoverage],
                             [NSNumber numberWithFloat:healthyCoverage],
                             [NSNumber numberWithFloat:accidentCoverage],
                             [NSNumber numberWithFloat:educationCoverage],
                             nil];
    
    return coverageArr;
}

-(buyModel)getBuyModel:(UserDb *)source{
    buyModel myBuyModel = 0;
    
    
//    if( ((18 <= source.age && source.age < 30) && source.earnOfAfterTax < 500000) || (source.age >= 50 && source.earnOfAfterTax < 500000) || (source.age > 60 && (100000 <= source.earnOfAfterTax && source.earnOfAfterTax < 500000)))
//    {
//        myBuyModel = buyWithAccidentAndHospitalization;
//        
//    }
//    
//    
//    if( ((18 <= source.age && source.age < 30) && source.earnOfAfterTax >= 500000) || ((30 <= source.age && source.age < 50) && source.earnOfAfterTax < 100000) || ((50 <= source.age && source.age < 60)  && (100000 <= source.earnOfAfterTax && source.earnOfAfterTax < 500000)))
//    {
//        myBuyModel = buyWithLifeAndCriticalDisease;
//        
//    }
//    
//    if((30 <= source.age && source.age < 50) && (100000 <= source.earnOfAfterTax && source.earnOfAfterTax < 5000000)){
//        myBuyModel = buyWithLongTermSaving;
//    }
//    
//    if(((35 <= source.age && source.age < 50) &&  source.earnOfAfterTax >= 5000000) || (source.age >= 50 && source.earnOfAfterTax >= 500000)){
//        myBuyModel = buyWithWealthManagement;
//    }
    
    int yearlyIncom = source.earnOfAfterTax;
    if( (source.age <= 30 && yearlyIncom < 500000) || (source.age > 50 && yearlyIncom < 100000) || (source.age > 60 && (100000 <= yearlyIncom && yearlyIncom < 500000)))
    {
        myBuyModel = buyWithAccidentAndHospitalization;
    }
    
    if( (source.age <= 30 && yearlyIncom >= 500000) || ((30 < source.age && source.age <= 50) && yearlyIncom < 100000) || ((30 < source.age && source.age <= 35)  && (100000 <= yearlyIncom && yearlyIncom < 500000))|| ((50 < source.age && source.age <= 60)  && (100000 <= yearlyIncom && yearlyIncom < 500000)))
    {
        myBuyModel = buyWithLifeAndCriticalDisease;
        
    }
    
    if(((35 < source.age && source.age <= 50) && (100000 <= yearlyIncom && yearlyIncom < 5000000))||((30 < source.age && source.age <=35) && (500000 <= yearlyIncom && yearlyIncom < 5000000))){
        myBuyModel = buyWithLongTermSaving;
    }
    
    if(((35 < source.age && source.age <= 50) &&  yearlyIncom >= 5000000) || (source.age > 50 && yearlyIncom >= 500000)){
        myBuyModel = buyWithWealthManagement;
    }
    
    return myBuyModel;
}

//计算调整之后的保额 (7分钟版本)
-(NSMutableArray *)getInsuranceResult:(UserDb *)source{
    int buyModeIndex = 0;
    buyModel myBuyModel = [self getBuyModel:source];
    
    switch (myBuyModel) {
        case buyWithAccidentAndHospitalization:
            buyModeIndex = 0;
            break;
        case buyWithLifeAndCriticalDisease:
            buyModeIndex = 1;
            break;
        case buyWithLongTermSaving:
            buyModeIndex = 2;
            break;
        case buyWithWealthManagement:
            buyModeIndex = 3;
            break;
    }
    
    //traditional:0 , retirement:1 , healthy:2, accident:3 , education:4
    NSArray *levelArr = [self getInsuranceLevel:source];
    NSArray *coverageArr = [self getInsuranceCoverage:source];
    
    ResultData * resultdata = [[ResultData  alloc] init:buyModeIndex
                                                    age:source.age
                                             careerType:source.investment.selectIndex
                                           LifeCoverage:[[coverageArr objectAtIndex:0] floatValue]
                                           LifePriority:[[levelArr objectAtIndex:0] floatValue]
                                      EducationCoverage:[[coverageArr objectAtIndex:4] floatValue]
                                      EducationPriority:[[levelArr objectAtIndex:4] floatValue]
                                         HealthCoverage:[[coverageArr objectAtIndex:2] floatValue]
                                         HealthPriority:[[levelArr objectAtIndex:2] floatValue]
                                       AccidentCoverage:[[coverageArr objectAtIndex:3] floatValue]
                                       AccidentPriority:[[levelArr objectAtIndex:3] floatValue]
                                        PensionCoverage:[[coverageArr objectAtIndex:1] floatValue]
                                        PensionPriority:[[levelArr objectAtIndex:1] floatValue]
                                           YearlyIncome:source.earnOfAfterTax
                                                 userDB:source];
    
    [resultdata ReCalculation:source.earnOfAfterTax];
    
    return [resultdata Sort];
}

//计算调整之后的保额 (2分钟版本)
-(NSMutableArray *)getInsuranceResult:(UserDb *)source ResultBaseArr:(NSArray *)ResultBase_Arr{
    int buyModeIndex = 0;
    buyModel myBuyModel = [self getBuyModel:source];
    
    switch (myBuyModel) {
        case buyWithAccidentAndHospitalization:
            buyModeIndex = 0;
            break;
        case buyWithLifeAndCriticalDisease:
            buyModeIndex = 1;
            break;
        case buyWithLongTermSaving:
            buyModeIndex = 2;
            break;
        case buyWithWealthManagement:
            buyModeIndex = 3;
            break;
    }
    

    float TraditionalLifeLevel,AccidentalLevel,HealthyLevel,PensionLevel,EducationLevel;
    float TraditionalLifeCoverage,AccidentalCoverage,HealthyCoverage,PensionCoverage,EducationCoverage;

    for(int i=0; i<ResultBase_Arr.count; i++ )
    {
        ResultBase * rb = ResultBase_Arr[i];
        
        switch (rb.InsuranceType ) {
            case insuranceModel_Traditional:
                TraditionalLifeLevel = rb.Priority;
                TraditionalLifeCoverage = rb.unadjusted_Coverage;
                break;
            case insuranceModel_Accidental:
                AccidentalLevel = rb.Priority;
                AccidentalCoverage = rb.unadjusted_Coverage;
                break;
            case insuranceModel_Healthy:
                HealthyLevel = rb.Priority;
                HealthyCoverage = rb.unadjusted_Coverage;
                break;
            case insuranceModel_Pension:
                PensionLevel = rb.Priority;
                PensionCoverage = rb.unadjusted_Coverage;
                break;
            case insuranceModel_Education:
                EducationLevel = rb.Priority;
                EducationCoverage = rb.unadjusted_Coverage;
                break;
            default:
                break;
        }
    }
    
    ResultData * resultdata = [[ResultData  alloc] init:buyModeIndex
                                                    age:source.age
                                             careerType:source.investment.selectIndex
                                           LifeCoverage:TraditionalLifeCoverage
                                           LifePriority:TraditionalLifeLevel
                                      EducationCoverage:EducationCoverage
                                      EducationPriority:EducationLevel
                                         HealthCoverage:HealthyCoverage
                                         HealthPriority:HealthyLevel
                                       AccidentCoverage:AccidentalCoverage
                                       AccidentPriority:AccidentalLevel
                                        PensionCoverage:PensionCoverage
                                        PensionPriority:PensionLevel
                                           YearlyIncome:source.earnOfAfterTax
                                                 userDB:source];
    
    [resultdata ReCalculation:source.earnOfAfterTax];
    return [resultdata Sort];
}

-(NSString *)getCategoryName:(insuranceModel)model{
    NSString *CategoryName;
    switch (model) {
        case insuranceModel_Traditional:
            CategoryName = @"人寿和意外险";
            break;
        case insuranceModel_Accidental:
            CategoryName = @"意外险";
            break;
        case insuranceModel_Healthy:
            CategoryName = @"健康险";
            break;
        case insuranceModel_Pension:
            CategoryName = @"养老险";
            break;
        case insuranceModel_Education:
            CategoryName = @"教育险";
            break;
        default:
            break;
    }
    return CategoryName;
}
@end
