//
//  InsuringInfo.m
//  Insurance
//
//  Created by BizOpsTech on 11/8/13.
//  Copyright (c) 2013 RbBtSn0w. All rights reserved.
//

#import "InsuringInfo.h"
#import "BeneficiaryInfoViewController.h"


@interface LabelAndTextFiledCell : UITableViewCell
@property (nonatomic, strong) UILabel *cusLabel;
@property (nonatomic, strong) UITextField *cusTextField;

@end

@implementation LabelAndTextFiledCell



- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        _cusLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 80, 21)];
        _cusLabel.font = [UIFont systemFontOfSize:16.0f];
        _cusLabel.text = @"Label";
        
        _cusTextField = [[UITextField alloc] initWithFrame:CGRectMake(110, 7, 180, 30)];
        _cusTextField.font = [UIFont systemFontOfSize:14.0f];
        _cusTextField.borderStyle = UITextBorderStyleLine;
        _cusTextField.text = @"TextField";
        _cusTextField.contentMode = UIViewContentModeCenter;
        _cusTextField.layer.borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0].CGColor;
        _cusTextField.layer.borderWidth = 1.f;
        _cusTextField.layer.cornerRadius = 4.f;
        
        
        [self.contentView addSubview:_cusLabel];
        [self.contentView addSubview:_cusTextField];
        
    }
    
    return self;
}

@end


@interface InsuringInfo ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,BeneficiaryInfoDelegate>{
    UITextField *tapCurrentTF;
}

@property(nonatomic,assign)CGFloat screenHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,assign)CGFloat screenOriginY;
@property (nonatomic, strong) NSMutableArray *infos;


@end

@implementation InsuringInfo

/*
 The insured info
 Name
 ID
 Phone
 Email
 
 
 Beneficiary info
 Add beneficiary
 
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _screenHeight = self.view.frame.size.height;
    _screenOriginY = self.view.frame.origin.y;
    
    self.infos= [NSMutableArray array];
    NSDictionary *dicButton = @{@"button": @"添加受益人"};
    [self.infos addObject:dicButton];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)strokeForTextField:(UIView *)view{
    NSUInteger count = [[view subviews]count];
    UIView *tempView;
    UIColor *borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0];
    for(NSUInteger i=0; i< count; i++){
        tempView = [[view subviews]objectAtIndex:i];
        if([tempView isKindOfClass:[UITextField class]]){
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            UITextField *txt = [[view subviews]objectAtIndex:i];
            txt.layer.borderColor = borderColor.CGColor;
            txt.layer.borderWidth = 1.f;
            txt.layer.cornerRadius = 4.f;
            txt.borderStyle = UITextBorderStyleLine;
            txt.leftView = paddingView;
            txt.leftViewMode = UITextFieldViewModeAlways;
            
        }
    }
}

#pragma mark    -UITextFiled

//文本框事件委托
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    tapCurrentTF = textField;
    //视图向上推
    [self wallpaperSlidUp:textField];
    //启动键盘
    return YES;
    
}

//视图向上推送
-(void)wallpaperSlidUp:(UITextField *)textField{
    CGRect frame = textField.frame;
    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0);//键盘高度216
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
    if(offset > 0)
        self.view.frame = CGRectMake(0.0f, -offset, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
//视图还原
-(void)wallpaperSlidDown{
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

//点击文本框以外的地方 就收起键盘
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self wallpaperSlidDown];
    //[self checkInputData];
    [tapCurrentTF resignFirstResponder];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"AddBeneficiary"]) {
        
        UINavigationController *navc= segue.destinationViewController;
        BeneficiaryInfoViewController * bivc = navc.viewControllers[0];
        bivc.delegate = self;
        
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 4;
            break;
        case 1:
            return self.infos.count;
        default:
            break;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"LabelAndTextFiledCell" forIndexPath:indexPath];
        
        LabelAndTextFiledCell *latCell = (LabelAndTextFiledCell*)cell;
        latCell.selectionStyle = UITableViewCellSelectionStyleNone;
        [latCell.cusTextField addTarget:self action:@selector(exitKeyboardWithTextField:) forControlEvents:UIControlEventEditingDidEndOnExit];
        
        switch (indexPath.row) {
            case 0:
                latCell.cusLabel.text = @"姓名";
                latCell.cusTextField.text = @"";
                break;
            case 1:
                latCell.cusLabel.text = @"身份证号";
                latCell.cusTextField.text = @"";
                break;
            case 2:
                latCell.cusLabel.text = @"联系电话";
                latCell.cusTextField.text = @"";
                break;
            case 3:
                latCell.cusLabel.text = @"电子邮箱";
                latCell.cusTextField.text = @"";
                break;
            case 4:
                latCell.cusLabel.text = @"";
                latCell.cusTextField.text = @"";
                break;
                
            default:
                break;
        }
        

        
    }else{
        static NSString *defaultCellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:defaultCellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:defaultCellIdentifier];
        }
        
        if (indexPath.row == self.infos.count-1) {
            NSString *title = [[self.infos objectAtIndex:indexPath.row] objectForKey:@"button"];
            cell.textLabel.text = title;
        }else{
            NSString *title = [[self.infos objectAtIndex:indexPath.row] objectForKey:@"name"];
            NSString *detail = [[self.infos objectAtIndex:indexPath.row] objectForKey:@"ratio"];
            
            cell.textLabel.text = title;
            cell.detailTextLabel.text = detail;
        }
        
        
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 1 && indexPath.row != (self.infos.count-1)) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_infos removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
    
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *headerTitle = @"";
    if (section == 0) {
        headerTitle = @"保单信息";
    }else{
        headerTitle = @"受益人信息";
    }
    return headerTitle;
}


#pragma mark    -
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section == 1) {
        
        [self performSegueWithIdentifier:@"AddBeneficiary" sender:[tableView cellForRowAtIndexPath:indexPath]];
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




#pragma mark    -

- (void)beneficiaryInfoViewController:(BeneficiaryInfoViewController*)aBIVC withInfos:(NSDictionary*)infos{
    
    if (self.infos.count > 0) {
        
        [self.infos insertObject:infos atIndex:self.infos.count-1];
        [self.tableView reloadData];
    }
}


-(void)exitKeyboardWithTextField:(UITextField*)sender{
    [sender resignFirstResponder];
}

@end
