//
//  QuestionWithAsset.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithAsset.h"
#import "SWContentView.h"
#import "SWContentStyle1.h"

@interface QuestionWithAsset ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;
@property(nonatomic,strong)AppDelegate *app;
@property(nonatomic,strong)Question *Qhouse;
@property(nonatomic,strong)Question *Qcar;
@end

@implementation QuestionWithAsset




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _app = [[UIApplication sharedApplication]delegate];
    _Qhouse = [Question new];
    _Qcar = [Question new];
    
    _btnArr = [NSArray arrayWithObjects:_btnCostForEach,_btnHaveHouse,_btnHaveCar,_btnAsset,_btnLoans, nil];
   

    #pragma mark 填充用户内容
    [self setValueToPage];

    [self addCustomViewOnScroll];
    
    [_app autoFixedForButton:_btnArr];
    [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionWithButton:(UIButton *)sender {
    int page = 0;
    //_btnCostForEach,_btnHaveHouse,_btnHaveCar,_btnAsset,_btnLoans
    if([sender isEqual:_btnCostForEach]){
        page = 0;
        //[self changeBackgroundForButton:YES ButtonIndex:0];
        
    }else if([sender isEqual:_btnHaveHouse]){
        page = 1;
       // [self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnHaveCar]){
        page = 2;
       // [self changeBackgroundForButton:YES ButtonIndex:2];
        
    }else if([sender isEqual:_btnAsset]){
        page = 3;
       // [self changeBackgroundForButton:YES ButtonIndex:3];
        
    }else if ([sender isEqual:_btnLoans]){
        //_btnLoans
        page = 4;
       // [self changeBackgroundForButton:YES ButtonIndex:4];
        
    }else{
        return;
    }
    
    
    
    CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
    
}


#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    UIColor *borderColor;
    
    if(isEditStatu){
        borderColor = [UIColor colorWithRed:0/255.f green:204/255.f blue:255/255.f alpha:1.0];
        
        ////// 修改按钮样式 ///////////////////////////////////////////////////////////////////
        
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 2.0;
        btn.layer.borderColor = borderColor.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@"buttonBG"] forState:UIControlStateNormal];
        
        ////// 取消其他按钮样式 //////////////////////////////////////////////////////////////////
        [self didEditCompleteFromButton:btnIndex];
        
        
    }else{
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        btn.layer.borderWidth = 0;
    }
}

#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{
    if(_app.recodDb.earnOfAfterTax >0){
        [self setValueToButton:0 componentVal:[NSString stringWithFormat:@"%d万",_app.recodDb.earnOfAfterTax / UnitRMB]];
    }
    if(_app.recodDb.statusOfHouse.selectIndex >0){
        _Qhouse.selectIndex = _app.recodDb.statusOfHouse.selectIndex;
        [self setValueToButton:1 componentVal:_app.recodDb.statusOfHouse.answer];
    }
    if(_app.recodDb.statusOfCar.selectIndex >0){
        _Qcar.selectIndex = _app.recodDb.statusOfCar.selectIndex;
        [self setValueToButton:2 componentVal:_app.recodDb.statusOfCar.answer];
    }
    if(_app.recodDb.familyAssets >0){
        [self setValueToButton:3 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.familyAssets/ UnitRMB]];
    }
    if(_app.recodDb.debtCounts >0){
        [self setValueToButton:4 componentVal:[NSString stringWithFormat:@"%ld万",(long)_app.recodDb.debtCounts/ UnitRMB]];
    }
}

/*
 @property(nonatomic,assign)int earnOfAfterTax;  //税后收入
 @property(nonatomic,assign)Question *statusOfHouse;
 @property(nonatomic,assign)Question *statusOfCar;
 @property(nonatomic,assign)NSUInteger familyAssets;  //家庭资产
 @property(nonatomic,assign)NSUInteger debtCounts;    //债务总额
 
 _btnCostForEach,_btnHaveHouse,_btnHaveCar,_btnAsset,_btnLoans
 */
#pragma mark 记录选项值
-(void)getValueFromButton{
    _app.recodDb.earnOfAfterTax = [_btnCostForEach.titleLabel.text intValue]*UnitRMB;
    _app.recodDb.familyAssets = [_btnAsset.titleLabel.text integerValue]*UnitRMB;
    _app.recodDb.debtCounts = [_btnLoans.titleLabel.text integerValue]*UnitRMB;
    
    _Qhouse.answer = _btnHaveHouse.titleLabel.text;
    _Qcar.answer = _btnHaveCar.titleLabel.text;
    
    _app.recodDb.statusOfHouse = _Qhouse;
    _app.recodDb.statusOfCar = _Qcar;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
}



#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    if(![UIDevice isRunningOniPhone5]){
        [self.view sendSubviewToBack:_tabScroll];
    }
}

- (void)addCustomViewOnScroll{
    
    self.tabScroll.pagingEnabled = YES;
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    
    self.tabScroll.contentSize = CGSizeMake(width*5, height);
    
    SWContentView *content1 = [[SWContentView alloc] init];
    content1.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"saving24"]];
    content1.tag = KQuestion1;
    content1.delegate = self;
    content1.swSlider.tag = 724;
    content1.swSlider.minimumValue = 2;
    content1.swSlider.maximumValue = 500;
    content1.minLabel.text = @"2万";
    content1.maxLabel.text = @"500万";
    if (_app.recodDb.earnOfAfterTax >0) {
        
        content1.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.earnOfAfterTax/UnitRMB];
        content1.swSlider.value = (float)_app.recodDb.earnOfAfterTax/UnitRMB;
        [content1 sliderValueChanged: content1.swSlider];
        
    }else{
        content1.currentNumberLabel.text = @"0";
        content1.swSlider.value = 2;
    }
    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    
    SWContentStyle1 *content2 = [[SWContentStyle1 alloc] init];
    content2.images = @[[UIImage imageNamed:@"house_no"],[UIImage imageNamed:@"house_yes"]];
    if(_app.recodDb.statusOfHouse.selectIndex >0){
        content2.imageView.image = content2.images[_app.recodDb.statusOfHouse.selectIndex-1];
        [content2.rgView2 selectedButtonAtIndex:_app.recodDb.statusOfHouse.selectIndex];
    }else{
        content2.imageView.image = ImageName(@"house_no");
    }
    content2.tag = KQuestion2;
    content2.delegate = self;
    content2.leftNameLabel.text = @"没有";
    content2.rightNameLabel.text = @"有";
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    
    SWContentStyle1 *content3 = [[SWContentStyle1 alloc] init];
    content3.images = @[[UIImage imageNamed:@"car_no"],[UIImage imageNamed:@"car_yes"]];
    if(_app.recodDb.statusOfCar.selectIndex >0){
        //content3.imageView.image = content3.images[_app.recodDb.statusOfCar.selectIndex-1];
        [content3.rgView2 selectedButtonAtIndex:_app.recodDb.statusOfCar.selectIndex];
    }else{
        content3.imageView.image = ImageName(@"car_no");
    }
    content3.tag = KQuestion3;
    content3.delegate = self;
    content3.leftNameLabel.text = @"没有";
    content3.rightNameLabel.text = @"有";
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
    
    
    SWContentView *content4 = [[SWContentView alloc] init];
    content4.imageView.image = ImageName(@"assets");
    content4.images = @[ImageName(@"assets")];
    content4.tag = KQuestion4;
    content4.delegate = self;
    content4.rsView.intervalValue = 0;
    content4.swSlider.tag = 727;
    content4.swSlider.minimumValue = 0;
    content4.swSlider.maximumValue = 500;
    content4.minLabel.text = @"0";
    content4.maxLabel.text = @"500万";
    content4.minSubLabel.hidden=YES;
    if(_app.recodDb.familyAssets >0){
        //content4.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.familyAssets/UnitRMB];
        content4.swSlider.value = _app.recodDb.familyAssets/UnitRMB;
    }else{
        content4.currentNumberLabel.text = @"0";
        content4.swSlider.value = 0;
    }

    
    [self addViewObject:content4 withHeight:height withWidth:width withAtIndex:3];
    
    
    SWContentView *content5 = [[SWContentView alloc] init];
    content5.imageView.image = ImageName(@"debt");
    content5.images = @[ImageName(@"debt")];
    content5.tag = KQuestion5;
    content5.delegate = self;
    content5.rsView.intervalValue = 0;
    content5.swSlider.tag = 727;
    content5.swSlider.minimumValue = 0;
    content5.swSlider.maximumValue = 500;
    content5.minLabel.text = @"0";
    content5.maxLabel.text = @"500万";
    content5.minSubLabel.hidden=YES;
    if(_app.recodDb.debtCounts >0){
        content5.swSlider.value = (float)_app.recodDb.debtCounts/UnitRMB;
        content5.currentNumberLabel.text = [NSString stringWithFormat:@"%lu",(long)_app.recodDb.debtCounts/UnitRMB];
    }else{
        content5.swSlider.value = 0;
        content5.currentNumberLabel.text = @"0";
    }
    [self addViewObject:content5 withHeight:height withWidth:width withAtIndex:4];
    
    
    
}

- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
    
    NSString *title;
    UIButton *cusButton;
    
    switch (tagTag) {
        case KQuestion1:
            cusButton = _btnCostForEach;
            title = [NSNumber numberWithInt:value].stringValue;
                title = [title stringByAppendingString:@"万"];
            break;

        case KQuestion2:
            _Qhouse.selectIndex = index;
            cusButton = _btnHaveHouse;
            switch (index) {
                case 1:
                    title = @"没有" ;
                    break;
                case 2:
                    title = @"有";
                    break;
                default:
                    break;
            }
            break;
        case KQuestion3:
            _Qcar.selectIndex = index;
            cusButton = _btnHaveCar;
            switch (index) {
                case 1:
                    title = @"没有" ;
                    break;
                case 2:
                    title = @"有";
                    break;
                default:
                    break;
            }
            break;
        case KQuestion4:
            cusButton = _btnAsset;
            title = [NSNumber numberWithInt:value].stringValue;
                title = [title stringByAppendingString:@"万"];
            break;
            
        case KQuestion5:
            cusButton = _btnLoans;
            title = [NSNumber numberWithInt:value].stringValue;
                title = [title stringByAppendingString:@"万"];
            break;
            
        default:
            break;
    }
    
    [self myCustomButton:cusButton withTitle:title];
}
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}

@end
