//
//  QuestionWithInsurance.h
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionWithInsurance : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *tabScroll;

@property (strong, nonatomic) IBOutlet UIButton *btnTotal;

@property (strong, nonatomic) IBOutlet UIButton *btnEndowment;

@property (strong, nonatomic) IBOutlet UIButton *btnLife;

@property (strong, nonatomic) IBOutlet UIButton *btnDisease;

- (IBAction)actionWithButton:(UIButton *)sender;



@end
