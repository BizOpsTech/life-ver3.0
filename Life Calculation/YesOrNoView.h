//
//  YesOrNoView.h
//  Life Calculation
//
//  Created by Snow on 9/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YesOrNoViewDelegate;
@interface YesOrNoView : UIView{
    
    __weak IBOutlet UIImageView *rightBoxImageView;
    __weak IBOutlet UIImageView *leftBoxImageview;
    __weak IBOutlet UIButton *leftButton;
    __weak IBOutlet UIButton *rightButton;
    

    UIImage *nikeImage;
    

    
}

@property (nonatomic, assign) id<YesOrNoViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (nonatomic, getter = isON) BOOL on;




@end


@protocol YesOrNoViewDelegate <NSObject>

@optional
- (void)selectedButtonState:(BOOL)state;

@end