//
//  SWContentStyle2.m
//  Life_20131116
//
//  Created by Snow on 11/18/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import "SWContentStyle2.h"

@implementation SWContentStyle2

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"SWContentStyle2" owner:self options:nil] lastObject];
    
    if (self) {
        self.rgView.delegate =self;
    }
    
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    _leftNameLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    _middleNameLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
    _rightNameLabel.font = [UIFont fontWithName:@"HelveticaNeueLTPro-Cn" size:14];
}


-(void)selectedButtonStateWithRadio:(NSUInteger)index{
    
    UIImage *isImage = _images[index - 1];
    if (isImage) {
        self.imageView.image = _images[index-1];
    }
    [self.delegate changeValueDelegateWithObject:self withSelectedhRadio:index];
    
}

@end
