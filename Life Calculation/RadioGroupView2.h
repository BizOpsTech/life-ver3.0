//
//  RadioGroupView2.h
//  Life_20131116
//
//  Created by Snow on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioGroupView.h"


@protocol RadioGroupViewDelegate;
@interface RadioGroupView2 : UIView{
    
    __weak IBOutlet UIImageView *leftBoxImageView;
    __weak IBOutlet UIImageView *rigthBoxImageView;
    
    __weak IBOutlet UIButton *leftButton;
    __weak IBOutlet UIButton *rightButton;
    
    UIImage *nikeImage;
}

@property(nonatomic,assign)id<RadioGroupViewDelegate>delegate;
@property (strong, nonatomic) IBOutlet UILabel *leftNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (nonatomic,getter=didSelectIndex)NSUInteger selectIndex;

- (void)selectedButtonAtIndex:(NSUInteger)index;

@end
