//
//  QuestionWithFamily.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithFamily.h"
#import "SWContentView.h"
#import "SWContentStyle2.h"

@interface QuestionWithFamily ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;
@property(nonatomic,strong)AppDelegate *app;
@property (nonatomic, strong) UILabel *testLabel;
@property(nonatomic,strong)Question *Qschool;
@end

@implementation QuestionWithFamily



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _app = [[UIApplication sharedApplication]delegate];
     _Qschool = [Question new];
    _btnArr = [NSArray arrayWithObjects:_btnOfKids,_btnOfSeniors,_btnOfSpending,_btnOfSchool,_btnOfYear,nil];
    
    
    #pragma mark 填充用户内容
    [self setValueToPage];
	// Do any additional setup after loading the view.
    
   
    [self addCustomViewOnScroll];
    
    [_app autoFixedForButton:_btnArr];
    [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionWithButton:(UIButton *)sender {
    
    int page = 0;
    if ([sender isEqual:_btnOfKids]) {
        //孩纸 个数选项控件
        page = 0;
       // [self changeBackgroundForButton:YES ButtonIndex:0];
    }else if([sender isEqual:_btnOfSeniors]){
        //长者 个数选项控件
        page = 1;
      //  [self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnOfSpending]){
        //每月的消费控件
        page = 2;
       // [self changeBackgroundForButton:YES ButtonIndex:2];
        
        
    }else if([sender isEqual:_btnOfSchool]){
        //长者 个数选项控件
        page = 3;
       // [self changeBackgroundForButton:YES ButtonIndex:3];
        
    }else if([sender isEqual:_btnOfYear]){
        //年数 选项
        page = 4;
        //[self changeBackgroundForButton:YES ButtonIndex:4];
        
    }else{
        return;
    }
    
    
    CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
    
}

#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    UIColor *borderColor;
    
    if(isEditStatu){
        borderColor = [UIColor colorWithRed:0/255.f green:204/255.f blue:255/255.f alpha:1.0];
        
        ////// 修改按钮样式 ///////////////////////////////////////////////////////////////////
        
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 2.0;
        btn.layer.borderColor = borderColor.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@"buttonBG"] forState:UIControlStateNormal];
        
        ////// 取消其他按钮样式 //////////////////////////////////////////////////////////////////
        [self didEditCompleteFromButton:btnIndex];
        
        
    }else{
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        btn.layer.borderWidth = 0;
    }
}

#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{

    //_btnOfKids,_btnOfSeniors,_btnOfSpending,_btnOfSchool,_btnOfYear
    if(_app.recodDb.family.Kids >0){
        [self setValueToButton:0 componentVal:[NSString stringWithFormat:@"%d",_app.recodDb.family.Kids]];
    }
    if(_app.recodDb.family.Elders >0){
        [self setValueToButton:1 componentVal:[NSString stringWithFormat:@"%d",_app.recodDb.family.Elders]];
    }
    
    if(_app.recodDb.family.RaiseCosts >0){
        [self setValueToButton:2 componentVal:[NSString stringWithFormat:@"%d万",_app.recodDb.family.RaiseCosts/UnitRMB ]];
    }
    
    if(_app.recodDb.SchoolType.selectIndex > 0){
        _Qschool.selectIndex = _app.recodDb.SchoolType.selectIndex;
        NSString *strType = _app.recodDb.SchoolType.answer;
        [self setValueToButton:3 componentVal:strType];
    }
    if(_app.recodDb.family.attendSchoolInYears >0){
        
        [self setValueToButton:4 componentVal:[NSString stringWithFormat:@"%d",_app.recodDb.family.attendSchoolInYears]];
    }
    
}


/*
 @property(nonatomic,assign)userFamily family;
 @property(nonatomic,copy)NSString *SchoolType;
 typedef struct {
 int Kids;       //养几个小孩
 int Elders;     //赡养几个老人
 int RaiseCosts; //赡养（抚养）费用
 int attendSchoolInYears;  //在指定类型学校读几年
 }userFamily;
 */
#pragma mark 记录选项值
-(void)getValueFromButton{
    userFamily uf;
    uf.Kids = [_btnOfKids.titleLabel.text intValue];
    uf.Elders = [_btnOfSeniors.titleLabel.text intValue];
    uf.RaiseCosts = [_btnOfSpending.titleLabel.text intValue] * UnitRMB;
    uf.attendSchoolInYears = [_btnOfYear.titleLabel.text intValue];
    _app.recodDb.family = uf;
    
    _Qschool.answer = _btnOfSchool.titleLabel.text;
    _app.recodDb.SchoolType = _Qschool;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
}




#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    
}

- (void)addCustomViewOnScroll{
    
    self.tabScroll.pagingEnabled = YES;
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    
    self.tabScroll.contentSize = CGSizeMake(width*5, height);
    
    SWContentView *content1 = [[SWContentView alloc] init];
    content1.delegate = self;
    content1.tag = KQuestion1;
    content1.imageView.image = [UIImage imageNamed:@"kids"];
    content1.images = @[[UIImage imageNamed:@"kids"]];
    content1.swSlider.minimumValue = 0;
    content1.swSlider.maximumValue = 5;
    content1.minLabel.text = @"0";
    content1.maxLabel.text = @"5";
    content1.minSubLabel.hidden=YES;
    if(_app.recodDb.family.Kids >0){
        content1.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.family.Kids];
        content1.swSlider.value = (float)_app.recodDb.family.Kids;
    }else{
        content1.currentNumberLabel.text = @"0";
        content1.swSlider.value = 0;
    }

    
    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    SWContentView *content2 = [[SWContentView alloc] init];
    content2.delegate = self;
    content2.tag = KQuestion2;
    content2.imageView.image = [UIImage imageNamed:@"oldmen"];
    content2.images = @[[UIImage imageNamed:@"oldmen"]];
    content2.swSlider.minimumValue = 0;
    content2.swSlider.maximumValue = 5;
    content2.minLabel.text = @"0";
    content2.maxLabel.text = @"5";
    content2.minSubLabel.hidden=YES;
    if(_app.recodDb.family.Elders >0){
        content2.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.family.Elders];
        content2.swSlider.value = (float)_app.recodDb.family.Elders;
    }else{
        content2.currentNumberLabel.text = @"0";
        content2.swSlider.value = 0;
    }

    
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    SWContentView *content3 = [[SWContentView alloc] init];
    content3.tag = KQuestion3;
    content3.delegate = self;
    content3.imageView.image = ImageName(@"month_expense");
    content3.images = @[ImageName(@"month_expense")];
    content3.swSlider.tag = 727;
    content3.swSlider.minimumValue = 0;
    content3.swSlider.maximumValue = 5;
    content3.minLabel.text = @"0";
    content3.maxLabel.text = @"5万";
    content3.minSubLabel.hidden=YES;
    if(_app.recodDb.family.RaiseCosts >0){
        content3.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.family.RaiseCosts/UnitRMB];
        content3.swSlider.value = (float)_app.recodDb.family.RaiseCosts/UnitRMB;
    }else{
        content3.currentNumberLabel.text = @"0";
        content3.swSlider.value = 0;
    }

    
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
    
    
    SWContentStyle2 *content4 = [[SWContentStyle2 alloc] init];
    content4.delegate = self;
    content4.tag = KQuestion4;
    content4.images = [NSArray arrayWithObjects:ImageName(@"domestic"),ImageName(@"oversea"),ImageName(@"unknow"), nil];
    if(_app.recodDb.SchoolType.selectIndex >0){
        //content4.imageView.image = content4.images[_app.recodDb.SchoolType.selectIndex-1];
        [content4.rgView selectedButtonAtIndex:_app.recodDb.SchoolType.selectIndex];
    }else{
        content4.imageView.image = [UIImage imageNamed:@"domestic"];
    }
    content4.leftNameLabel.text = @"国内";
    content4.middleNameLabel.text = @"国外";
    content4.rightNameLabel.text = @"不清楚";
    [self addViewObject:content4 withHeight:height withWidth:width withAtIndex:3];
    
    SWContentView *content5 = [[SWContentView alloc] init];
    content5.delegate = self;
    content5.tag = KQuestion5;
    content5.imageView.image = [UIImage imageNamed:@"calendar"];
    content5.images = [NSArray arrayWithObject:ImageName(@"calendar")];
    content5.swSlider.minimumValue = 0;
    content5.swSlider.maximumValue = 18;
    content5.minLabel.text = @"0";
    content5.maxLabel.text = @"18";
    content5.minSubLabel.hidden=YES;
    if(_app.recodDb.family.attendSchoolInYears >0){
        content5.swSlider.value = (float)_app.recodDb.family.attendSchoolInYears;
        content5.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.family.attendSchoolInYears];
    }else{
        content5.swSlider.value = 0;
        content5.currentNumberLabel.text = @"0";
    }

    [self addViewObject:content5 withHeight:height withWidth:width withAtIndex:4];
    
    
}

- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
    
    NSString *title;
    UIButton *cusButton;
    
    switch (tagTag) {
        case KQuestion1:
            cusButton = _btnOfKids;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
            
        case KQuestion2:
            cusButton = _btnOfSeniors;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
            
        case KQuestion3:
            cusButton = _btnOfSpending;
            title = [NSNumber numberWithInt:value].stringValue;
            title = [title stringByAppendingString:@"万"];
            break;
            
        case KQuestion4:
            _Qschool.selectIndex = index;
            cusButton = _btnOfSchool;
            switch (index) {
                case 1:
                    title = @"国内";
                    break;
                case 2:
                    title = @"国外" ;
                    break;
                case 3:
                    title = @"不清楚";
                    break;
                default:
                    break;
            }
            break;
        case KQuestion5:
            cusButton = _btnOfYear;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        default:
            break;
    }
    
    [self myCustomButton:cusButton withTitle:title];
}
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}


@end
