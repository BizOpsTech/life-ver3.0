//
//  UICommon.h
//  TestDemo
//
//  Created by BizOpsTech on 11/26/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIStyle.h"
@interface UICommon : NSObject
/*
////////// 描边 UITextField
 NSDictionary *style 
 */
#pragma mark
-(void)customTextField:(UITextField *)textField textStyle:(UIStyle *)style;
-(void)customButton:(UIButton *)button buttonStyle:(UIStyle *)style isReset:(BOOL)update;
-(void)gradientConfig:(UIView *)sender ColorArr:(NSArray*)colorArr;
-(void)gradientUpdate:(UIView *)sender ColorArr:(NSArray*)colorArr;
@end
