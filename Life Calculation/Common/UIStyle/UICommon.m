//
//  UICommon.m
//  TestDemo
//
//  Created by BizOpsTech on 11/26/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
//#import <QuartzCore/QuartzCore.h>
//#import <Quartzcore/CALayer.h>

#import "UICommon.h"
@implementation UICommon
-(void)setCommonAttr:(id)sender Style:(UIStyle *)style{
    
    float borderWidth = 1.0;
    float cornerRadius = 4.0;
    UIColor *borderColor = [UIColor lightGrayColor];
    UIColor *backgroundColor = [UIColor whiteColor];
    
    if(style.borderWidth > 1){
        borderWidth = style.borderWidth;
    }
    if(style.cornerRadius > 1){
        cornerRadius = style.cornerRadius;
    }
    if(![style.borderColor isEqual:nil]){
        borderColor = style.borderColor;
        
    }
    if(![style.backgroundColor isEqual:nil]){
        backgroundColor = style.backgroundColor;
    }
    
    UIView *UI = (UIView *)sender;
    //背景色
    UI.backgroundColor = backgroundColor;
    //圆角
    UI.layer.cornerRadius = cornerRadius;
    //边框宽度
    UI.layer.borderWidth = borderWidth;
    //边框颜色
    UI.layer.borderColor = borderColor.CGColor;
    //溢出剪除
    UI.clipsToBounds = YES;
}

#pragma mark 绘制渐变背景
-(void)gradientConfig:(UIView *)sender ColorArr:(NSArray*)colorArr{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, sender.frame.size.width, sender.frame.size.height);
    gradient.colors = colorArr;
    [sender.layer insertSublayer:gradient atIndex:0];
}

-(void)gradientUpdate:(UIView *)sender ColorArr:(NSArray*)colorArr{
    CAGradientLayer *layer = [sender.layer.sublayers objectAtIndex:0];
    layer.colors = colorArr;
}

#pragma mark 自定义文本框样式
-(void)customTextField:(UITextField *)textField textStyle:(UIStyle *)style{
    CGRect imgCGRect =  CGRectMake(0, 5, 20, 20);
    UIImageView *viewForLeft,*viewForRigth;
    CGRect paddingCGRect =  CGRectMake(0, 0, 5, 30);
    UIView *paddingForView = [[UIView alloc]initWithFrame:paddingCGRect];
    BOOL isIconForLeft = NO;
    BOOL  isIconForRight = NO;
    
    if(style != nil){
        if(![style.iconNameForTextLeft isEqual:nil] && style.iconNameForTextLeft.length > 0){
            viewForLeft = [[UIImageView alloc]initWithFrame:imgCGRect];
            [viewForLeft setImage:[UIImage imageNamed:style.iconNameForTextLeft]];
            isIconForLeft = YES;
        }
        if(![style.iconNameForTextRight isEqual:nil] && style.iconNameForTextRight.length > 0){
            viewForRigth = [[UIImageView alloc]initWithFrame:imgCGRect];
            [viewForRigth setImage:[UIImage imageNamed:style.iconNameForTextRight]];
            isIconForRight = YES;
        }
        [self setCommonAttr:textField Style:style];
        
    }
    textField.borderStyle = UITextBorderStyleLine;
    textField.leftView = isIconForLeft ? viewForLeft : paddingForView;
    textField.rightView = isIconForRight ? viewForRigth : paddingForView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark 自定义按钮样式
-(void)customButton:(UIButton *)button buttonStyle:(UIStyle *)style isReset:(BOOL)update{
    [self setCommonAttr:button Style:style];
    if(style.gradientColorArr != nil){
        if(update){
            [self gradientUpdate:button ColorArr:style.gradientColorArr];
        }else{
            [self gradientConfig:button ColorArr:style.gradientColorArr];
        }
    }
    
}
@end
