//
//  UIStyle.h
//  TestDemo
//
//  Created by BizOpsTech on 12/2/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIStyle : NSObject
/////// uitextfield
@property(nonatomic,copy)NSString *iconNameForTextLeft;
@property(nonatomic,copy)NSString *iconNameForTextRight;


//////// 通用属性
@property(nonatomic,assign)float borderWidth;
@property(nonatomic,assign)float cornerRadius;
@property(nonatomic,retain)UIColor *borderColor;
@property(nonatomic,retain)UIColor *backgroundColor;
@property(nonatomic,retain)NSArray *gradientColorArr;

@end
