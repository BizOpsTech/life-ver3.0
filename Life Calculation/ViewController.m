//
//  ViewController.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/16/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
//
#import "AppDelegate.h"
#import "UIDevice+Resolutions.h"
#import "ViewController.h"
@interface ViewController ()
@property(nonatomic,strong)AppDelegate *app;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _app = [[UIApplication sharedApplication]delegate];
    
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden =YES;
    
//    if(![UIDevice isRunningOniPhone5]){
//        UIImageView  *wallpaperIOS7 = (UIImageView *)[self.view viewWithTag:200];
//        [wallpaperIOS7 removeFromSuperview];
//        
//        UIImageView *wallPaperIOS6 = [[UIImageView alloc]initWithFrame:CGRectMake(0, -44, 320, 568)];
//        [wallPaperIOS6 setImage:[UIImage imageNamed:@"start"]];
//        [self.view addSubview:wallPaperIOS6];
//        [self.view sendSubviewToBack:wallPaperIOS6];
//    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    int surveyIndex;
    if([segue.identifier isEqualToString:@"goto2M"]){
        surveyIndex = 2;
    }else{
        surveyIndex = 7;
    }
    
    if(_app.surveyIndex != surveyIndex){
        _app.recodDb = nil;
        _app.recodDb = [UserDb new];
        _app.surveyIndex = surveyIndex;
    }
    
}
@end
