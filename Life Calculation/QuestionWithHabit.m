//
//  QuestionWithHabit.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithHabit.h"
#import "SWContentStyle1.h"
#import "SWContentStyle2.h"

@interface QuestionWithHabit ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;

@property(nonatomic,strong)AppDelegate *app;
@property(nonatomic,strong)Question *Qtravel;
@property(nonatomic,strong)Question *Qexercise;
@property(nonatomic,strong)Question *QisSmoke;
@property(nonatomic,strong)Question *QisIllness;
@end

@implementation QuestionWithHabit


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _app = [[UIApplication sharedApplication]delegate];
    _Qtravel = [Question new];
    _Qexercise = [Question new];
    _QisSmoke = [Question new];
    _QisIllness = [Question new];
    _btnArr = [NSArray arrayWithObjects:_btnFrequency,_btnMany,_btnIsSmoke,_btnIsHasIllness,nil];

    #pragma mark 填充用户内容
    [self setValueToPage];
    
	// Do any additional setup after loading the view.

    [self addCustomViewOnScroll];
    
    [_app autoFixedForButton:_btnArr];
    [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 按钮动作触发
- (IBAction)actionWithButton:(UIButton *)sender {
    
    
    int page = 0;
    
    //@_btnFrequency,_btnMany,_btnIsSmoke,_btnIsHasIllness
    if([sender isEqual:_btnFrequency]){
        //加载出差选项 [very often,normal,not very often]
        page = 0;
      //  [self changeBackgroundForButton:YES ButtonIndex:0];
        
    }else if([sender isEqual:_btnMany]){
        //加载锻炼强度选项 [0-2 times,3-4 times,5 times]
        page = 1;
     //   [self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnIsSmoke]){
        //加载是否吸烟选项 [I am,I am not]
        page = 2;
     //   [self changeBackgroundForButton:YES ButtonIndex:2];
        
    }else if([sender isEqual:_btnIsHasIllness]){
        //加载是否有家族病史选项[I have,I don't have]
        page = 3;
     //   [self changeBackgroundForButton:YES ButtonIndex:3];
        
    }else{
        return;
    }
    
    
    CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
    
}


#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    UIColor *borderColor;
    
    if(isEditStatu){
        borderColor = [UIColor colorWithRed:0/255.f green:204/255.f blue:255/255.f alpha:1.0];
        
        ////// 修改按钮样式 ///////////////////////////////////////////////////////////////////
        
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 2.0;
        btn.layer.borderColor = borderColor.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@"buttonBG"] forState:UIControlStateNormal];
        
        ////// 取消其他按钮样式 //////////////////////////////////////////////////////////////////
        [self didEditCompleteFromButton:btnIndex];
        
        
    }else{
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        btn.layer.borderWidth = 0;
    }
}


#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{

    if(_app.recodDb.travel.selectIndex >0){
        _Qtravel.selectIndex = _app.recodDb.travel.selectIndex;
        [self setValueToButton:0 componentVal:_app.recodDb.travel.answer];
    }
    if(_app.recodDb.exercise.selectIndex >0){
        _Qexercise.selectIndex = _app.recodDb.exercise.selectIndex;
        [self setValueToButton:1 componentVal:_app.recodDb.exercise.answer];
    }
    if(_app.recodDb.isSmoke.selectIndex >0){
        _QisSmoke.selectIndex = _app.recodDb.isSmoke.selectIndex;
        [self setValueToButton:2 componentVal:_app.recodDb.isSmoke.answer];
    }
    if(_app.recodDb.isIllness.selectIndex >0){
        _QisIllness.selectIndex = _app.recodDb.isIllness.selectIndex;
        [self setValueToButton:3 componentVal:_app.recodDb.isIllness.answer];
    }
}

#pragma mark 记录选项值
/*
 //Page2 for habit
 出差频率
 @property(nonatomic,assign)Question * travel;
 
 锻炼次数
 @property(nonatomic,assign)Question *exercise;
 
 是否吸烟
 @property(nonatomic,assign)Question *isSmoke;
 
 是否有家族病史
 @property(nonatomic,assign)Question *isIllness;
 */
-(void)getValueFromButton{
    _Qtravel.answer = _btnFrequency.titleLabel.text;
    _Qexercise.answer = _btnMany.titleLabel.text;
    _QisSmoke.answer = _btnIsSmoke.titleLabel.text;
    _QisIllness.answer = _btnIsHasIllness.titleLabel.text;
    
    _app.recodDb.travel = _Qtravel;
    _app.recodDb.exercise = _Qexercise;
    _app.recodDb.isSmoke = _QisSmoke;
    _app.recodDb.isIllness = _QisIllness;
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
}






#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    
}

- (void)addCustomViewOnScroll{
    self.tabScroll.pagingEnabled = YES;
    
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    
    self.tabScroll.contentSize = CGSizeMake(width*4, height);
    
    SWContentStyle2 *content1 = [[SWContentStyle2 alloc] init];
    content1.delegate =self;
    content1.tag = KQuestion1;
    content1.leftNameLabel.text = @"经常";
    content1.middleNameLabel.text = @"一般";
    content1.rightNameLabel.text = @"很少";
    NSArray *allImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"travel_often"],[UIImage imageNamed:@"travel_normal"],[UIImage imageNamed:@"travel_notoften"], nil];
    content1.images = allImages;
    if(_app.recodDb.travel.selectIndex >0){
        //content1.imageView.image = allImages[_app.recodDb.travel.selectIndex-1];
        [content1.rgView selectedButtonAtIndex:_app.recodDb.travel.selectIndex];
        
    }else{
        content1.imageView.image = allImages[0];
    }
    
    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    SWContentStyle2 *content2 = [[SWContentStyle2 alloc] init];
    content2.tag = KQuestion2;
    content2.delegate = self;
    content2.leftNameLabel.text = @"0-2次";
    content2.middleNameLabel.text = @"3-4次";
    content2.rightNameLabel.text = @"5次以上";
    NSArray *allImages2 = [NSArray arrayWithObjects:[UIImage imageNamed:@"exercise0_2"],[UIImage imageNamed:@"exercise3_4"],[UIImage imageNamed:@"exercise5"], nil];
    content2.images = allImages2;
    if(_app.recodDb.exercise.selectIndex >0){
        //content2.imageView.image = allImages2[_app.recodDb.exercise.selectIndex-1];
        [content2.rgView selectedButtonAtIndex:_app.recodDb.exercise.selectIndex];
    }else{
        content2.imageView.image = allImages2[0];
    }
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    SWContentStyle1 *content3 = [[SWContentStyle1 alloc] init];
    content3.tag = KQuestion3;
    content3.delegate = self;
    content3.leftNameLabel.text = @"不吸烟";
    content3.rightNameLabel.text = @"吸烟";
    NSArray *allImage3 = @[[UIImage imageNamed:@"nosmoke"],[UIImage imageNamed:@"yessmoke"]];
    content3.images = allImage3;
    if(_app.recodDb.isSmoke.selectIndex >0){
        //content3.imageView.image = allImage3[_app.recodDb.isSmoke.selectIndex-1];
        [content3.rgView2 selectedButtonAtIndex:_app.recodDb.isSmoke.selectIndex];
    }else{
        content3.imageView.image = allImage3[0];
    }
    
    
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
    
    SWContentStyle1 *content4 = [[SWContentStyle1 alloc] init];
    content4.tag = KQuestion4;
    content4.delegate = self;
    content4.leftNameLabel.text = @"没有";
    content4.rightNameLabel.text = @"有";
    NSArray *allImage4 = @[[UIImage imageNamed:@"illness_no"],[UIImage imageNamed:@"illness_yes"]];
    content4.images = allImage4;
    if(_app.recodDb.isIllness.selectIndex >0){
        //content4.imageView.image = allImage4[_app.recodDb.isIllness.selectIndex-1];
        [content4.rgView2 selectedButtonAtIndex:_app.recodDb.isIllness.selectIndex];
        
    }else{
        content4.imageView.image = allImage4[0];
    }
    
    [self addViewObject:content4 withHeight:height withWidth:width withAtIndex:3];
    
    
}


- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
    
    NSString *title;
    UIButton *cusButton;
    
    switch (tagTag) {
        case KQuestion1:
            _Qtravel.selectIndex = index;
            cusButton = _btnFrequency;
            switch (index) {
                case 1:
                    title = @"经常";
                    break;
                case 2:
                    title = @"一般";
                    break;
                case 3:
                    title = @"很少";
                    break;
                default:
                    break;
            }
            break;
            
        case KQuestion2:
            _Qexercise.selectIndex = index;
            cusButton = _btnMany;
            switch (index) {
                case 1:
                    title = @"0-2次";
                    break;
                case 2:
                    title = @"3-4次";
                    break;
                case 3:
                    title = @"5次以上";
                    break;
                default:
                    break;
            }
            break;
            
        case KQuestion3:
            _QisSmoke.selectIndex = index;
            cusButton = _btnIsSmoke;
            switch (index) {
                case 1:
                    title = @"不吸烟";
                    break;
                case 2:
                    title = @"吸烟";
                    break;
                default:
                    break;
            }
            break;
            
        case KQuestion4:
            _QisIllness.selectIndex = index;
            cusButton = _btnIsHasIllness;
            switch (index) {
                case 1:
                    title = @"没有";
                    break;
                case 2:
                    title = @"有";
                    break;
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
    
    [self myCustomButton:cusButton withTitle:title];
}
- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}


@end
