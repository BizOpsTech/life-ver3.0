//
//  QuestionWithAbout.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/17/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//
#import "AppDelegate.h"
#import "QuestionWithAbout.h"

#import "SWContentStyle1.h"
#import "SWContentStyle2.h"
#import "SWContentView.h"
#import "FSTWork.h"

@interface QuestionWithAbout ()<UIScrollViewDelegate,ChangeValueDelegate>
@property(nonatomic,strong)NSArray *btnArr;
@property(nonatomic,strong)AppDelegate *app;

@property(nonatomic,strong)Question *Qgender;
@property(nonatomic,strong)Question *Qmarried;
@property(nonatomic,strong)Question *Qoccupation;


- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index;
- (void)addCustomViewOnScroll;

@end

@implementation QuestionWithAbout




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)cancelAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithTitle:@"首页" style:UIBarButtonItemStyleDone target:self action:@selector(cancelAction:)];
    self.navigationItem.leftBarButtonItem = leftBar;
    
    /////////////////////////////////////////
     _btnArr = [NSArray arrayWithObjects:_btnAge,_btnGender,_btnMarried,_btnKids,_btnOccupation, nil];
    _app = [[UIApplication sharedApplication]delegate];
    _Qgender = [Question new];
    _Qmarried = [Question new];
    _Qoccupation = [Question new];
    

    
    #pragma mark 填充用户内容
    [self setValueToPage];
    
	// Do any additional setup after loading the view.
    [_app autoFixedForButton:_btnArr];
    [_app fixedConstraintForIOS7:_tabScroll SuperView:self.view];


    [self addCustomViewOnScroll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark 按钮动作
- (IBAction)ActionWithButton:(UIButton *)sender {
    //snowControll * controll = [snowControll new];
    //[self.tabScroll addSubview:controll];
    
    int page = 0;
    if ([sender isEqual:_btnAge]) {
        //载入 年龄控件
        page = 0;
        
        //[self changeBackgroundForButton:YES ButtonIndex:0];
        
    }else if([sender isEqual:_btnGender]){
        //载入 性别控件
        page = 1;
        //[self changeBackgroundForButton:YES ButtonIndex:1];
        
    }else if([sender isEqual:_btnMarried]){
        //载入 婚姻控件
        page = 2;
        //[self changeBackgroundForButton:YES ButtonIndex:2];
        
    }else if([sender isEqual:_btnKids]){
        //载入 选择孩子个数控件
        page = 3;
        //[self changeBackgroundForButton:YES ButtonIndex:3];
        
    }else if([sender isEqual:_btnOccupation]){
        //载入 职位选择控件
        page = 4;
        //[self changeBackgroundForButton:YES ButtonIndex:4];
        
    }else{
        if (page == 0) {
            return;
        }
    }
    
    
	CGRect frame = self.tabScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.tabScroll scrollRectToVisible:frame animated:NO];
    
}

#pragma mark 取消当前以外的按钮背景
-(void)didEditCompleteFromButton:(NSUInteger)exceptionIndex{
    for(NSUInteger i=0; i< _btnArr.count; i++){
        if(i != exceptionIndex){
            [self changeBackgroundForButton:NO ButtonIndex:i];
        }
    }
}

#pragma mark 修改按钮背景
-(void)changeBackgroundForButton:(BOOL)isEditStatu ButtonIndex:(NSUInteger)btnIndex{
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //UIColor *borderColor;
    
    if(isEditStatu){
        UIImageView *iconView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"iconEdit"]];
        iconView.frame = CGRectMake(btn.superview.frame.size.width -17, 3, 17, 17);
        iconView.tag = btnIndex+8 ;
        [btn.superview addSubview:iconView];
        [btn.superview bringSubviewToFront:btn];
        [self didEditCompleteFromButton:btnIndex];
        
    }else{
        
        [[btn.superview viewWithTag:btnIndex+8] removeFromSuperview];
    }
}

#pragma mark 给按钮赋值
-(void)setValueToButton:(NSUInteger)btnIndex componentVal:(NSString *)value{
    UIColor *FontColor = [UIColor colorWithRed:78/255.f green:108/255.f blue:249/255.f alpha:1.0];
    UIButton *btn = [_btnArr objectAtIndex:btnIndex];
    //btn.titleLabel.font = _app.myFont;
    [btn setTitle:value forState:UIControlStateNormal];
    [btn setTitleColor:FontColor forState:UIControlStateNormal];
}

/*
 @_btnAge,_btnGender,_btnMarried,_btnKids,_btnOccupation
 */
#pragma mark 使用recordDb给页面元素赋值
-(void)setValueToPage{
    if(_app.recodDb.age > 0){
        NSString *strAge = [NSString stringWithFormat:@"%d",_app.recodDb.age];
        [self setValueToButton:0 componentVal:strAge];
    }
    
    if(_app.recodDb.gender.selectIndex >0){
        _Qgender.selectIndex =_app.recodDb.gender.selectIndex;
        NSString *strGender = _app.recodDb.gender.answer;
        [self setValueToButton:1 componentVal:strGender];
    }
    
    if(_app.recodDb.isMarried.selectIndex >0){
        _Qmarried.selectIndex = _app.recodDb.isMarried.selectIndex;
        NSString *strMarried = _app.recodDb.isMarried.answer;
        [self setValueToButton:2 componentVal:strMarried];
    }
    
    if(_app.recodDb.kids >0){
        NSString *strKids = [NSString stringWithFormat:@"%ld",(long)_app.recodDb.kids];
        [self setValueToButton:3 componentVal:strKids];
    }
    
    if(_app.recodDb.occupation.selectIndex >0){
        _Qoccupation.selectIndex = _app.recodDb.occupation.selectIndex;
        NSString *strOccupation = _app.recodDb.occupation.answer;
       [self setValueToButton:4 componentVal:strOccupation];
    }
}


#pragma mark 记录选项值
/*
 @property(nonatomic,assign)int age;            //年龄
 @property(nonatomic,copy)NSString *gender;     //性别
 @property(nonatomic,assign)NSInteger isMarried;     //婚姻状态
 @property(nonatomic,assign)NSUInteger kids;    //小孩个数
 @property(nonatomic,assign)NSUInteger occupation;//职位
 */
-(void)getValueFromButton{
    _Qgender.answer = _btnGender.titleLabel.text;
    _Qmarried.answer = _btnMarried.titleLabel.text;
    _Qoccupation.answer = _btnOccupation.titleLabel.text;
    
    _app.recodDb.age = [_btnAge.titleLabel.text intValue];
    _app.recodDb.kids = [_btnKids.titleLabel.text intValue];
    
    _app.recodDb.gender = _Qgender;
    _app.recodDb.isMarried = _Qmarried;
    _app.recodDb.occupation = _Qoccupation;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self getValueFromButton];
    
}


#pragma mark    - Add SubView on

- (void)addViewObject:(UIView*)myView withHeight:(float)height withWidth:(float)width withAtIndex:(NSUInteger)index{
    
    myView.frame = CGRectMake(index*320, 0, width, height);
    [self.tabScroll addSubview:myView];
    
}

- (void)addCustomViewOnScroll{
    self.tabScroll.pagingEnabled = YES;
    
    self.tabScroll.showsHorizontalScrollIndicator = NO;
    self.tabScroll.showsVerticalScrollIndicator = NO;
    self.tabScroll.delegate = self;
    self.tabScroll.bounces = NO;
    self.tabScroll.scrollEnabled = NO;
    //    self.tabScroll.userInteractionEnabled = NO;
    
    
    float height = CGRectGetHeight(self.tabScroll.bounds);
    float width = CGRectGetWidth(self.tabScroll.bounds);
    self.tabScroll.contentSize = CGSizeMake(width*5, height);
    
    ////////// 问题1
    SWContentView *content1 = [[SWContentView alloc] init];
    content1.images = @[[UIImage imageNamed:@"age1"],[UIImage imageNamed:@"age2"],ImageName(@"age3")];
    content1.imageView.image = ImageName(@"age1");
    content1.tag = KQuestion1;
    content1.swSlider.tag = 725;
    content1.swSlider.minimumValue = 18;
    content1.swSlider.maximumValue = 60;
    content1.swSlider.value = 18;
    content1.minLabel.text = @"18";
    content1.maxLabel.text = @"60";
    if(_app.recodDb.age > 0){
        [content1.swSlider setValue:(float)_app.recodDb.age animated:YES];
        content1.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.age];
    }else{
        content1.currentNumberLabel.text = @"18";
    }
    content1.delegate = self;
    [self addViewObject:content1 withHeight:height withWidth:width withAtIndex:0];
    
    
    ////////// 问题2
    SWContentStyle1 *content2 = [[SWContentStyle1 alloc] init];
    content2.images = @[[UIImage imageNamed:@"woman_1"],[UIImage imageNamed:@"man_1"]];
    if(_app.recodDb.gender.selectIndex >0){
        //content2.imageView.image = content2.images[_app.recodDb.gender.selectIndex-1];
        [content2.rgView2 selectedButtonAtIndex:_app.recodDb.gender.selectIndex];
    }else{
        content2.imageView.image = ImageName(@"woman_1");
    }
    content2.tag = KQuestion2;
    content2.delegate = self;
    content2.leftNameLabel.text = @"女性";
    content2.rightNameLabel.text = @"男性";
    [self addViewObject:content2 withHeight:height withWidth:width withAtIndex:1];
    
    
    ////////// 问题3
    SWContentStyle2 *content3 = [[SWContentStyle2 alloc] init];
    content3.tag = KQuestion3;
    content3.leftNameLabel.text = @"已婚";
    content3.middleNameLabel.text = @"未婚";
    content3.rightNameLabel.text = @"其他";
    NSArray *allImages = @[[UIImage imageNamed:@"married"],[UIImage imageNamed:@"unmarried"],[UIImage imageNamed:@"others"]];
    content3.images = allImages;
    if(_app.recodDb.isMarried.selectIndex >0){
        content3.imageView.image = allImages[_app.recodDb.isMarried.selectIndex-1];
        [content3.rgView selectedButtonAtIndex:_app.recodDb.isMarried.selectIndex];
    }else{
        content3.imageView.image = allImages[0];
    }
    content3.delegate = self;
    [self addViewObject:content3 withHeight:height withWidth:width withAtIndex:2];
    
    
    ////////// 问题4
    SWContentView *content4 = [[SWContentView alloc] init];
    content4.imageView.image = ImageName(@"kids");
    content4.images = @[ImageName(@"kids")];
    content4.tag = KQuestion4;
    content4.swSlider.minimumValue = 0;
    content4.swSlider.maximumValue = 6;
    content4.minLabel.text = @"0";
    content4.maxLabel.text = @"6";
    content4.minSubLabel.hidden = YES;
    if(_app.recodDb.kids >0){
        content4.currentNumberLabel.text = [NSString stringWithFormat:@"%d",_app.recodDb.kids];
        [content4.swSlider setValue:(float)_app.recodDb.kids animated:YES];
    }else{
        content4.currentNumberLabel.text = @"0";
        content4.swSlider.value = 0;
    }
    content4.delegate = self;
    [self addViewObject:content4 withHeight:height withWidth:width withAtIndex:3];
    
    
    FSTWork *work = [[FSTWork alloc] init];
    work.tag = 705;
    work.delegate = self;
    if(_app.recodDb.occupation.selectIndex >0){
        [work selectedListAtIndex:_app.recodDb.occupation.selectIndex];
    }
    [self addViewObject:work withHeight:height withWidth:width withAtIndex:4];
    
}


- (void)myCustomButton:(UIButton*)buttonStyle withTitle:(NSString*)title{
    
    [buttonStyle setTitleColor:CustomSelectedFontColor forState:UIControlStateNormal];
    [buttonStyle setTitle:title forState:UIControlStateNormal];
   // [buttonStyle.titleLabel setFont:_app.myFont];
    
}

- (void)reloadDataWithTagTag:(NSInteger)tagTag withBoolStatus:(BOOL)status withValue:(float)value withSelectedhRadio:(NSUInteger)index{
 
    
    NSString *title;
    UIButton *cusButton;
    
    
    switch (tagTag) {
        case 701:
            cusButton = _btnAge;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        case 702:
            _Qgender.selectIndex = index;
            switch (index) {
                case 1:
                    title = @"女性";
                    break;
                case 2:
                title = @"男性";
                    break;
            }
            cusButton = _btnGender;
            break;
        case 703:
            _Qmarried.selectIndex = index;
            switch (index) {
                case 1:
                    title = @"已婚";
                    break;
                case 2:
                    title = @"未婚";
                    break;
                case 3:
                    title = @"其他";
                    break;
                default:
                    break;
            }
            cusButton = _btnMarried;
            break;
        case 704:
            cusButton = _btnKids;
            title = [NSNumber numberWithInt:value].stringValue;
            break;
        case 705:
            _Qoccupation.selectIndex = index;
            switch (index) {
                case 1:
                    title = @"政府或国企员工";
                    break;
                case 2:
                    title = @"一般企业员工";
                    break;
                case 3:
                    title = @"企业高管";
                    break;
                case 4:
                    title = @"自由职业者";
                    break;
                case 5:
                    title = @"中小企业主";
                    break;
                case 6:
                    title = @"销售人员";
                    break;
                default:
                    break;
            }
            cusButton = _btnOccupation;
            break;
            
        default:
            break;
    }
    
    
    [self myCustomButton:cusButton withTitle:title];
}

- (void)chooseBoolDelegateWithObject:(id)viewObject withBoolStatus:(BOOL)status{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:status withValue:0 withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withValue:(float)value{
    
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:value withSelectedhRadio:0];
    
}

- (void)changeValueDelegateWithObject:(id)viewObject withSelectedhRadio:(NSUInteger)index{
 
    NSInteger tagTag = [(UIView*)viewObject tag];
    [self reloadDataWithTagTag:tagTag withBoolStatus:NO withValue:0 withSelectedhRadio:index];
    
}


@end
