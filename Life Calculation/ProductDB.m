//
//  ProductDB.m
//  Life_20131116
//
//  Created by BizOpsTech on 11/19/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
// ●

#import "ProductDB.h"

@implementation ProductDB
-(NSString *)getDetailContentWithModel:(insuranceModel)model productId:(int)index{
    NSString *content;
    switch (model) {
        #pragma mark 传统保险
        case insuranceModel_Traditional:
            switch (index) {
                case 0:
                    content = @"● The duration of this protection: 30 years;\n\n● 100% of  premium of one year as compensation for death or total disability from disease within the first policy year.\n\n● 100% of coverage as compensation for accidental death and total disability;\n\n● 100% of coverage as compensation for death and total disability from disease one year later since effective date of this policy ;";
                    break;
                case 1:
                    content = @"● The duration of this protection: 20 years;\n\n● 100% of coverage as compensation for death;\n\n● 150% of coverage as compensation for accidental death;";
                    break;
                case 2:
                    content = @"● The duration of this protection: 30 years;\n\n● 200% of coverage as compensation for death;\n\n● 110% of accumulated payment is returned if the insured is still alive after expiration of the policy;";
                    break;
                case 3:
                    content = @"● The duration of this protection: whole life;\n\n● 150% of coverage as compensation for death before 18 years old of insured;\n\n● 300% of coverage as compensation for death from 18 to 60 years old of insured;\n\n● 100% of coverage and total payment as compensation for death after 60 years old of insured;";
                    break;
                default:
                    break;
            }
            break;
        #pragma mark 意外保险
        case insuranceModel_Accidental:
            switch (index) {
                case 0:
                    content = @"● The duration of this protection: 20 years;\n\n● Total compensation up to 100% of coverage against accidental hurt;";
                    break;
                case 1:
                    content = @"● The duration of this protection:  30 year;\n\n● 100% of coverage as compensation for accidental death and total disability;\n\n● Certain percentage of coverage as compensation for certain degree of accidental disability.\n\n● 100% of coverage as compensation for health care;\n\n● Allowance of 200 RMB/day for hospitalization";
                    break;
                case 2:
                    content = @"● The duration of this protection: 30 years;\n\n● Certain percentage of coverage as compensation for certain degree of accidental disability;\n\n● Certain percentage of coverage as compensation for certain degree of scald;";
                    break;
                case 3:
                    content = @"● The duration of this protection: 20 year;\n\n● 100% of coverage as compensation for accidental death and total disability;\n\n● Certain percentage of coverage as compensation for certain degree of accidental disability.";
                    break;
                default:
                    break;
            }
            break;
        #pragma mark 健康保险
        case insuranceModel_Healthy:
            switch (index) {
                case 0:
                    content = @"● The duration of this protection : whole life;\n\n● 100% of coverage as compensation for agreed dread disease;\n\n● 100% of coverage as compensation for accidental death;";
                    break;
                case 1:
                    content = @"● The duration of this protection: 10 years;\n\n● 100% of coverage as compensation for agreed dread disease;";
                    break;
                case 2:
                    content = @"● The duration of this protection: 20 years;\n\n● 100% of coverage as compensation for agreed dread illness;";
                    break;
                case 3:
                    content = @"● The duration of this protection:  up to 80 years old;\n\n● 100% of coverage as compensation for agreed dread disease;\n\n● max between accumulated premium and cash value as compensation for death";
                    break;
                default:
                    break;
            }
            break;
        #pragma mark 养老保险
        case insuranceModel_Pension:
            switch (index) {
                case 0:
                    content = @"● The duration of this protection: whole life;\n\n● The drawing year is 60 years old;\n\n● The insured can draw pension of coverage once or by year or by month.\n\n● 100% of coverage as compensation for death";
                    break;
                case 1:
                    content = @"● The duration of this protection: up to 80 years old;\n\n● 20% of coverage offered to the insured yearly from the second policy year to 60 years old;\n\n● 40% of coverage offered to the insured yearly from 60 to 80 years old;\n\n● 100% of payment return if the insured is still alive after expiration date of the policy;";
                    break;
                case 2:
                    content = @"● Term of protection: up to 80 years;\n\n● 15% of coverage offered to the insured yearly from the effective date of policy;\n\n● 100% of payment return if the insured is still alive after expiration date of the policy;\n\n●100% of accumulated premium return if the insured is dead during the policy effective period";
                    break;
                case 3:
                    content = @"● The duration of this protection : up to 100 years old;\n\n● 10% of coverage offered to the insured yearly during the effective date to 60 years old;\n\n● 20% of coverage offered to the insured yearly during 60 to 80 years old;\n\n● 100% of payment return if the insured is still alive after expiration date of the policy;";
                    break;
                default:
                    break;
            }
            break;
        #pragma mark 教育保险
        case insuranceModel_Education:
            switch (index) {
                case 0:
                    content = @"● The duration of this protection : up to 22 years old;\n\n● 70% of coverage offered as education fund of university\n\n● 30% of coverage offered as fund of starting company ;\n\n● 150 % of accumulated premium return if the insured is dead during the policy effective period";
                    break;
                case 1:
                    content = @"● The duration of this protection: up to 30 years old;\n\n● 12% of coverage offered to the insured yearly from 15 to 24 years old of the insured;\n\n● 60% of coverage offered as fund of starting company ;\n\n● 100 % of accumulated premium return if the insured is dead during the policy effective period";
                    break;
                case 2:
                    content = @"● The duration of this protection: up to 28 years old;\n\n● 20% of coverage offered to the insured yearly from 15 to 21 years old;\n\n● 60% of coverage offered  to the insured as fund of further study ;\n\n● 100 % of coverage if the insured is dead during the policy effective period";
                    break;
                case 3:
                    content = @"● The duration of this protection: up to 30 years old;\n\n● 25% of coverage offered to the insured yearly from 18 to 21 years old;\n\n● The maximum between accumulated premium and cash value return if the insured is dead  during the policy effective period";
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    return content;
}
@end
