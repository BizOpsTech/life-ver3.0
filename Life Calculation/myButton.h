//
//  myButton.h
//  CF_00
//
//  Created by BizOpsTech on 8/29/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myButton : UIButton
@property (nonatomic)NSString *buttonRole;
@property(nonatomic)NSNumber *cornerRadius;
/*
 [NSArray arrayWithObjects:(id)[UIColor redColor].CGColor,(id)[UIColor greenColor].CGColor,nil]
 */
@property(nonatomic,retain)NSArray *bgColorArr;
@property(nonatomic,retain)UIColor *bgColor;

@property(nonatomic,retain)NSArray *hoverColorArr;
@property(nonatomic,retain)UIColor *hoverColor;
@end
