//
//  CustomResultCell.m
//  Life Calculation
//
//  Created by BizOpsTech on 11/15/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import "CustomResultCell.h"
@interface CustomResultCell()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *coverageLabel;
@property (strong, nonatomic) IBOutlet UITextField *txtRiskLevel;
@property (strong, nonatomic) IBOutlet UIImageView *iconView;

@end

@implementation CustomResultCell
@synthesize title = _title;
@synthesize icon = _icon;
@synthesize inPutRiskLevel = _inPutRiskLevel;
@synthesize coverage = _coverage;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

-(void)strokeToLeftView{
    UIView *tempView;
    UIColor *bc = [UIColor colorWithRed:214.f/255 green:214.f/255 blue:214.f/255 alpha:1.0];
    for(NSUInteger i=0; i<[self.contentView subviews].count;i++){
        tempView = [[self.contentView subviews]objectAtIndex:i];
        if(tempView.tag == 8 ){
            tempView.layer.borderWidth = 1.0;
            tempView.layer.borderColor = bc.CGColor;
        }
    }
}

-(void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
    [self strokeToLeftView];
}
-(void)setCoverage:(float)coverage{
    NSNumberFormatter *myFormatter = [[NSNumberFormatter alloc]init];
    myFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.coverageLabel.text = [myFormatter stringFromNumber:[NSNumber numberWithFloat:coverage]];
}
-(void)setInPutRiskLevel:(int)inPutRiskLevel{
    int level = inPutRiskLevel > 5 ? 5 :inPutRiskLevel;
    self.txtRiskLevel.text = [NSString stringWithFormat:@"%d",level];
    self.riskValue = level;
}

-(void)setIcon:(NSString *)icon{
    self.iconView.image = [UIImage imageNamed:icon];
    self.iconView.layer.cornerRadius = 6.f;
    self.iconView.layer.borderWidth = 1.0;
    self.iconView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clipsToBounds = YES;
    
}

//-(void)setLeftButton:(UIButton *)leftButton{
//    [leftButton addTarget:self action:@selector(updateRiskLevel:) forControlEvents:UIControlEventTouchUpInside];
//}
//-(void)setRightButton:(UIButton *)rightButton{
//    [rightButton addTarget:self action:@selector(updateRiskLevel:) forControlEvents:UIControlEventTouchUpInside];
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

////////////////////////////
- (IBAction)updateRiskLevel:(UIButton *)sender {
    int level = [self.txtRiskLevel.text intValue];
    if([sender isEqual:self.leftButton]){
        if(level > 0){
            level--;
        }
    }else{
        if(level < 5){
            level++;
        }
    }
    self.txtRiskLevel.text = [NSString stringWithFormat:@"%d",level];
    self.riskValue = level;
}

@end
