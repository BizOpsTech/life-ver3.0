//
//  Employees.h
//  CF_00
//
//  Created by BizOpsTech on 9/5/13.
//  Copyright (c) 2013 com.bizopstech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Employees : NSObject
@property(nonatomic,strong)UIImage *Icon;
@property(nonatomic,strong)NSString *Name;
@property(nonatomic,strong)NSString *Code;
@end
