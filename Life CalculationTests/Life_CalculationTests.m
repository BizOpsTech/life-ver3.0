//
//  Life_CalculationTests.m
//  Life CalculationTests
//
//  Created by Snow on 9/22/13.
//  Copyright (c) 2013 BizOpsTech. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Calculate.h"
#import "ProductList.h"

@interface Life_CalculationTests : XCTestCase
{
@private
    Calculate *calculator;
    
    
    
    float fYearlyIncome;
    float fYearlyExpense;
    BOOL fIsRetiredOfMan;
    float fCurrentAge;
    float fEndowmentInsurance;
    float fLife;
    float fAD;
    float fCityType;
    float fSocialInsurance;
    float fYearlyExpenseNow;
    float fChilds;
    float fCollageAge;
    float fIsForeignCollage;
    float fExpectedAge;
    
}
@end

@implementation Life_CalculationTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    
    fYearlyIncome = 200000;
    fYearlyExpense = 48000;
    fCurrentAge = 35;
    fEndowmentInsurance = 50000;
    fLife = 200000;
    fAD = 200000;
    fSocialInsurance = 200000;
    fYearlyExpenseNow = fYearlyExpense;
    fChilds = 1;
    fCollageAge = 13;
    ///////////////////////////////////
    fIsRetiredOfMan = YES;
    fIsForeignCollage = NO;
    ///////////////////////////////////
    fExpectedAge = 81;
    fCityType = 1;
    
    calculator = [[Calculate alloc] init];
    XCTAssertNotNil(calculator, @"instance is fail");
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//- (void)testExample
//{
//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
//}


#pragma mark    Tranditional Life
/*
 Traditional
 @yearlyIncom:年收入
 @yearyExpense:年费用
 @retiredAge：退休年龄：女55 男60
 @currentAge：当前年龄
 */

- (void)testCalculateTraditionalLifeCoverage{
    NSLog(@"%@ start",self.name);
    
    float result = [calculator calculateTraditionalLifeCoverageByYearlyIncome:fYearlyIncome
                                                        withSelfYearlyExpense:fYearlyExpense
                                                           withIsRetiredOfMan:fIsRetiredOfMan
                                                               withCurrentAge:fCurrentAge
                                                       withEndowmentInsurance:fEndowmentInsurance
                                                                     withLife:fLife];
    
    
    int resultInt = result;
    
    int minResult = (resultInt/10-3)*10;
    int maxResult = (resultInt/10+3)*10;
    
    XCTAssertTrue((resultInt>minResult && resultInt<maxResult), @"");
    NSLog(@"=====%@========resultInt %d=============",self.name,resultInt);
    NSLog(@"%@ end",self.name);
}

#pragma mark    Accident
/*
 Accident
 @yearlyIncom:年收入
 @yearyExpense:年费用
 @retiredAge：退休年龄：女55 男60
 @currentAge：当前年龄
 */


- (void)testCalculateAccidentCoverage{
    NSLog(@"%@ start",self.name);
    
    float result = [calculator calculateAccidentCoverageByYearlyIncome:fYearlyIncome
                                                 withSelfYearlyExpense:fYearlyExpense
                                                    withIsRetiredOfMan:fIsRetiredOfMan
                                                        withCurrentAge:fCurrentAge
                                                withEndowmentInsurance:fEndowmentInsurance
                                                              withLife:fLife
                                                                withAD:fAD];
    int resultInt = result;
    
    int minResult = (resultInt/10-3)*10;
    int maxResult = (resultInt/10+3)*10;
    
    XCTAssertTrue((resultInt>minResult && resultInt<maxResult), @"");
    NSLog(@"=====%@========resultInt %d=============",self.name,resultInt);
    NSLog(@"%@ end",self.name);
}

#pragma mark    Health
/*
 Health
 @cityType:城市等级
 @currentAge：当前年龄
 */

- (void)testCalculateHealthCoverage{
    NSLog(@"%@ start",self.name);
    
    float result = [calculator calculateHealthCoverageByCityType:fCityType
                                                  withCurrentAge:fCurrentAge
                                             withSocialInsurance:fSocialInsurance
                                                          withAD:fAD];
    int resultInt = result;
    
    int minResult = (resultInt/10-3)*10;
    int maxResult = (resultInt/10+3)*10;
    
    XCTAssertTrue((resultInt>minResult && resultInt<maxResult), @"");
    NSLog(@"=====%@========resultInt %d=============",self.name,resultInt);
    NSLog(@"%@ end",self.name);
    
}

#pragma mark    Retirement
/*
 Retirement
 @expectedAge:
 @isRetiredOfMan:是否退休
 @currentAge:当前年龄
 @yearlyExpenseNow:年费用
 */

- (void)testCalculateRetirementCoverage{
    NSLog(@"%@ start",self.name);
    
    float result = [calculator calculateRetirementCoverageByExpectedAge:fExpectedAge
                                                     withIsRetiredOfMan:fIsRetiredOfMan
                                                         withCurrentAge:fCurrentAge
                                                   withYearlyExpenseNow:fYearlyExpenseNow
                                                 withEndowmentInsurance:fEndowmentInsurance];
    
    int resultInt = result;
    
    int minResult = (resultInt/10-3)*10;
    int maxResult = (resultInt/10+3)*10;
    
    XCTAssertTrue((resultInt>minResult && resultInt<maxResult), @"");
    NSLog(@"=====%@========resultInt %d=============",self.name,resultInt);
    NSLog(@"%@ end",self.name);
    
}
#pragma mark    Children education
/*
 @fA:
 @fB:
 @fn：
 */

//Children education
//@ages:孩子年龄

- (void)testCalculateChildrenEducationCoverage{
    NSLog(@"%@ start",self.name);
    
    float result = [calculator calculateChildrenEducationCoverageByChilds:fChilds
                                                           withCollageAge:fCollageAge
                                                     withIsForeignCollage:fIsForeignCollage];
    
    int resultInt = result;
    
    int minResult = (resultInt/10-3)*10;
    int maxResult = (resultInt/10+3)*10;
    
    XCTAssertTrue((resultInt>minResult && resultInt<maxResult), @"");
    NSLog(@"=====%@========resultInt %d=============",self.name,resultInt);
    NSLog(@"%@ end",self.name);
}


- (void)testProductListRmt{
    
    NSLog(@"%@ start",self.name);
    
    
    
}

@end
